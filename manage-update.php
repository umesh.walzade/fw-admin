<?php
include("global/user_global.php");

check_login();
$page		=	"Portal Update Date";
$sub_page	=	"manage-update";
if(isset($_SESSION["id"]))
{
	unset($_SESSION["id"]);
}

?>
<!DOCTYPE html>
<html lang="en" class="app">
<head>
    <meta charset="utf-8" />
    <title>Welcome</title>
    <link rel="icon" href="images/favicon.ico">
    <meta name="description" content="app, web app, responsive, admin dashboard, admin, flat, flat ui, ui kit, off screen nav" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="stylesheet" href="css/app.v2.css" type="text/css" />
    <link rel="stylesheet" href="css/font.css" type="text/css" cache="false" />
    <link rel="stylesheet" href="js/calendar/bootstrap_calendar.css" type="text/css" cache="false" />
    <link rel="stylesheet" href="js/datatables/datatables.css" type="text/css" cache="false" />
    
    <!--[if lt IE 9]> <script src="js/ie/html5shiv.js" cache="false"></script> <script src="js/ie/respond.min.js" cache="false"></script> <script src="js/ie/excanvas.js" cache="false"></script> <![endif]-->
</head>

<body>
    <section class="vbox">
        <?php include("include/latest_js.php") ?>
        <?php include("include/topbar.php") ?>
        <section>
            <section class="hbox stretch">
                <!-- .aside -->
                <?php include("include/sidebar.php") ?>
                <!-- /.aside -->
                <section id="content">
                    <section class="vbox">
                        <section class="scrollable padder">
                            <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                                <li><a href="dashboard"><i class="fa fa-home"></i> Home</a></li>
                                <li><a href="">Last Update Date</a></li>
                                <li class="active">Manage Portal Update Date</li>
                            </ul>
                            <div class="m-b-md">
                                <h3 class="m-b-none">Manage Portal Update Date</h3>
                                <small>Welcome back, <?php echo $_SESSION["admin_name"] ?></small>
                                <input type="button" class="btn btn-danger pull-right" value="CREATE" onClick="location.href='create-update'">
                            </div>
                            <section class="panel panel-default">
                                <div class="table-responsive">
                                    <table class="table table-striped m-b-none" id="mytable">
                                        <thead>
                                            <tr>
                                                <th width="10%">Sr.</th>
                                                <th>Last Update Date</th>
                                                <th>Description</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        $i              =   1;
                                        $result_arr = array('i');
                                        $result_arr_val = array(1);
                                        $result_cat     =   $db_object->execute_select($fetch_update_date,$result_arr,$result_arr_val);
                                        while($rows       =   pg_fetch_array($result_cat))
                                        {
                                            $date = new DateTime($rows["lst_update_tmstmp"]);                                           
                                        ?>
                                            <tr>
                                                <td width="10%"><?php echo $i ?></td>
                                                <td><?php echo $date->format("d F Y"); ?></td>
                                                <td><?php echo $rows["modify_dtls"] ?></td>                                                
                                            </tr>
                                         <?php
                                         $i++;
                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                            </section>

                            <div class="m-b-md" style="margin-bottom: 40px !important; ">
                                <h3 class="m-b-none">Manage Update Date</h3>

                                <input type="button" class="btn btn-danger pull-right" value="CREATE" onClick="location.href='create-update-date.php'">
                            </div>


                            <section class="panel panel-default">
                                <div class="table-responsive">
                                    <table class="table table-striped m-b-none" id="mytableupdate">
                                        <thead>
                                            <tr>
                                                <th width="10%">Sr.</th>
                                                <th>Title</th>
                                                <th>Updated Date</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        $i              =   1;
                                        $result_arr = array('i');
                                        $result_arr_val = array(1);
                                        $result_cat     =   $db_object->execute_select($fetch_manage_date,$result_arr,$result_arr_val);
                                        while($rows       =   pg_fetch_array($result_cat))
                                        {?>
                                            <tr>
                                                <td width="10%"><?php echo $i ?></td>
                                                <td><?php echo $rows["title"] ?></td>
                                                <td><?php echo date('d-m-Y',strtotime($rows["lst_dt"])) ?></td>
                                                <td><a href="create-update-date.php?f9c7a57c74dcc509=<?php echo md5($rows["date_id"]) ?>"><i class="fa fa-pencil-square-o"></i></a></td>
                                            </tr>
                                         <?php
                                         $i++;
                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                            </section>
                            
                        </section>
                    </section>
                    <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav"></a>
                </section>
                <aside class="bg-light lter b-l aside-md hide" id="notes">
                    <div class="wrapper">Notification</div>
                </aside>
            </section>
        </section>
    </section>
    <script src="js/app.v2.js"></script>
    <!-- Bootstrap -->
    <!-- App -->
    <script src="js/sortable/jquery.sortable.js" cache="false"></script>
    <script src="js/datatables/jquery.dataTables.min.js" cache="false"></script>
    <script>
    $(document).ready(function() {
    $('#mytable').DataTable();
} );
    </script>
     <script>
    function ConfirmDelete()
    {
      var x = confirm("Are you sure you want to delete?");
      if (x)
          return true;
      else
        return false;
    }
    $(document).ready(function() {
    $('#mytable').DataTable();
} );
    
    </script>

    <script>
	$(document).ready(function() {
    $('#mytableupdate').DataTable();
} );
	</script>
     <script>
	function ConfirmDelete()
    {
      var x = confirm("Are you sure you want to delete?");
      if (x)
          return true;
      else
        return false;
    }
	$(document).ready(function() {
    $('#mytableupdate').DataTable();
} );
    
	</script>

</body>

</html>
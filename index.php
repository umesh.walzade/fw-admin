<?php
include("global/user_global.php");
$ranStr = md5(microtime());
$ranStr = substr($ranStr, 0, 6);
$_SESSION['cap_code'] = $ranStr;

if(isset($_SESSION["error_id"]))
{
  unset($_SESSION["error_id"]);
  echo '<script type="text/JavaScript">  
     alert("Something wrong, please try again"); 
     </script>' ;   
}

?>
<!DOCTYPE html>
<html lang="en" style="background:#e6e7e9">
  <head> 
    <meta charset="utf-8" /> 
    <title>Welcome</title> 
    <link rel="icon" href="images/favicon.ico">
    <meta name="description" content="app, web app, responsive, admin dashboard, admin, flat, flat ui, ui kit, off screen nav" /> 
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" /> 
    <link rel="stylesheet" href="css/app.v2.css" type="text/css" /> 
    <link rel="stylesheet" href="css/font.css" type="text/css" cache="false" /> 
    <!--[if lt IE 9]> <script src="js/ie/html5shiv.js" cache="false"></script> <script src="js/ie/respond.min.js" cache="false"></script> <script src="js/ie/excanvas.js" cache="false"></script> <![endif]-->
    <style>
		.message{
			background: #E31417; color: #FFF; padding: 10px; width: 100%; text-align: center;
		}
	  </style>
  </head>
  <body> 
    <section id="content" class="m-t-lg wrapper-md animated fadeInUp"> 
    <div style="width:100%; background:#009ad0">
    <center>
    <img src="images/dotLogo.png" style="padding:10px;">
    </center>
    </div>
      <div class="container aside-xxl"> 
        <section class="panel panel-default bg-white m-t-lg"> 
          <header class="panel-heading text-center"> 
            <strong>Sign in
            </strong> 
          </header> 
          <form id="login" name="login" action="" method="post" class="panel-body wrapper-lg" autocomplete="off"> 
          	<div class="message" style="margin-bottom: 5px;"><span class="msg"><?php echo $errMsg;?></span></div>
            <div class="form-group"> 
              <label class="control-label">Email
              </label> 
              <input type="email" name="email" id ="email" placeholder="test@example.com" class="form-control input-lg" value=""> 
            </div> 
            <div class="form-group"> 
              <label class="control-label">Password
              </label> 
              <input type="password" name="password" id="inputPassword" placeholder="Password" class="form-control input-lg" value=""> 
            </div>
             <div class="form-group"> 
              <label class="control-label"><?php echo "<div style='width:120px; padding:1px 10px;-webkit-user-select: none;-moz-user-select: none; -ms-user-select: none;-o-user-select: none; user-select: none; font-size:30px; background:url(images/captcha.jpg) no-repeat; align:center; border:1px solid;'>$ranStr</div>"; ?>
              </label> 
              <input type="text" name="captcha" id="captcha" placeholder="Enter Code" class="form-control input-lg"> 
            </div> 
            <input type="hidden" id="page" name="page" value="login">
          
            <input type="submit" name="action" id = "logBtn" class="btn btn-primary" value="Sign In">
            <div class="line line-dashed">
            </div> 
          </form> 
        </section> 
      </div> 
    </section> 
    <!-- footer --> 
    <footer id="footer"> 
      <div class="text-center padder"> 
        <p>
          <!--<small>Powered By Framework Communication <br>&copy; 2016</small>--></p> 
      </div> 
    </footer> 
    <!-- / footer --> 
    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/app.v2.js"></script> 
    <script type="text/javascript" src="js/jquery.validate.js"></script> 
<script type="text/javascript" src="js/jquery.form.js"></script>
<script>
  
$(document).ready(function(e) {

	 $(".message").hide();
	 $("#login").validate({
		submitHandler: function() {
      
				$.post("actions/login.php", //post
				$("#login").serialize(), 
					function(data){
						//if message is sent

						if (data!=1) {							
							  if((data).length  > 300){
								$("#page").val("token");
								$("#logBtn").prop('disabled', true);
							  }
							  else{
								$("#page").val("login");
								$("#logBtn").prop('disabled', false);
							  }
							  $(".msg").html(data); 
							$(".message").fadeIn(); //show confirmation message
						}
						else
						{
              location.assign('dashboard');
						}
					});
			return false; //don't let the form refresh the page...
		}
	});
});

function frmSubmit() {   
  $("#login").submit();
};
function frmcancel() {  
   //$(".msg").html("");
   //$(".message").hide();
   $("#page").val("login");
   $("#logBtn").prop('disabled', false);
   window.location.href = "index.php";
};


</script>

    <!-- Bootstrap --> 
    <!-- App --> 
  </body>
</html>

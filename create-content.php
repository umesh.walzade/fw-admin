<?php
include("global/user_global.php");
check_login();
header( 'Content-Type: text/html; charset=utf-8' ); 
$page			=	"content";
$sub_page		=	"manage-content";
$id				=	"";
$label			=	"Add";
$myaction		=	"cabdfdc";
$product_cnt	=	5;
if(isset($_REQUEST["f9c7a57c74dcc509"]))
{

    $_SESSION["id"]	=	$_REQUEST["f9c7a57c74dcc509"];


}
$result_arr = array('s');
$result_arr_val = array(md5($_SESSION["admin_id"]));
$result_tkn         =   $db_object->execute_select($fetch_token,$result_arr,$result_arr_val);
while($rows_tkn       =   pg_fetch_array($result_tkn)){
    $admin_tkn   =   $rows_tkn["token"];
}
if(isset($_SESSION["id"]))
{
   $myaction			=	"ecdbiftc";
	$label				=	"Edit";
	$id					=	$_SESSION["id"];
	/*if( ! filter_var($id, FILTER_VALIDATE_INT) )
	{
  	redirect('manage-content');
	}*/
	
	//$sql_content		=	$db_object->return_query("SELECT * from fw_content_master c INNER JOIN fw_menu_master m on m.menu_id = c.menu_id													where md5(c.cid)='".$_REQUEST["f9c7a57c74dcc509"]."'");
    $result_arr = array('s');
    $result_arr_val = array($_REQUEST["f9c7a57c74dcc509"]);
    $result_cat         =   $db_object->execute_select($fetch_edit_content,$result_arr,$result_arr_val);
    
    if(pg_num_rows($result_cat) === 0) exit('No rows');
    while($sql_content      =   pg_fetch_array($result_cat)){
    	$menu_id			=	$sql_content["menu_id"];
    	$page_name			=	$sql_content["page_name"];
    	$page_title			=	$sql_content["page_title"];
    	$content_title_eng	=	html_entity_decode(stripcslashes($sql_content["content_title_eng"]));
    	$content_title_hindi=	html_entity_decode(stripcslashes($sql_content["content_title_hindi"]));
    	$main_content_eng	=	$sql_content["main_content_eng"];
    	$main_content_hindi	=	$sql_content["main_content_hindi"];
    	$meta_keywords		=	$sql_content["meta_keywords"];
    	$meta_desc			=	$sql_content["meta_desc"];
    	$start_date			=	date("d-m-Y",strtotime($sql_content["start_date"]));
    	$end_date			=	date("d-m-Y",strtotime($sql_content["end_date"]));
        $action_btn         = "Archive & Update";
    }    
}
else
{
	$id					=	"";
	$menu_id			=	"";
	$page_name			=	"";
	$page_title			=	"";
	$content_title_eng	=	"";
	$content_title_hindi=	"";
	$main_content_eng	=	"";
	$main_content_hindi	=	"";
	$meta_keywords		=	"";
	$meta_desc			=	"";
	$start_date			=	"";
    $action_btn         = "Submit";
}
?>
<!DOCTYPE html>
<html lang="en" class="app">
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <title>Create Content</title>
    <link rel="icon" href="images/favicon.ico">
    <meta name="description" content="app, web app, responsive, admin dashboard, admin, flat, flat ui, ui kit, off screen nav" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="stylesheet" href="css/app.v2.css" type="text/css" />
    <link rel="stylesheet" href="css/font.css" type="text/css" cache="false" />
    <link rel="stylesheet" href="js/calendar/bootstrap_calendar.css" type="text/css" cache="false" />
    <link rel="stylesheet" href="js/datepicker/datepicker.css" type="text/css" cache="false" />
    <link rel="stylesheet" href="js/datatables/datatables.css" type="text/css" cache="false" />
    <script src="js/jQuery-3.5.1.min.js"></script>
    <script src="ckeditor/ckeditor.js"></script>
<style>
textarea{
	width:100%;
}
</style>
    <!--[if lt IE 9]> <script src="js/ie/html5shiv.js" cache="false"></script> <script src="js/ie/respond.min.js" cache="false"></script> <script src="js/ie/excanvas.js" cache="false"></script> <![endif]-->
</head>

<body>
    <section class="vbox">
        <?php include("include/topbar.php") ?>
        <section>
            <section class="hbox stretch">
                <!-- .aside -->
                <?php include("include/sidebar.php") ?>
                <!-- /.aside -->
                <section id="content">
                    <section class="vbox">
                        <section class="scrollable padder">
                            <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                                <li><a href="index.html"><i class="fa fa-home"></i> Home</a></li>
                                <li><a href="add-product">Content</a></li>
                                <li><a href="add-product">Manage Content</a></li>
                                <li class="active"><?php echo $label ?> Content</li>
                            </ul>
                            <div class="m-b-md">
                                <h3 class="m-b-none"><?php echo $label ?> Content</h3>
                                <small>Welcome back, <?php echo $_SESSION["admin_name"] ?></small>
                                <input type="button" class="btn btn-danger pull-right" value="Back" onClick="location.href='manage-content'">
                            </div>
                            <form name="myform" id="myform" method="post" action="actions/create-content.php" class="form-horizontal" enctype="multipart/form-data">
                            <input type="hidden" name="myaction" value="<?php echo md5($myaction) ?>">
                            <input type="hidden" name="token" value="<?php echo $admin_tkn; ?>">
                            <input type="hidden" name="f9c7a57c74dcc509" value="<?php echo $_REQUEST["f9c7a57c74dcc509"] ?>">
                                <section class="panel panel-default">
                                   <header class="panel-heading font-bold"><?php echo $label ?> Content</header>
                                   <div class="panel-body">
                                   		<div class="form-group"> <label class="col-sm-2 control-label">Select Menu</label>
                                        	<div class="col-sm-10"> 
                                            	<select name="menu_id" id="menu_id" class="form-control m-b"> 
                                                <option value="0" >Select Menu</option>
                                                <?php
												//$sql_menu       =   $db_object->execute_query("SELECT * from fw_menu_master order by menu_name ASC");
                                                //while($rows     =   mysql_fetch_array($sql_menu))
                                                $result_arr = array('i');
                                                $result_arr_val = array(1);
                                                $result_cat     =   $db_object->execute_select($fetch_manage_menu,$result_arr,$result_arr_val);
                                                while($rows       =   pg_fetch_array($result_cat))
												{
												?>
                                                <option value="<?php echo $rows["menu_id"] ?>" <?php echo $db_object->return_compare($rows["menu_id"],$menu_id,"selected='selected'","") ?>><?php echo $rows["menu_name"]." (".$rows["menu_name_hindi"].")" ?></option>
                                                <?php
												}
												?>
                                                </select>
                                            </div>
                                        </div>
                                        
                                        <div id="showdate">
                                        <div class="line line-dashed line-lg pull-in"></div>
                                        <div class="form-group"> <label class="col-sm-2 control-label">Start Date</label>
                                        	<div class="col-sm-10"> 
                                            	<input type="text" name="start_date" id="start_date" value="<?php echo $start_date ?>" class="form-control  datepicker-input" placeholder="dd-mm-yyyy" data-date-format="dd-mm-yyyy">
                                            </div>
                                        </div> 
                                        <div class="line line-dashed line-lg pull-in"></div>
                                        
                                        <div class="form-group"> <label class="col-sm-2 control-label">End Date</label>
                                        	<div class="col-sm-10"> 
                                            	<input type="text" name="end_date" id="end_date" value="<?php echo $end_date ?>" class="form-control  datepicker-input" placeholder="dd-mm-yyyy" data-date-format="dd-mm-yyyy">
                                            </div>
                                        </div> 
                                        </div>
                                        
                                        <div class="line line-dashed line-lg pull-in"></div>
                                        <div class="form-group"> <label class="col-sm-2 control-label">Page Name <em>(URL Name)</em></label>
                                        	<div class="col-sm-10"> 
                                            	<input type="text" name="page_name" value="<?php echo $page_name ?>" class="form-control" placeholder="Page Name" <?php if($label=="Edit") { ?> readonly <?php } ?> required>
                                            </div>
                                        </div>
                                        <div class="line line-dashed line-lg pull-in"></div>
                                        <div class="form-group"> <label class="col-sm-2 control-label">Page / Content Title</label>
                                        	<div class="col-sm-10"> 
                                            	<input type="text" name="page_title" value="<?php echo $page_title ?>" class="form-control" placeholder="Page / Content Title" required>
                                            </div>
                                        </div> 
                                        <div class="line line-dashed line-lg pull-in"></div>
                                        <div class="form-group"> <label class="col-sm-2 control-label">Content Title English</label>
                                        	<div class="col-sm-10"> 
                                                <input type="text" name="content_title_eng" id="content_title_eng" value="<?php echo $content_title_eng ?>" class="form-control" placeholder="Content Title English" required>
                                            </div>
                                        </div> 
                                        <div class="line line-dashed line-lg pull-in"></div>
                                        <div class="form-group"> <label class="col-sm-2 control-label">Content Title Hindi</label>
                                        	<div class="col-sm-10"> 
                                            	<input type="text" name="content_title_hindi" id="content_title_hindi" value="<?php echo $content_title_hindi ?>" class="form-control" placeholder="Content Title Hindi" required>
                                            </div>
                                        </div> 
                                        <div class="line line-dashed line-lg pull-in"></div>
                                        <div class="form-group" style="display:none;"> <label class="col-sm-2 control-label">Banner Image</label>
                                        	<div class="col-sm-10"> 
                                            	<input type="file" class="form-control" name="photo">
                                            </div>
                                        <div class="line line-dashed line-lg pull-in"></div>
                                        </div>
                                        <div class="form-group"> <label class="col-sm-2 control-label">Content English</label>
                                        	<div class="col-sm-10"> 
                                            	<textarea name="main_content_eng" id="main_content_eng" class="ckeditor"><?php echo $main_content_eng ?></textarea>
                                            </div>
                                        </div>
                                         <div class="line line-dashed line-lg pull-in"></div>
                                        <div class="form-group"> <label class="col-sm-2 control-label">Content Hindi</label>
                                        	<div class="col-sm-10"> 
                                            	<textarea name="main_content_hindi" id="main_content_eng" class="ckeditor" ><?php echo $main_content_hindi ?></textarea>
                                            </div>
                                        </div>
                                        <div class="line line-dashed line-lg pull-in"></div>
                                        <div class="form-group"> <label class="col-sm-2 control-label">Meta Keywords</label>
                                        	<div class="col-sm-10"> 
                                            	<input type="text" name="meta_keywords" value="<?php echo $meta_keywords ?>" class="form-control" placeholder="Meta Keywords" required>
                                            </div>
                                        </div> 
                                        <div class="line line-dashed line-lg pull-in"></div>
                                        <div class="form-group"> <label class="col-sm-2 control-label">Meta Description</label>
                                        	<div class="col-sm-10"> 
                                            	<input type="text" name="meta_desc" value="<?php echo $meta_desc ?>" class="form-control" placeholder="Meta Description" required>
                                            </div>
                                        </div> 
                                        <div class="line line-dashed line-lg pull-in"></div>
                                        <div class="form-group"> <label class="col-sm-2 control-label">Flag</label>
                                        	<div class="col-sm-10"> 
                                            	<select name="options" class="form-control m-b"> 
                                                <option value="1" >Publish</option>
                                                <option value="0" >Unpublish</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="line line-dashed line-lg pull-in"></div>
                                        <div class="form-group">  <label class="col-sm-2 control-label"></label>
                                        	<div class="col-sm-10"><input type="submit" name="action" class="btn btn-success btn-default" value="<?php echo $action_btn;?>"></div>
                                        </div>                                        
                                   </div>
                                </section>
                            </form>
                        </section>
                    </section>
                    <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav"></a>
                </section>
                <aside class="bg-light lter b-l aside-md hide" id="notes">
                    <div class="wrapper">Notification</div>
                </aside>
            </section>
        </section>
    </section>
    <script src="js/app.v2.js"></script>
    <!-- Bootstrap -->
    <!-- App -->
    <script src="js/sortable/jquery.sortable.js" cache="false"></script>
    <script src="js/datatables/jquery.dataTables.min.js" cache="false"></script>
    <script src="js/datepicker/bootstrap-datepicker.js" cache="false"></script>
    <script>
	$(document).ready(function() {
		<?php
	if($sql_content["circular"]==0)
	{
	?>
		$("#showdate").hide();
	<?php
	}
	?>
    $('#mytable').DataTable();
		$("#menu_id1").change(function(){
		id = this.value;
		$.ajax({     
    		type: "POST",
    		url: 'get_data.php',
    		data: 'action&id='+id,
    		success: function (data) {
    		if(data==1)
    			{
    				$("#showdate").show();
    				$("#start_date").prop('required',true);
    				$("#start_date").prop('required',true);
    			}
    			else
    				{
    					$("#showdate").hide();
    				}
    		},
    		dataType: "text"
		});
		})	
});

	</script>
    <script type="text/javascript">
                $(function(){
                $("textarea").htmlarea(); // Initialize jHtmlArea's with all default values
					
				
					
                });
                </script>
</body>

</html>
<?php
include("global/user_global.php");
check_login();
$page			=	"Gallery";
$sub_page		=	"manage-slider";
$id				=	"";
$label			=	"Add";
$myaction		=	"cabdfdc";
$product_cnt	=	5;
if(isset($_REQUEST["f9c7a57c74dcc509"]))
{
	$_SESSION["id"]	=	$_REQUEST["f9c7a57c74dcc509"];
}
$result_arr = array('i');
$result_arr_val = array(md5($_SESSION["admin_id"]));
$result_tkn         =   $db_object->execute_select($fetch_token,$result_arr,$result_arr_val);
while($rows_tkn       =   pg_fetch_array($result_tkn)){
    $admin_tkn   =   $rows_tkn["token"];
}
if(isset($_REQUEST["f9c7a57c74dcc509"]))
{
	$myaction			=	"ecdbiftc";
	$label				=	"Edit";
	$id					=	$_SESSION["id"];
	/*if( ! filter_var($id, FILTER_VALIDATE_INT) )
	{
  		redirect('manage-slider');
	}*/

    $result_arr = array('i');
    $result_arr_val = array($_REQUEST["f9c7a57c74dcc509"]);
	$result_cat       = $db_object->execute_select($fetch_edit_download,$result_arr,$result_arr_val);
    if(pg_num_rows($result_cat) === 0) exit('No rows');
    while($sql_content       =   pg_fetch_array($result_cat)){
        
    	$dwnld_title_eng	=	$sql_content["dwnld_title_eng"];
    	$dwnld_title_hindi	=	$sql_content["dwnld_title_hindi"];
    	$dwnld_url			=	$sql_content["dwnld_url"];
    	$dwnld_file_nm		=	$sql_content["dwnld_file_nm"];
		$dwnld_text_eng		=	$sql_content["dwnld_text_eng"];
		$dwnld_text_hindi	=	$sql_content["dwnld_text_hindi"];
    	$dwnld_size			=	$sql_content["dwnld_size"];
    	$dwnld_type			=	$sql_content["dwnld_type"];
    	$flag				=	$sql_content["flag"];
		
    }    
}
else
{
	$id					=	"";
	$dwnld_title_eng	=	"";
	$dwnld_url			=	"";
	$dwnld_file_nm		=	"";
	$dwnld_text_eng		=	"";
	$dwnld_size			=	"";
	$dwnld_type			=	"";
	$flag				=	1;
}
?>
<!DOCTYPE html>
<html lang="en" class="app">
<head>
    <meta charset="utf-8" />
    <title>Add Download</title>
    <link rel="icon" href="images/favicon.ico">
    <meta name="description" content="app, web app, responsive, admin dashboard, admin, flat, flat ui, ui kit, off screen nav" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="stylesheet" href="css/app.v2.css" type="text/css" />
    <link rel="stylesheet" href="css/font.css" type="text/css" cache="false" />
    <link rel="stylesheet" href="js/calendar/bootstrap_calendar.css" type="text/css" cache="false" />
    <link rel="stylesheet" href="js/datatables/datatables.css" type="text/css" cache="false" />
    <script src="js/jQuery-3.5.1.min.js"></script>
    <script src="ckeditor/ckeditor.js"></script>
<style>
textarea{
	width:100%;
}
</style>
    <!--[if lt IE 9]> <script src="js/ie/html5shiv.js" cache="false"></script> <script src="js/ie/respond.min.js" cache="false"></script> <script src="js/ie/excanvas.js" cache="false"></script> <![endif]-->
</head>

<body>
    <section class="vbox">
        <?php include("include/topbar.php") ?>
        <section>
            <section class="hbox stretch">
                <!-- .aside -->
                <?php include("include/sidebar.php") ?>
                <!-- /.aside -->
                <section id="content">
                    <section class="vbox">
                        <section class="scrollable padder">
                            <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                                <li><a href="index.html"><i class="fa fa-home"></i> Home</a></li>
                                <li><a href="manage-gallery">Download</a></li>
                                <li class="active"><?php echo $label ?> Add Download Data</li>
                            </ul>							
                            <div class="m-b-md">
                                <h3 class="m-b-none"><?php echo $label ?> Download</h3>
                                <small>Welcome back, <?php echo $_SESSION["admin_name"] ?></small>
                                <input type="button" class="btn btn-danger pull-right" value="Back" onClick="location.href='manage-download'">
                            </div>
                            <form name="myform" id="myform" method="post" action="actions/add-download.php" class="form-horizontal" enctype="multipart/form-data">
                            <input type="hidden" name="myaction" value="<?php echo md5($myaction) ?>">
                            <input type="hidden" name="token" value="<?php echo $admin_tkn; ?>">
                            <input type="hidden" name="f9c7a57c74dcc509" value="<?php echo $_REQUEST["f9c7a57c74dcc509"] ?>">
                                <section class="panel panel-default">
                                   <header class="panel-heading font-bold"><?php echo $label ?> Download</header>
                                   <div class="panel-body">
                                        <div class="form-group"> <label class="col-sm-2 control-label">Title in English</label>
                                        	<div class="col-sm-10"> 
                                            	<input type="text" name="dwnld_title_eng" value="<?php echo $dwnld_title_eng ?>" class="form-control" placeholder="Download File Title" required>
                                            </div>
                                        </div> 
                                        
                                        <div class="line line-dashed line-lg pull-in"></div>
										<div class="form-group"> <label class="col-sm-2 control-label">Title in Hindi</label>
                                        	<div class="col-sm-10"> 
                                            	<input type="text" name="dwnld_title_hindi" value="<?php echo $dwnld_title_hindi ?>" class="form-control" placeholder="Download File Title" required>
                                            </div>
                                        </div> 
                                        
                                        <div class="line line-dashed line-lg pull-in"></div>
										<div class="form-group"> <label class="col-sm-2 control-label">Link Text English</label>
                                        	<div class="col-sm-10"> 
                                            	<input type="text" name="dwnld_text_eng" value="<?php echo $dwnld_text_eng ?>" class="form-control" placeholder="Download Link Title" required>
                                            </div>
                                        </div> 
                                        
                                        <div class="line line-dashed line-lg pull-in"></div>
										<div class="form-group"> <label class="col-sm-2 control-label">Link Text Hindi</label>
                                        	<div class="col-sm-10"> 
                                            	<input type="text" name="dwnld_text_hindi" value="<?php echo $dwnld_text_hindi ?>" class="form-control" placeholder="Download Link Title" required>
                                            </div>
                                        </div> 
                                        
                                        <div class="line line-dashed line-lg pull-in"></div>
										<div class="form-group"> <label class="col-sm-2 control-label">Download File</label>
                                        	<div class="col-sm-10"> 
                                            	<input type="file" class="form-control" name="downfile">
                                            </div>
                                        </div>  
                                        <div class="line line-dashed line-lg pull-in"></div>										
                                        <div class="line line-dashed line-lg pull-in"></div>
                                        <div class="form-group"> <label class="col-sm-2 control-label">Flag</label>
                                        	<div class="col-sm-10"> 
                                            	<select name="options" class="form-control m-b"> 
                                                <option value="1" <?php echo $db_object->return_compare(1, $flag,"selected='selected'","") ?>>Publish</option>
                                                <option value="0" <?php echo $db_object->return_compare(0, $flag,"selected='selected'","") ?>>Un Publish</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="line line-dashed line-lg pull-in"></div>
                                        <div class="form-group">  <label class="col-sm-2 control-label"></label>
                                        	<div class="col-sm-10"><input type="submit" name="action" class="btn btn-success btn-default" value="Submit"></div>
                                        </div>                                        
                                   </div>
                                </section>
                            </form>
                        </section>
                    </section>
                    <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav"></a>
                </section>
                <aside class="bg-light lter b-l aside-md hide" id="notes">
                    <div class="wrapper">Notification</div>
                </aside>
            </section>
        </section>
    </section>
    <script src="js/app.v2.js"></script>
    <!-- Bootstrap -->
    <!-- App -->
    <script src="js/sortable/jquery.sortable.js" cache="false"></script>
    <script src="js/datatables/jquery.dataTables.min.js" cache="false"></script>
    
    <script>
	$(document).ready(function() {
    $('#mytable').DataTable();
});

	</script>
    <script type="text/javascript">
                $(function(){
                $("textarea").htmlarea(); // Initialize jHtmlArea's with all default values
                });
                </script>
</body>

</html>
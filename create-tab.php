<?php
include("global/user_global.php");
check_login();
$page		=	"tab";
$sub_page	=	"manage-tab";

$result_arr = array('s');
$result_arr_val = array(md5($_SESSION["admin_id"]));
$result_tkn         =   $db_object->execute_select($fetch_token,$result_arr,$result_arr_val);
while($rows_tkn       =   pg_fetch_array($result_tkn)){
    $admin_tkn   =   $rows_tkn["token"];
}

if(isset($_REQUEST["f9c7a57c74dcc509"]))
{
	$id					=	$db_object->sanatize_value($_REQUEST["f9c7a57c74dcc509"]);
	/*if( ! filter_var($_REQUEST["f9c7a57c74dcc509"], FILTER_VALIDATE_INT) )
	{
  	redirect('manage-menu');
	}*/

    //mysqli_query('SET character_set_results=utf8');

    $result_arr = array('s');
    $result_arr_val = array($_REQUEST["f9c7a57c74dcc509"]);
	$result_cat				=	$db_object->execute_select($fetch_edit_tab,$result_arr,$result_arr_val);
    if(pg_num_rows($result_cat) === 0) exit('No rows');
    while($sql       =   pg_fetch_array($result_cat)){
    	$myaction			=	"ecdbiftc";
    	$label				=	"Edit";
        $tab_name           =   $sql["tab_name"];
        $tab_name_hindi         =   $sql["tab_name_hindi"];
        $main_content_eng         =   $sql["main_content_eng"];
    	$main_content_hindi			=	$sql["main_content_hindi"];
    	$flag				=	$sql["flag"];
    	$sidebar			=	$sql["sidebar"];
    	$ordering			=	$sql["ordering"];
    }
}
else
{
	$cat_name			=	"";
	$myaction			=	"cabdfdc";
	$id					=	"";
	$label				=	"Create";
	$tab_name			=	"";
    $tab_name_hindi     =   "";
    $main_content_eng   =   "";
    $main_content_hindi =   "";
	$flag				=	"";
	$sidebar			=	1;
	$ordering			=	NULL;
}

?>
<!DOCTYPE html>
<html lang="en" class="app">
<head>
    <meta charset="utf-8" />
    <title>Create Tab</title>
    <link rel="icon" href="images/favicon.ico">
    <meta name="description" content="app, web app, responsive, admin dashboard, admin, flat, flat ui, ui kit, off screen nav" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="stylesheet" href="css/app.v2.css" type="text/css" />
    <link rel="stylesheet" href="css/font.css" type="text/css" cache="false" />
    <link rel="stylesheet" href="js/calendar/bootstrap_calendar.css" type="text/css" cache="false" />
    <link rel="stylesheet" href="js/datatables/datatables.css" type="text/css" cache="false" />
     <script src="js/jQuery-3.5.1.min.js"></script>
    <script src="ckeditor/ckeditor.js"></script>
<style type="text/css">
    textarea{
    width:100%;
}

</style>    
    <!--[if lt IE 9]> <script src="js/ie/html5shiv.js" cache="false"></script> <script src="js/ie/respond.min.js" cache="false"></script> <script src="js/ie/excanvas.js" cache="false"></script> <![endif]-->
</head>

<body>
    <section class="vbox">
        <?php include("include/latest_js.php") ?>
        <?php include("include/topbar.php") ?>
        <section>
            <section class="hbox stretch">
                <!-- .aside -->
                <?php include("include/sidebar.php") ?>
                <!-- /.aside -->
                <section id="content">
                    <section class="vbox">
                        <section class="scrollable padder">
                            <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                                <li><a href="index.html"><i class="fa fa-home"></i> Home</a></li>
                                <li><a href="manage-tab">Tab</a></li>
                                <li><a href="manage-tab">Manage Tab</a></li>
                                <li class="active"><?php echo $label ?> Tab</li>
                            </ul>
                            <div class="m-b-md">
                                <h3 class="m-b-none"><?php echo $label ?> Tab</h3>
                                <small>Welcome back, <?php echo $_SESSION["admin_name"] ?></small>
                                <input type="button" class="btn btn-danger pull-right" value="Back" onClick="location.href='manage-tab.php'">
                            </div>
                            <form name="myform" id="myform" method="post" action="actions/create-tab.php">
                            <input type="hidden" name="myaction" value="<?php echo md5($myaction) ?>">
                            <input type="hidden" name="token" value="<?php echo $admin_tkn; ?>">
                            <input type="hidden" name="f9c7a57c74dcc509" value='<?php echo $_REQUEST["f9c7a57c74dcc509"]?>'>
                                <section class="panel panel-default">
                                   <header class="panel-heading font-bold"><?php echo $label ?> Tab</header>
                                   <div class="panel-body">
                                         <div class="form-group"> <label>Tab Name English</label> <input type="text" class="form-control" placeholder="Enter Tab Name" name="tab_name" value="<?php echo $tab_name ?>" required> </div>

                                         <div class="form-group"> <label>Tab Name Hindi</label> <input type="text" class="form-control" placeholder="Enter Tab Name" name="tab_name_hindi" value="<?php echo $tab_name_hindi ?>" required> </div>

                                          <div class="form-group"> <label>English Content</label> 
                                                <textarea name="main_content_eng" id="main_content_eng" class="ckeditor"><?php echo $main_content_eng ?></textarea>
                                          </div>

                                          <div class="form-group"> <label>Hindi Content</label> 
                                                <textarea name="main_content_hindi" id="main_content_hindi" class="ckeditor"><?php echo $main_content_hindi ?></textarea>
                                          </div>

                                         <div class="form-group"> <label>Flag</label>
                                        <select name="flag" class="form-control" required>
                                            <option value="1" <?php echo $db_object->return_compare($flag,1,"selected='selected'","") ?>>Publish</option>
                                            <option value="0" <?php echo $db_object->return_compare($flag,0,"selected='selected'","") ?>>Unpublish</option>
                                        </select>
                                        </div>
                                        <input type="hidden" name="sidebar" value="0"> 
                                         <input type="button" class="btn btn-success btn-default" value="Submit" name="action" id="submit_action">
                                   </div>
                                </section>
                            </form>                                                        
                        </section>
                    </section>
                    <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav"></a>
                </section>
                <aside class="bg-light lter b-l aside-md hide" id="notes">
                    <div class="wrapper">Notification</div>
                </aside>
            </section>
        </section>
    </section>
    <script src="js/app.v2.js"></script>
    <!-- Bootstrap -->
    <!-- App -->
    <script src="js/sortable/jquery.sortable.js" cache="false"></script>
    <script src="js/datatables/jquery.dataTables.min.js" cache="false"></script>


    <script>
	$(document).ready(function() {
    $('#mytable').DataTable();

    $("#submit_action").click(function(){               
        //if(validateForm()){
           //alert("Validation Pass");
            //document.myForm.submit();           
        //}
        $( "#myform" ).submit();
        
    });
} );
    function validateForm(){  
            
            /*var name=document.myform.name.value; 
            var email=document.myform.email.value; 
            var pass=document.myform.pass.value; 

            var ck_name = /^[A-Za-z ]{3,20}$/;
            var ck_email = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i 
            var ck_password =  /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{7,15}$/; ///^[A-Za-z0-9!@#$%^&*()_]{6,20}$/; 
              
            if (!ck_name.test(name)) {
               alert("Enter a valid Name ");
               document.myform.name.value=""; 
            

               return false;
            }
            else if (!ck_email.test(email)) {
                alert("Enter a valid email address");
                
               document.myform.email.value="";
               
                return false;
            }
            
            else if (!ck_password.test(pass)) {
              alert("password between 7 to 15 characters which contain at least one numeric digit and a special character");
             
               document.myform.pass.value="";
              return false;
            }
            else{
                return true;
            }
            */
            return true;
            
        }
	</script>
</body>

</html>
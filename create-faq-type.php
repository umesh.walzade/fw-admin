<?php
include("global/user_global.php");
check_login();
$page		=	"menu";
$sub_page	=	"manage-menu";

$result_arr = array('s');
$result_arr_val = array(md5($_SESSION["admin_id"]));
$result_tkn         =   $db_object->execute_select($fetch_token,$result_arr,$result_arr_val);
while($rows_tkn       =   pg_fetch_array($result_tkn)){
    $admin_tkn   =   $rows_tkn["token"];
}

if(isset($_REQUEST["f9c7a57c74dcc509"]))
{
	$id					=	$db_object->sanatize_value($_REQUEST["f9c7a57c74dcc509"]);
	
    //mysqli_query('SET character_set_results=utf8');
    $result_arr = array('s');
    $result_arr_val = array($_REQUEST["f9c7a57c74dcc509"]);
    
	$result_cat				=	$db_object->execute_select($fetch_edit_faq_mstr,$result_arr,$result_arr_val);
    if(pg_num_rows($result_cat) === 0) exit('No rows');
    while($sql       =   pg_fetch_array($result_cat)){
    	$myaction			=	"ecdbiftc";
    	
    	$label				=	"Edit";
    	$category_name		=	$sql["faq_category_name"];
    	$cat_parent			=	$sql["faq_cat_parent"];
    	$category_desc		=	$sql["faq_category_desc"];
    	$flag				=	$sql["flag"];
    }
}
else
{
	$cat_name			=	"";
	$myaction			=	"cabdfdc";
	$id					=	"";
	$label				=	"Create";
	$category_name		=	"";
	$cat_parent			=	"";
	$category_desc		=	"";
	$flag				=	"1";
}

?>
<!DOCTYPE html>
<html lang="en" class="app">
<head>
    <meta charset="utf-8" />
    <title>Create FAQ</title>
    <link rel="icon" href="images/favicon.ico">
    <meta name="description" content="app, web app, responsive, admin dashboard, admin, flat, flat ui, ui kit, off screen nav" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="stylesheet" href="css/app.v2.css" type="text/css" />
    <link rel="stylesheet" href="css/font.css" type="text/css" cache="false" />
    <link rel="stylesheet" href="js/calendar/bootstrap_calendar.css" type="text/css" cache="false" />
    <link rel="stylesheet" href="js/datatables/datatables.css" type="text/css" cache="false" />
    
    <!--[if lt IE 9]> <script src="js/ie/html5shiv.js" cache="false"></script> <script src="js/ie/respond.min.js" cache="false"></script> <script src="js/ie/excanvas.js" cache="false"></script> <![endif]-->
</head>

<body>
    <section class="vbox">
        <?php include("include/latest_js.php") ?>
        <?php include("include/topbar.php") ?>
        <section>
            <section class="hbox stretch">
                <!-- .aside -->
                <?php include("include/sidebar.php") ?>
                <!-- /.aside -->
                <section id="content">
                    <section class="vbox">
                        <section class="scrollable padder">
                            <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                                <li><a href="index.html"><i class="fa fa-home"></i> Home</a></li>
                                <li><a href="manage-menu">FAQ Category</a></li>
                                <li><a href="manage-menu">Manage FAQ Category</a></li>
                                <li class="active"><?php echo $label ?> FAQ Category</li>
                            </ul>
                            <div class="m-b-md">
                                <h3 class="m-b-none"><?php echo $label ?> FAQ Type</h3>
                                <small>Welcome back, <?php echo $_SESSION["admin_name"] ?></small>
                                <input type="button" class="btn btn-danger pull-right" value="Back" onClick="location.href='manage-faq-type'">
                            </div>
                            <form name="myform" id="myform" method="post" action="actions/create-faq-type.php">
                            <input type="hidden" name="myaction" value="<?php echo md5($myaction) ?>">
                            <input type="hidden" name="token" value="<?php echo $admin_tkn; ?>">
                            <input type="hidden" name="f9c7a57c74dcc509" value='<?php echo $_REQUEST["f9c7a57c74dcc509"]?>'>
                                <section class="panel panel-default">
                                   <header class="panel-heading font-bold"><?php echo $label ?> FAQ Type</header>
                                   <div class="panel-body">
                                      	<div class="form-group"> <label>Category</label>
										 <input type="text" class="form-control" placeholder="Enter Category Name" name="category_name" value="<?php echo $category_name ?>" required>
                                        
                                        </div>
                                        <div class="form-group"> <label>Parent Category</label>
                                        <select name="parent_menu" class="form-control">
                                        	<option value="0">Select Parent Category</option>
                                        	<?php
                                            $result_arr = array('i');
                                            $result_arr_val = array(1);
                                            $result_cat     =   $db_object->execute_select($fetch_faq_sub_cat,$result_arr,$result_arr_val);
                                            while($rows     =   pg_fetch_array($result_cat))
											{
											?>
                                        	<option value="<?php echo $rows["faq_cat_id"] ?>" <?php echo $db_object->return_compare($cat_parent,$rows["faq_cat_id"],"selected='selected'","") ?>><?php echo $rows["faq_category_name"]?></option>
                                            <?php
											}
											?>
                                        </select>
                                        </div>
                                         <div class="form-group"> <label>faq_category_desc</label> <input type="text" class="form-control" placeholder="Enter Category Description" name="category_desc" value="<?php echo $category_desc ?>" required> </div>
                                          
                                         <div class="form-group"> <label>Flag</label>
                                        <select name="flag" class="form-control" required>
                                            <option value="1" <?php echo $db_object->return_compare($flag,1,"selected='selected'","") ?>>Publish</option>
                                            <option value="0" <?php echo $db_object->return_compare($flag,0,"selected='selected'","") ?>>Unpublish</option>
                                        </select>
                                        </div>
                                        
                                         <input type="button" class="btn btn-success btn-default" value="Submit" name="action" id="submit_action">
                                   </div>
                                </section>
                            </form>                                                        
                        </section>
                    </section>
                    <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav"></a>
                </section>
                <aside class="bg-light lter b-l aside-md hide" id="notes">
                    <div class="wrapper">Notification</div>
                </aside>
            </section>
        </section>
    </section>
    <script src="js/app.v2.js"></script>
    <!-- Bootstrap -->
    <!-- App -->
    <script src="js/sortable/jquery.sortable.js" cache="false"></script>
    <script src="js/datatables/jquery.dataTables.min.js" cache="false"></script>
    <script>
	$(document).ready(function() {
    $('#mytable').DataTable();

    $("#submit_action").click(function(){               
        $( "#myform" ).submit();        
    });
} );
    function validateForm(){  
            
            return true;
            
        }
	</script>
</body>

</html>
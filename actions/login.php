<?php
include("../global/user_global.php");
$date = new DateTime("now");


if(isset($_REQUEST["action"]))
{
	switch($_REQUEST["page"])
	{	
		case "token":

			$email		=	$db_object->sanatize_string($_REQUEST["email"]);
			$password	=	$db_object->sanatize_string($_REQUEST["password"]);
			//$token = mcrypt_encrypt_data($_SESSION["admin_id"],$_REQUEST["token"]);
			$display ="";
			$sql		=	"SELECT * from fw_administrator where admin_id=$1 and token_dt=$2 and email=$3 and pass=$4";
			$result_arr = array('i','s','s','s');
			$result_arr_val = array($_SESSION["admin_id"],$_REQUEST["token"],$email,$password);
			$result1 = $db_object->execute_select($sql,$result_arr,$result_arr_val);

			if(pg_num_rows($result1)== 0) {
				$display	=	"Something Wrong, Please login";
			}
			else{	
				$display ="";
				$email		=	$db_object->sanatize_string($_REQUEST["email"]);
				$password	=	$db_object->sanatize_string($_REQUEST["password"]);
				$token ="";
				while($result       =   pg_fetch_array($result1)){									
					//session_regenerate_id();
					$_SESSION["admin_id"]		=	$result["admin_id"];
					$_SESSION["admin_name"]		=	$result["name"];
					$display					=	$result["admin_id"];

					
					if($display!=1){	
						$display = "Please contact to Administrator";
					}
					$_SESSION["admin_role"]		=	$result["admin_role"];
				
				$dateTime = $date->format("Y-m-d H:m:s");
				$token = mcrypt_encrypt_data($_SESSION["admin_id"],$dateTime);
				$sql		=	"update fw_administrator set um_token =$1, token_dt=$2 where admin_id=$3";
				$result_arr = array('s','s','i');
				$result_arr_val = array($token,$dateTime,$_SESSION["admin_id"]);
				$result		=	$db_object->execute_select($sql, $result_arr, $result_arr_val);
				}	

				$update_token ="update fw_token_tbl set token=$1 where session_id=$2";
				$result_arr = array('s','i');
				$result_arr_val = array(md5($token),$_SESSION["admin_id"]);
				$result_tkn = $db_object->execute_select($update_token,$result_arr,$result_arr_val);
			}	
			

		echo $display;		
		break;

		case "login":
		
		//db conn end
		
		/*$host		= "172.25.2.246";
$database	= "rms_portal";
$username 	= "enterprisedb";
$password 	= "enterprisedb";
		$con = pg_connect("host=$host dbname=$database user=$username password=$password")
    or die ("Could not connect to server\n"); 

$query = "SELECT VERSION()"; 
$rs = pg_query($con, $query) or die("Cannot execute query: $query\n"); 
$row = pg_fetch_row($rs);

echo $row[0] . "\n"."DB Connection Succesfully extiblished";

pg_close($con); */


//db conn end

		$email		=	$db_object->sanatize_string($_REQUEST["email"]);
		$password	=	$db_object->sanatize_string($_REQUEST["password"]);
		if ($_POST['captcha'] != $_SESSION['cap_code'])
		{
			$display	=	"Captcha code is wrong!";
		}
		else
		{
		$sql		=	"SELECT * from fw_administrator where email=$1 and pass=$2";
		//$result		=	$db_object->return_query($sql);
		$result_arr = array('s','s');
		$result_arr_val = array($email,$password);
		$result1 = $db_object->execute_select($sql,$result_arr,$result_arr_val);
	
		
		if(pg_num_rows($result1) === 0) {
			$display	=	"Wrong Username or Password";
		}
		else{	
			
			while($result       =   pg_fetch_array($result1)){

					session_regenerate_id();
					$_SESSION["admin_id"]		=	$result["admin_id"];
					$_SESSION["admin_name"]		=	$result["name"];
					$display					=	$result["admin_id"];
					$_SESSION["admin_role"]		=	$result["admin_role"];


					if($result["is_login"]==1)
					{
						$display	=	"<p>Your current session is still active. Do you want to override the active session?</p>
										<input type='hidden' id='token' name='token' value='".$result["token_dt"]."'> 										
										<input type='button' id ='yes' name='action' class='btn btn-primary' onclick='frmSubmit()' value='Sign In'>
										<input type='button' id='no' name='action1' class='btn btn-primary' onclick='frmcancel()' value='Cancel'>";
					}
					else if($result["email"]!="" && $result["pass"]!="")
					{
						// Prevent multiple login with same User Id : Vulnerability
						$sql		=	"update fw_administrator set is_login =$1 where email=$2 and pass=$3";
						$result_arr = array('i','s','s');
						$result_arr_val = array(1, $email,$password);
						$result		=	$db_object->execute_select($sql, $result_arr, $result_arr_val);
						$dateTime = $date->format("Y-m-d H:m:s");
						$token = mcrypt_encrypt_data($_SESSION["admin_id"],$dateTime);
						$sql		=	"update fw_administrator set um_token =$1, token_dt=$2 where email=$3 and pass=$4";
						$result_arr = array('s','s','s','s');
						$result_arr_val = array($token,$dateTime,$email,$password);
						$result		=	$db_object->execute_select($sql, $result_arr, $result_arr_val);

						$update_token ="update fw_token_tbl set token=$1 where session_id=$2";
						$result_arr = array('s','i');
						$result_arr_val = array(md5($token),$_SESSION["admin_id"]);
						$result_tkn = $db_object->execute_select($update_token,$result_arr,$result_arr_val);

						if($display!=1){	
							$display = "Please contact to Administrator";
						}	
						//$_SESSION["dateTime"]		=	$dateTime;
						//exit;
					}
					else
					{
						$display	=	"Wrong Username or Password";
					}
				}
			}
		}
		echo $display;
		break;

		

		case "logout":
		unset($_SESSION["admin_id"]);
		echo 0;
		break;
	}
}
else
{
	echo "error";
}
?>
<?php
include("../global/user_global.php");
$date = new DateTime("now");


if(isset($_REQUEST["action"]))
{
	switch($_REQUEST["page"])
	{	
		case "token":

			$email		=	$db_object->sanatize_string($_REQUEST["email"]);
			$password	=	$db_object->sanatize_string($_REQUEST["password"]);
			//$token = mcrypt_encrypt_data($_SESSION["admin_id"],$_REQUEST["token"]);
			$display ="";
			$sql		=	"SELECT * from mgr_admin_login where user_id=? and token_dt=? and usr_email=? and usr_password=?";
			$result_arr = array('i','s','s','s');
			$result_arr_val = array($_SESSION["admin_id"],$_REQUEST["token"],$email,$password);
			$result1 = $db_object->execute_select_multiple($sql,$result_arr,$result_arr_val);
			if($result1->num_rows == 0) {
				$display	=	"Something Wrong, Please login";
			}
			else{	
				$display ="";
				$email		=	$db_object->sanatize_string($_REQUEST["email"]);
				$password	=	$db_object->sanatize_string($_REQUEST["password"]);
				$token ="";
				while($result       =   $result1->fetch_array(MYSQLI_ASSOC)){									
					//session_regenerate_id();
					$_SESSION["admin_id"]		=	$result["user_id"];
					$_SESSION["admin_name"]		=	$result["usr_name"];
					$display					=	$result["user_id"];

					
					if($display!=1){	
						$display = "Please contact to Administrator";
					}
					$_SESSION["admin_role"]		=	$result["admin_role"];
				
				$dateTime = $date->format("Y-m-d H:m:s");
				$token = mcrypt_encrypt_data($_SESSION["admin_id"],$dateTime);
				$sql		=	"update mgr_admin_login set usr_token =?, token_dt=? where user_id=?";
				$result_arr = array('s','s','i');
				$result_arr_val = array($token,$dateTime,$_SESSION["admin_id"]);
				$result		=	$db_object->execute_update($sql, $result_arr, $result_arr_val);
				}	

				$update_token ="update fw_token_tbl set token=? where session_id=?";
				$result_arr = array('s','i');
				$result_arr_val = array(md5($token),$_SESSION["admin_id"]);
				$result_tkn = $db_object->execute_update($update_token,$result_arr,$result_arr_val);

			}	
			

		echo $display;		
		break;

		case "login":
		$email		=	$db_object->sanatize_string($_REQUEST["email"]);
		$password	=	$db_object->sanatize_string($_REQUEST["password"]);
		if ($_POST['captcha'] != $_SESSION['cap_code'])
		{
			$display	=	"Captcha code is wrong!";
		}
		else
		{
		$sql		=	"SELECT * from mgr_admin_login where usr_email=? and usr_password=?";
		//$result		=	$db_object->return_query($sql);
		$result_arr = array('s','s');
		$result_arr_val = array($email,$password);
		$result1 = $db_object->execute_select_multiple($sql,$result_arr,$result_arr_val);
		
		if($result1->num_rows === 0) {
			$display	=	"Wrong Username or Password";
		}
		else{	
			
			while($result       =   $result1->fetch_array(MYSQLI_ASSOC)){

					session_regenerate_id();
					$_SESSION["admin_id"]		=	$result["user_id"];
					$_SESSION["admin_name"]		=	$result["usr_name"];
					$display					=	$result["user_id"];
					$_SESSION["admin_role"]		=	$result["admin_role"];


					if($result["is_login"]==1)
					{
						$display	=	"<p>Your current session is still active. Do you want to override the active session?</p>
										<input type='hidden' id='token' name='token' value='".$result["token_dt"]."'> 										
										<input type='button' id ='yes' name='action' class='btn btn-primary' onclick='frmSubmit()' value='Sign In'>
										<input type='button' id='no' name='action1' class='btn btn-primary' onclick='frmcancel()' value='Cancel'>";
					}
					else if($result["usr_email"]!="" && $result["usr_password"]!="")
					{
						// Prevent multiple login with same User Id : Vulnerability
						$sql		=	"update mgr_admin_login set is_login =? where usr_email=? and usr_password=?";
						$result_arr = array('i','s','s');
						$result_arr_val = array(1, $email,$password);
						$result		=	$db_object->execute_update($sql, $result_arr, $result_arr_val);
						$dateTime = $date->format("Y-m-d H:m:s");
						$token = mcrypt_encrypt_data($_SESSION["admin_id"],$dateTime);
						$sql		=	"update mgr_admin_login set usr_token =?, token_dt=? where usr_email=? and usr_password=?";
						$result_arr = array('s','s','s','s');
						$result_arr_val = array($token,$dateTime,$email,$password);
						$result		=	$db_object->execute_update($sql, $result_arr, $result_arr_val);

						$update_token ="update fw_token_tbl set token=? where session_id=?";
						$result_arr = array('s','i');
						$result_arr_val = array(md5($token),$_SESSION["admin_id"]);
						$result_tkn = $db_object->execute_update($update_token,$result_arr,$result_arr_val);

						if($display!=1){	
							$display = "Please contact to Administrator";
						}	
						//$_SESSION["dateTime"]		=	$dateTime;
						//exit;
					}
					else
					{
						$display	=	"Wrong Username or Password";
					}
				}
			}
		}
		echo $display;
		break;

		

		case "logout":
		unset($_SESSION["admin_id"]);
		echo 0;
		break;
	}
}
else
{
	echo "error";
}
?>
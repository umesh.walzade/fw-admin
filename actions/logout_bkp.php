<?php
include("../global/user_global.php");

//Remove logged in flag for multiple login with same User Id : Vulnerability

//$sql	= $db_object->execute_query("update fw_administrator set is_login =0 where admin_id=".$_SESSION["admin_id"]);
$token = mcrypt_encrypt_data(0,0);

$sql= "update mgr_admin_login set is_login =?,usr_token =? where user_id=?";
$result_arr = array('i','s','i');
$result_arr_val = array(0, $token,$_SESSION["admin_id"]);
$result		=	$db_object->execute_update($sql, $result_arr, $result_arr_val);	


$key = 'mcrypt@encrypt!logout';
$date = new DateTime("now");
$dateTime = $date->format("Y-m-d H:m:s");
$token_key =$_SERVER['REMOTE_ADDR'].$_SESSION["admin_id"].$dateTime.$key;
$iv = md5(md5($token_key));
$update_token ="update fw_token_tbl set token=? where session_id=?";
$result_arr = array('s','i');
$result_arr_val = array($iv,$_SESSION["admin_id"]);
$result1 = $db_object->execute_update($update_token,$result_arr,$result_arr_val);


unset($_SESSION["admin_id"]);
unset($_SESSION["admin_name"]);

//destroy all sessions 
fun_session_destroy();

//session_regenerate_id();
//session_unset();
//session_destroy();

redirect("../index");
?>
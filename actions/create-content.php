<?php
include("../global/user_global.php");
check_login();
$file_obj=new file_manup("$upload_path");
$photo_obj=new img_manup("$upload_path");

function get_pro_img($id)
{
	global $db_object;
	$sql = $db_object->return_query("SELECT * from fw_product_master where product_id=$id");
	return $sql["product_img"];
}

function get_menu_id($id)
{
	global $db_object;
	//$sql 	=	$db_object->return_query("SELECT * from fw_menu_master where menu_id=$id");
	$result_arr = array('s');
	$result_arr_val = array($id);
	$result_cat     =   $db_object->execute_select("SELECT * from fw_menu_master where menu_id=$1",$result_arr,$result_arr_val);
	if(pg_num_rows($result_cat) === 0) return $id;;
    while($sql       =   pg_fetch_array($result_cat))
	{
		if($sql["parent_menu"] > 0)
		{
			return $sql["parent_menu"];
		}
		else
		{
			return $id;
		}
	}
}

if(isset($_REQUEST["action"]))
{	
	$sqlhelper = new sqlhelper("fw_content_master");
	//$sqlhelper->db_connect($host,$username,$password,$database);
	$date = new DateTime("now");
	$dateTime = $date->format("Y-m-d H:m:s");
	
	switch($_REQUEST["myaction"])
	{
		case md5("cabdfdc"):
			
			$result_arr = array('i');
			$result_arr_val = array($_REQUEST["menu_id"]);
			$result_cat		= $db_object->execute_select($fetch_content_cnt,$result_arr,$result_arr_val);
			while($rows       =   pg_fetch_array($result_cat))
			{
				$total_rows = $rows["con_cnt"];
			}	

			if($total_rows > 0)
			{ 
				echo "<script type='text/javascript'>alert('Content already present for selected menu,Please try again'); window.location.assign('../manage-content')</script>";	
			}
			
			else{
			
			//$verify_token
			$result_arr = array('s','s');
			$result_arr_val = array($_SESSION["admin_id"],$_REQUEST["token"]);
			$result_tkn = $db_object->execute_select($verify_token,$result_arr,$result_arr_val);

			if(pg_num_rows($result_tkn) > 0) {
				$menu_id				=	$_REQUEST["menu_id"];
				$parent_menu_id			=	get_menu_id($menu_id);
				$page_name				=	$sqlhelper->cleanText(trim($_REQUEST["page_name"]));
				$page_title				=	$sqlhelper->cleanText(trim($_REQUEST["page_title"]));
				$content_title_eng		=	$sqlhelper->cleanText(trim($_REQUEST["content_title_eng"]));
				$content_title_hindi	=	$sqlhelper->cleanURL(trim(htmlspecialchars($_REQUEST["content_title_hindi"],ENT_QUOTES,"UTF-8")));
				//$main_content_eng		=	$sqlhelper->cleanURL(trim($_REQUEST["main_content_eng"]));
				//$main_content_hindi		=	$sqlhelper->cleanURL(trim($_REQUEST["main_content_hindi"]));
				//$meta_keywords			=	$sqlhelper->cleanText(trim($_REQUEST["meta_keywords"]));
				//$meta_desc				=	$sqlhelper->cleanText(trim($_REQUEST["meta_desc"]));
				$options				=	$sqlhelper->cleanText(trim($_REQUEST["options"]));

				$character = array("javascript", "script","alert","window.location","iframe","window","document.cookie");
		   		$main_content_eng = str_replace($character,".",trim($_REQUEST["main_content_eng"]));
		   		$main_content_hindi = str_replace($character,".",trim($_REQUEST["main_content_hindi"]));

		   		$key_regex = preg_match('/^[a-zA-Z, ]*$/', $_REQUEST["meta_keywords"]);
		       		$desc_regex = preg_match('/^[a-zA-Z, ]*$/', $_REQUEST["meta_desc"]);
			        if($key_regex==1 && $desc_regex==1)
			            {
	            	
			   		$meta_keywords			=	trim($_REQUEST["meta_keywords"]);
					$meta_desc				=	trim($_REQUEST["meta_desc"]);
	           		 }
			        else{
			            echo "<script type='text/javascript'>alert('Please insert proper Meta keyword & Description');window.location.assign('../manage-content')</script>";
			            exit();
		       		 }
		   		

				$photoname = date("dmYHis").rand(0,50);
				$uploadOk = 1;
					$imageFileType = strtolower(end(explode('.',$_FILES['photo']['name'])));;
					$photoname = $photo_name.".".$imageFileType;
					$target_file_path ="../../".$upload_path . $photoname;
					// Check if image file is a actual image or fake image
					  $check = getimagesize($_FILES["photo"]["tmp_name"]);
					  if($check !== false) {
						$uploadOk = 1;
					  } else {
						$uploadOk = 0;
					  }
					// Check if file already exists
					if (file_exists($target_file_path)) {
					  $uploadOk = 0;
					}

					if ($_FILES["photo"]["size"] > 500000) {
					  $uploadOk = 0;
					}

					// Allow certain file formats
					if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"  && $imageFileType != "gif" ) {
					  $uploadOk = 0;
					}

					// Check if $uploadOk is set to 0 by an error
					if ($uploadOk == 0) {
						$photoname = 'N';
					} else {
					  if (move_uploaded_file($_FILES["photo"]["tmp_name"], $target_file_path)) {
					  } else {
					  	echo "Sorry, there was an error uploading your file.";
					
					  }
					}
				
				//if ($upload_file == true){
					if(isset($_REQUEST["start_date"]) && $_REQUEST["start_date"]!="")
					{
						$start_date				=	date("Y-m-d",strtotime($_REQUEST["start_date"]));
						$end_date				=	date("Y-m-d",strtotime($_REQUEST["end_date"]));
						
					 	$result_arr = array('i','i','s','s','s','s','s','s','s','s','s','i','s','s','s');
						$result_arr_val = array($menu_id,$parent_menu_id, $photoname, $page_name, $page_title,$content_title_eng, $content_title_hindi,$main_content_eng, $main_content_hindi, $meta_keywords, $meta_desc,$options,$start_date,$end_date,$dateTime);
						if(pg_num_rows($db_object->execute_select($fetch_insert_content,$result_arr,$result_arr_val)) > 0 ){
							echo "<script type='text/javascript'>alert('Data added successfully');window.location.assign('../manage-conten')</script>";
						}
						else{
							echo "<script type='text/javascript'>alert('Error while Insert data.');window.location.assign('../manage-content')</script>";	
						}	

					}
					else if($_REQUEST["start_date"]=="" && $_REQUEST["start_date"]=="")
					{
						$result_arr = array('i','i','s','s','s','s','s','s','s','s','s','i','s');
						$result_arr_val = array($menu_id,$parent_menu_id, $photoname, $page_name, $page_title,$content_title_eng, $content_title_hindi,$main_content_eng, $main_content_hindi, $meta_keywords, $meta_desc,$options,$dateTime);
						$db_object->execute_select($fetch_insert_content_13,$result_arr,$result_arr_val);
						/*echo $fetch_insert_content_13;
						print_r($result_arr_val);
						if(pg_num_rows($db_object->execute_select($fetch_insert_content_13,$result_arr,$result_arr_val)) > 0 ){
							echo "<script type='text/javascript'>alert('Data added successfully');window.location.assign('../manage-content')</script>";
						}
						else{
							//echo "<script type='text/javascript'>alert('Error while Insert data..');window.location.assign('../manage-content')</script>";	
							echo "error capture"; exit();
						}*/
						echo "<script type='text/javascript'>alert('Data added successfully');window.location.assign('../manage-content')</script>";
			
					}
					else
					{

						$result_arr = array('i','i','s','s','s','s','s','s','s','s','s','i','s');
						$result_arr_val = array($menu_id,$parent_menu_id, $photoname, $page_name, $page_title,$content_title_eng, $content_title_hindi,$main_content_eng, $main_content_hindi, $meta_keywords, $meta_desc,$options,$dateTime);
						if($db_object->execute_select($fetch_insert_content_13,$result_arr,$result_arr_val) > 0 ){
							echo "<script type='text/javascript'>alert('Data added successfully');window.location.assign('../manage-content')</script>";
						}
						else{
							echo "<script type='text/javascript'>alert('Error while Insert data...');window.location.assign('../manage-content')</script>";	
						}	
					}

					//echo "<script type='text/javascript'>alert('Data added successfully');window.location.assign('../manage-content.php')</script>";
				/*}
				else{
						$upload_file = true;  //---- file validation fail. 
						echo "<script type='text/javascript'>alert('Please provide valid file');window.location.assign('../create-content')</script>";	
					}*/
			}
			else{
				echo "<script type='text/javascript'>alert('Something went wrong! Please try again');window.location.assign('../manage-content')</script>";
			}
		}
		break;
		
		case md5("ecdbiftc"):
			//$verify_token
			$result_arr = array('s','s');
			$result_arr_val = array($_SESSION["admin_id"],$_REQUEST["token"]);
			$result_tkn = $db_object->execute_select($verify_token,$result_arr,$result_arr_val);

			if(pg_num_rows($result_tkn) > 0) {
					$id						=	$_REQUEST["f9c7a57c74dcc509"];
					$menu_id				=	$_REQUEST["menu_id"];
					$parent_menu_id			=	get_menu_id($menu_id);
					//die($parent_menu_id);
					$page_name				=	$sqlhelper->cleanText(trim($_REQUEST["page_name"]));
					$page_title				=	$sqlhelper->cleanText(trim($_REQUEST["page_title"]));
					$content_title_eng		=	$sqlhelper->cleanText(trim($_REQUEST["content_title_eng"]));
					$content_title_hindi	=	$sqlhelper->cleanURL(trim(htmlspecialchars($_REQUEST["content_title_hindi"],ENT_QUOTES,"UTF-8")));
					//$main_content_eng		=	$sqlhelper->cleanURL(trim($_REQUEST["main_content_eng"]));
					//$main_content_hindi		=	$sqlhelper->cleanURL(trim($_REQUEST["main_content_hindi"]));
					//$meta_keywords			=	$sqlhelper->cleanText(trim($_REQUEST["meta_keywords"]));
					//$meta_desc				=	$sqlhelper->cleanText(trim($_REQUEST["meta_desc"]));
					$options				=	$sqlhelper->cleanText(trim($_REQUEST["options"]));


					$character = array("javascript", "script","alert","window.location","iframe","window","document.cookie");
			   		$main_content_eng = str_replace($character,".",trim($_REQUEST["main_content_eng"]));
			   		$main_content_hindi = str_replace($character,".",trim($_REQUEST["main_content_hindi"]));


					$key_regex = preg_match('/^[a-zA-Z, ]*$/', $_REQUEST["meta_keywords"]);
		        		$desc_regex = preg_match('/^[a-zA-Z, ]*$/', $_REQUEST["meta_desc"]);
				        if($key_regex==1 && $desc_regex==1)
	        			    {
	            	
			   			$meta_keywords			=	trim($_REQUEST["meta_keywords"]);
						$meta_desc				=	trim($_REQUEST["meta_desc"]);
				            }
		       			 else{
				            echo "<script type='text/javascript'>alert('Please insert proper Meta keyword & Description');window.location.assign('../manage-content')</script>";
				            exit();
		       			 }

					$photo_name = date("dmYHis").rand(0,50);
					
					$uploadOk = 1;
					$imageFileType = strtolower(end(explode('.',$_FILES['photo']['name'])));;
					$photoname = $photo_name.".".$imageFileType;
					$target_file_path ="../../".$upload_path . $photoname;
					// Check if image file is a actual image or fake image
					  $check = getimagesize($_FILES["photo"]["tmp_name"]);
					  if($check !== false) {
						$uploadOk = 1;
					  } else {
						$uploadOk = 0;
					  }
					// Check if file already exists
					if (file_exists($target_file_path)) {
					  $uploadOk = 0;
					}

					if ($_FILES["photo"]["size"] > 500000) {
					  $uploadOk = 0;
					}

					// Allow certain file formats
					if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"  && $imageFileType != "gif" ) {
					  $uploadOk = 0;
					}

					// Check if $uploadOk is set to 0 by an error
					if ($uploadOk == 0) {
						$photoname = 'N';
					} else {
					  if (move_uploaded_file($_FILES["photo"]["tmp_name"], $target_file_path)) {
					  	
					  } else {
					  	
					  }
					}
				
					if(isset($_REQUEST["start_date"]) && $_REQUEST["start_date"]!="")
					{
						$start_date				=	date("Y-m-d",strtotime($_REQUEST["start_date"]));
						$end_date				=	date("Y-m-d",strtotime($_REQUEST["end_date"]));
						
						//Archive content
						$result_arr = array('s');
						$result_arr_val = array($id);						
						$db_object->execute_select($content_archive,$result_arr,$result_arr_val);
						
						
						$result_arr = array('i','i','s','s','s','s','s','s','s','s','s','i','s','s','s','s');
						$result_arr_val = array($menu_id,$parent_menu_id,$photoname,$page_name,$page_title,$content_title_eng,$content_title_hindi,$main_content_eng,$main_content_hindi,$meta_keywords,$meta_desc,$options,$start_date,$end_date,$dateTime,$id);
						
						$db_object->execute_select($update_content_14,$result_arr,$result_arr_val);

					}
					else if($_REQUEST["start_date"]=="" && $_REQUEST["start_date"]=="")
					{
						//Archive content
						$result_arr = array('s');
						$result_arr_val = array($id);						
						$db_object->execute_select($content_archive,$result_arr,$result_arr_val);

						$result_arr = array('i','i','s','s','s','s','s','s','s','s','s','i','s','s');
						$result_arr_val = array($menu_id,$parent_menu_id,$photoname,$page_name,$page_title,$content_title_eng,$content_title_hindi,$main_content_eng,$main_content_hindi,$meta_keywords,$meta_desc,$options,$dateTime,$id);						
						$db_object->execute_select($update_content,$result_arr,$result_arr_val);
					}
					else
					{
						//Archive content
						$result_arr = array('s');
						$result_arr_val = array($id);						
						$db_object->execute_select($content_archive,$result_arr,$result_arr_val);

						$result_arr = array('i','i','s','s','s','s','s','s','s','s','s','i','s','s');
						$result_arr_val = array($menu_id,$parent_menu_id,$photoname,$page_name,$page_title,$content_title_eng,$content_title_hindi,$main_content_eng,$main_content_hindi,$meta_keywords,$meta_desc,$options,$dateTime,$id);
						$db_object->execute_select($update_content_14,$result_arr,$result_arr_val);
					}
					
					
				echo "<script type='text/javascript'>alert('Data updated successfully');window.location.assign('../manage-content')</script>";
			}
			else{
				echo "<script type='text/javascript'>alert('Something went wrong! Please try again');window.location.assign('../manage-content')</script>";
			}	
		break;
		
		case md5("Dceblfecte"):
			$id				=	$_REQUEST["f9c7a57c74dcc509"];
			$result_arr = array('i');
			$result_arr_val = array($_REQUEST["f9c7a57c74dcc509"]);
			$where = "md5(cid) = '".$id."'";			
			if ($db_object->execute_check($fetch_edit_content, $result_arr, $result_arr_val) > 0)
			{
				//$db_object->execute_query("DELETE from fw_content_master where md5(cid) = '".$id."'");
				$db_object->execute_select($delete_content,$result_arr, $result_arr_val);
				echo "<script type='text/javascript'>alert('Data deleted successfully');window.location.assign('../manage-content')</script>";
			}
			else{
				echo "<script type='text/javascript'>alert('Content does not exist');window.location.assign('../manage-content')</script>";	
			}
		break;
	}
}
?>

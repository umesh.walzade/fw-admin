<?php
include("global/user_global.php");
check_login();
$page		=	"dashboard";
$sub_page	=	"dashboard";

/*$result_cat = $db_object->execute_select($fetch_online_cnt,'1','1');
$row = $result_cat->fetch_array(MYSQLI_ASSOC);
$total_user = $row["cnt"];

$date				=	date('Y-m-d');
$total_order		=	mysqli_num_rows($db_object->execute_select($fetch_online_dt,'s',$date));
$total_product		=	mysqli_num_rows($db_object->execute_select($fetch_menu_cnt,'1','1'));
*/
?>
<!DOCTYPE html>
<html lang="en" class="app">

<head>
    <meta charset="utf-8" />
    <title>Welcome</title>
    <meta name="description" content="app, web app, responsive, admin dashboard, admin, flat, flat ui, ui kit, off screen nav" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="stylesheet" href="css/app.v2.css" type="text/css" />
    <link rel="stylesheet" href="css/font.css" type="text/css" cache="false" />
    <link rel="stylesheet" href="js/calendar/bootstrap_calendar.css" type="text/css" cache="false" />
    <!--[if lt IE 9]> <script src="js/ie/html5shiv.js" cache="false"></script> <script src="js/ie/respond.min.js" cache="false"></script> <script src="js/ie/excanvas.js" cache="false"></script> <![endif]-->
</head>

<body>
    <section class="vbox">
        <?php include("include/topbar.php") ?>
        <section>
            <section class="hbox stretch">
                <!-- .aside -->
                <?php include("include/sidebar.php") ?>
                <!-- /.aside -->
                <section id="content">
                    <section class="vbox">
                        <section class="scrollable padder">
                            <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                                <li><a href="dashboard.php"><i class="fa fa-home"></i> Home</a>
                                </li>
                                <li class="active">Dashboard</li>
                            </ul>
                            <div class="m-b-md">
                                <h3 class="m-b-none">Dashboard</h3>
                                <small>Welcome back, <?php echo $_SESSION["admin_name"] ?></small>
                            </div>
                            <section class="panel panel-default">
                                <div class="row m-l-none m-r-none bg-light lter">
                                    <div class="col-sm-6 col-md-4 padder-v b-r b-light"> <span class="fa-stack fa-2x pull-left m-r-sm"> <i class="fa fa-circle fa-stack-2x text-info"></i> <i class="fa fa-group fa-stack-1x text-white"></i><span class="easypiechart pos-abt" data-percent="00" data-line-width="4" data-track-Color="#fff" data-scale-Color="false" data-size="50" data-line-cap='butt' data-animate="1000" data-target="#users"></span> </span>
                                        <a class="clear" href="#"> <span class="h3 block m-t-xs"><strong id="users"><?php echo ($total_user) ?></strong></span> <small class="text-muted text-uc">Total Visit</small> </a>
                                    </div>
                                    <div class="col-sm-6 col-md-4 padder-v b-r b-light lt"> <span class="fa-stack fa-2x pull-left m-r-sm"> <i class="fa fa-circle fa-stack-2x text-warning"></i> <i class="fa fa-shopping-cart fa-stack-1x text-white"></i> <span class="easypiechart pos-abt" data-percent="00" data-line-width="4" data-track-Color="#fff" data-scale-Color="false" data-size="50" data-line-cap='butt' data-animate="1000" data-target="#bugs"></span> </span>
                                        <a class="clear" href="#"> <span class="h3 block m-t-xs"><strong id="bugs"><?php echo ($total_order) ?></strong></span> <small class="text-muted text-uc">Today Visits</small> </a>
                                    </div>
                                    <div class="col-sm-6 col-md-4 padder-v b-r b-light"> <span class="fa-stack fa-2x pull-left m-r-sm"> <i class="fa fa-circle fa-stack-2x text-danger"></i> <i class="fa fa-tags fa-stack-1x text-white"></i> <span class="easypiechart pos-abt" data-percent="00" data-line-width="4" data-track-Color="#f5f5f5" data-scale-Color="false" data-size="50" data-line-cap='butt' data-animate="2000" data-target="#firers"></span> </span>
                                        <a class="clear" href="#"> <span class="h3 block m-t-xs"><strong id="firers"><?php echo ($total_product) ?></strong></span> <small class="text-muted text-uc">Total Pages</small> </a>
                                    </div>
                                </div>
                            </section>
                        </section>
                    </section>
                    <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav"></a>
                </section>
                <aside class="bg-light lter b-l aside-md hide" id="notes">
                    <div class="wrapper">Notification</div>
                </aside>
            </section>
        </section>
    </section>
    <script src="js/app.v2.js"></script>
    <!-- Bootstrap -->
    <!-- App -->
    <script src="js/charts/easypiechart/jquery.easy-pie-chart.js" cache="false"></script>
    <script src="js/charts/sparkline/jquery.sparkline.min.js" cache="false"></script>
    <script src="js/charts/flot/jquery.flot.min.js" cache="false"></script>
    <script src="js/charts/flot/jquery.flot.tooltip.min.js" cache="false"></script>
    <script src="js/charts/flot/jquery.flot.resize.js" cache="false"></script>
    <script src="js/charts/flot/jquery.flot.grow.js" cache="false"></script>
    <script src="js/charts/flot/demo.js" cache="false"></script>
    <script src="js/calendar/bootstrap_calendar.js" cache="false"></script>
    <script src="js/calendar/demo.js" cache="false"></script>
    <script src="js/sortable/jquery.sortable.js" cache="false"></script>
</body>

</html>
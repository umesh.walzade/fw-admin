<aside class="bg-black lter aside-md hidden-print" id="nav">
                    <section class="vbox">
                        <section class="w-f scrollable">
                            <div class="slim-scroll" data-height="auto" data-disable-fade-out="true" data-distance="0" data-size="5px" data-color="#333333">
                                <!-- nav -->
                                <nav class="nav-primary hidden-xs">
                                    <ul class="nav">
                                        <li <?php //echo $db_object->return_compare("dashboard","$sub_page",'class="active"',"") ?>>
                                            <a href="dashboard" <?php //echo $db_object->return_compare("dashboard","$page",'class="active"',"") ?>>
                                                <i class="fa fa-dashboard icon">
                                                <b class="bg-danger"></b>
                                            </i>
                                                <span>Dashboard</span>
                                            </a>
                                        </li>
                                        <li <?php //echo $db_object->return_compare("menu","$page",'class="active"',"") ?>>
                                            <a href="#layout" >
                                                <i class="fa fa-columns icon">
                                                <b class="bg-warning"></b>
                                            </i>
                                                <span class="pull-right">
                                                <i class="fa fa-angle-down text"></i>
                                                <i class="fa fa-angle-up text-active"></i>
                                            </span>
                                                <span>Menu</span>
                                            </a>
                                            <ul class="nav lt">
                                                <li <?php //echo $db_object->return_compare("manage-menu","$sub_page",'class="active"',"") ?>>
                                                    <a href="manage-menu">
                                                        <i class="fa fa-angle-right"></i>
                                                        <span>Manage menu</span>
                                                    </a>
                                                </li>
                                                </ul>
                                        </li>
                                        <li <?php //echo $db_object->return_compare("content","$page",'class="active"',"") ?>>
                                            <a href="#uikit">
                                                <i class="fa fa-flask icon">
                                                <b class="bg-success"></b>
                                            </i>
                                                <span class="pull-right">
                                                <i class="fa fa-angle-down text"></i>
                                                <i class="fa fa-angle-up text-active"></i>
                                            </span>
                                                <span>Content</span>
                                            </a>
                                            <ul class="nav lt">
                                                <li <?php //echo $db_object->return_compare("manage-content","$sub_page",'class="active"',"") ?>>
                                                    <a href="manage-content">
                                                        <i class="fa fa-angle-right"></i>
                                                        <span>Manage Content</span>
                                                    </a>
                                                </li>
                                               <?php /*
                                                <li <?php //echo $db_object->return_compare("manage-news","$sub_page",'class="active"',"") ?>>
                                                    <a href="manage-news">
                                                        <i class="fa fa-angle-right"></i>
                                                        <span>Manage News</span>
                                                    </a>
                                                </li>
                                                <li <?php //echo $db_object->return_compare("manage-certificate","$sub_page",'class="active"',"") ?>>
                                                    <a href="manage-certificate">
                                                        <i class="fa fa-angle-right"></i>
                                                        <span>Manage Certificate</span>
                                                    </a>
                                                </li> */?>
                                            </ul>
                                        </li>
                                        <li <?php //echo $db_object->return_compare("Gallery","$page",'class="active"',"") ?>>
                                            <a href="#uikit">
                                                <i class="fa fa-file-text icon">
                                                <b class="bg-warning"></b>
                                            </i>
                                                <span class="pull-right">
                                                <i class="fa fa-angle-down text"></i>
                                                <i class="fa fa-angle-up text-active"></i>
                                            </span>
                                                <span>Galleries</span>
                                            </a>
                                            <ul class="nav lt">
                                                <?php /*<li <?php //echo $db_object->return_compare("manage-gallery","$sub_page",'class="active"',"") ?>>
                                                    <a href="manage-gallery">
                                                        <i class="fa fa-angle-right"></i>
                                                        <span>Manage Gallery</span>
                                                    </a>
                                                </li>*/?>
                                                <li <?php //echo $db_object->return_compare("manage-slider","$sub_page",'class="active"',"") ?>>
                                                    <a href="manage-slider">
                                                        <i class="fa fa-angle-right"></i>
                                                        <span>Manage Gallery</span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>
					<li <?php //echo $db_object->return_compare("Gallery","$page",'class="active"',"") ?>>
                                            <a href="#uikit">
                                                <i class="fa fa-spinner icon">
                                                <b class="bg-success"></b>
                                            </i>
                                                <span class="pull-right">
                                                <i class="fa fa-angle-down text"></i>
                                                <i class="fa fa-angle-up text-active"></i>
                                            </span>
                                                <span>FAQ</span>
                                            </a>
                                            <ul class="nav lt">
                                                <li <?php //echo $db_object->return_compare("manage-gallery","$sub_page",'class="active"',"") ?>>
                                                    <a href="manage-faq-type">
                                                        <i class="fa fa-angle-right"></i>
                                                        <span>FAQ Category</span>
                                                    </a>
                                                </li>
                                                <li <?php //echo $db_object->return_compare("manage-slider","$sub_page",'class="active"',"") ?>>
                                                    <a href="manage-faq">
                                                        <i class="fa fa-angle-right"></i>
                                                        <span>Manage FAQ</span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>
                                        
                                        <li <?php  //echo $db_object->return_compare("data","$page",'class="active"',"") ?>>
                                            <a href="#uikit">
                                                <i class="fa fa-users icon">
                                                <b class="bg-success"></b>
                                            </i>
                                                <span class="pull-right">
                                                <i class="fa fa-angle-down text"></i>
                                                <i class="fa fa-angle-up text-active"></i>
                                            </span>
                                                <span>Feedback</span>
                                            </a>
                                            <ul class="nav lt">
                                                <li <?php //echo $db_object->return_compare("feedback-data","$sub_page",'class="active"',"") ?>>
                                                    <a href="manage-feedback">
                                                        <i class="fa fa-angle-right"></i>
                                                        <span>Feedback Data</span>
                                                    </a>
                                                </li>                                                
                                            </ul>
                                        </li>                                        
										<li <?php //echo $db_object->return_compare("Archive","$page",'class="active"',"") ?>>
                                            <a href="#uikit">
                                                <i class="fa fa-archive icon">
                                                <b class="bg-success"></b>
                                            </i>
                                                <span class="pull-right">
                                                <i class="fa fa-angle-down text"></i>
                                                <i class="fa fa-angle-up text-active"></i>
                                            </span>
                                                <span>Last Update Date</span>
                                            </a>
                                            <ul class="nav lt">
                                                <li <?php //echo $db_object->return_compare("manage-archive","$sub_page",'class="active"',"") ?>>
                                                    <a href="manage-update.php">
                                                        <i class="fa fa-angle-right"></i>
                                                        <span>Manage Update Date</span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li <?php //echo $db_object->return_compare("Gallery","$page",'class="active"',"") ?>>
                                            <a href="#uikit">
                                                <i class="fa fa-file-text icon">
                                                <b class="bg-warning"></b>
                                            </i>
                                                <span class="pull-right">
                                                <i class="fa fa-angle-down text"></i>
                                                <i class="fa fa-angle-up text-active"></i>
                                            </span>
                                                <span>Photo Gallery</span>
                                            </a>
                                            <ul class="nav lt">
                                                <?php /*<li <?php //echo $db_object->return_compare("manage-gallery","$sub_page",'class="active"',"") ?>>
                                                    <a href="manage-gallery">
                                                        <i class="fa fa-angle-right"></i>
                                                        <span>Manage Gallery</span>
                                                    </a>
                                                </li>*/?>
                                                <li <?php //echo $db_object->return_compare("manage-slider","$sub_page",'class="active"',"") ?>>
                                                    <a href="manage-gallery.php">
                                                        <i class="fa fa-angle-right"></i>
                                                        <span>Manage Photo Gallery</span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>
                                        <!-- <li> 
                                            <a href="#uikit">
                                                <i class="fa fa-archive icon">
                                                <b class="bg-success"></b>
                                            </i>
                                                <span class="pull-right">
                                                <i class="fa fa-angle-down text"></i>
                                                <i class="fa fa-angle-up text-active"></i>
                                            </span>
                                                <span>Tabs</span>
                                            </a>
                                            <ul class="nav lt">
                                                <li <?php //echo $db_object->return_compare("manage-archive","$sub_page",'class="active"',"") ?>>
                                                    <a href="manage-tab.php">
                                                        <i class="fa fa-angle-right"></i>
                                                        <span>Manage Tab</span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </li> -->

                                        <?php /*<li  //echo $db_object->return_compare("Media","$page",'class="active"',"") ?>>
                                            <a href="#uikit">
                                                <i class="fa fa-file-text icon">
                                                <b class="bg-warning"></b>
                                            </i>
                                                <span class="pull-right">
                                                <i class="fa fa-angle-down text"></i>
                                                <i class="fa fa-angle-up text-active"></i>
                                            </span>
                                                <span>Media Upload</span>
                                            </a>
                                            <ul class="nav lt">
                                                <li <?php //echo $db_object->return_compare("media-upload","$sub_page",'class="active"',"") ?>>
                                                    <a href="media-upload">
                                                        <i class="fa fa-angle-right"></i>
                                                        <span>Manage Media</span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>
                                        */?>
                                    </ul>
                                </nav>
                                <!-- / nav -->
                            </div>
                        </section>
                        
                    </section>
                </aside>
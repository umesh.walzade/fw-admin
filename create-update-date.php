<?php
include("global/user_global.php");
check_login();
$page       =   "tab";
$sub_page   =   "manage-update";

$result_arr = array('s');
$result_arr_val = array(md5($_SESSION["admin_id"]));
$result_tkn         =   $db_object->execute_select($fetch_token,$result_arr,$result_arr_val);
while($rows_tkn       =   pg_fetch_array($result_tkn)){
    $admin_tkn   =   $rows_tkn["token"];
}

if(isset($_REQUEST["f9c7a57c74dcc509"]))
{
    $id                 =   $db_object->sanatize_value($_REQUEST["f9c7a57c74dcc509"]);
    /*if( ! filter_var($_REQUEST["f9c7a57c74dcc509"], FILTER_VALIDATE_INT) )
    {
    redirect('manage-menu');
    }*/

    //mysqli_query('SET character_set_results=utf8');

    $result_arr = array('s');
    $result_arr_val = array($_REQUEST["f9c7a57c74dcc509"]);
    $result_cat             =   $db_object->execute_select($fetch_edit_date,$result_arr,$result_arr_val);
    if(pg_num_rows($result_cat) === 0) exit('No rows');
    while($sql       =   pg_fetch_array($result_cat)){
        $myaction           =   "ecdbiftc";
        $label              =   "Edit";
        $title           =   $sql["title"];
        $lst_dt           =   $sql["lst_dt"];
    }
}
else
{
    $myaction           =   "cabdfdc";
    $id                 =   "";
    $label              =   "Create";
    $title              =   "";
    $lst_dt             =   "";
}

?>
<!DOCTYPE html>
<html lang="en" class="app">
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <title>Welcome</title>
    <link rel="icon" href="images/favicon.ico">
    <meta name="description" content="app, web app, responsive, admin dashboard, admin, flat, flat ui, ui kit, off screen nav" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="stylesheet" href="css/app.v2.css" type="text/css" />
    <link rel="stylesheet" href="css/font.css" type="text/css" cache="false" />
    <link rel="stylesheet" href="js/calendar/bootstrap_calendar.css" type="text/css" cache="false" />
    <link rel="stylesheet" href="js/datepicker/datepicker.css" type="text/css" cache="false" />
    <link rel="stylesheet" href="js/datatables/datatables.css" type="text/css" cache="false" />
    <script src="js/jQuery-3.5.1.min.js"></script>
    <script src="ckeditor/ckeditor.js"></script>
<style>
textarea{
    width:100%;
}
</style>
    <!--[if lt IE 9]> <script src="js/ie/html5shiv.js" cache="false"></script> <script src="js/ie/respond.min.js" cache="false"></script> <script src="js/ie/excanvas.js" cache="false"></script> <![endif]-->
</head>

<body>
    <section class="vbox">
        <?php include("include/topbar.php") ?>
        <section>
            <section class="hbox stretch">
                <!-- .aside -->
                <?php include("include/sidebar.php") ?>
                <!-- /.aside -->
                <section id="content">
                    <section class="vbox">
                        <section class="scrollable padder">
                            <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                                <li><a href="index.html"><i class="fa fa-home"></i> Home</a></li>
                                <li><a href="add-product">Content</a></li>
                                <li class="active"><?php echo $label ?> Content</li>
                            </ul>
                            <div class="m-b-md">
                                <h3 class="m-b-none"><?php echo $label ?> Content</h3>
                                <small>Welcome back, <?php echo $_SESSION["admin_name"] ?></small>
                                <input type="button" class="btn btn-danger pull-right" value="Back" onClick="location.href='manage-update.php'">
                            </div>
                            <form name="myform" id="myform" method="post" action="actions/create-update-date.php" class="form-horizontal" enctype="multipart/form-data">
                            <input type="hidden" name="myaction" value="<?php echo md5($myaction) ?>">
                            <input type="hidden" name="token" value="<?php echo $admin_tkn; ?>">
                            <input type="hidden" name="f9c7a57c74dcc509" value='<?php echo $_REQUEST["f9c7a57c74dcc509"]?>'>
                                <section class="panel panel-default">
                                   <header class="panel-heading font-bold"><?php echo $label ?> Content</header>
                                   <div class="panel-body">
                                        <div class="form-group"> <label class="col-sm-2 control-label">Last Update Date</label>
                                            <div class="col-sm-10"> 
                                            <?php 
                                              if(isset($lst_dt) && $lst_dt!='')
                                                $update_date = date('d-m-Y',strtotime($lst_dt));
                                              else  
                                                $update_date = '';
                                            ?>
                                                <input type="text" autocomplete="off" name="lst_dt" id="lst_dt" class="datepicker datepicker-input" placeholder="dd-mm-yyyy" data-date-format="dd-mm-yyyy" value="<?php echo $update_date ?>">
                                            </div>
                                        </div>
                                        
                                        <div id="showdate">
                                        <div class="line line-dashed line-lg pull-in"></div>
                                        <div class="form-group"> <label class="col-sm-2 control-label">Description</label>
                                            <div class="col-sm-10"> 
                                                <input type="text" name="lst_dt_desc" autocomplete="off" id="lst_dt_desc" value="<?php echo $title ?>" <?php if($label=="Edit") { ?> readonly <?php } ?>>
                                            </div>
                                        </div> 
                                        
                                        <div class="line line-dashed line-lg pull-in"></div>
                                        <div class="form-group">  <label class="col-sm-2 control-label"></label>
                                            <div class="col-sm-10"><input type="submit" name="action" class="btn btn-success btn-default" value="Submit"></div>
                                        </div>                                        
                                   </div>
                                </section>
                            </form>
                        </section>
                    </section>
                    <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav"></a>
                </section>
                <aside class="bg-light lter b-l aside-md hide" id="notes">
                    <div class="wrapper">Notification</div>
                </aside>
            </section>
        </section>
    </section>
    <script src="js/app.v2.js"></script>
    <!-- Bootstrap -->
    <!-- App -->
    <script src="js/sortable/jquery.sortable.js" cache="false"></script>
    <script src="js/datatables/jquery.dataTables.min.js" cache="false"></script>
    <script src="js/datepicker/bootstrap-datepicker.js" cache="false"></script>
    <script type='text/javascript'>
        $(document).ready(function(){
        //alert('testing insert');
        /* $('#lst_dt').datepicker({
          dateFormat: "dd-mm-yyyy",
          maxDate:'+10d',
          minDate: -10

         });*/
        //$("#lst_dt").datepicker('option', 'minDate', selectedDate || '2021-01-03');
        //$('.datepicker').data('DateTimePicker').minDate('10/01/2021')
        $(".datepicker").datepicker({maxDate: '0'});

        });
    </script>
    
</body>

</html>
<?php
include("global/user_global.php");
check_login();
$page		=	"menu";
$sub_page	=	"manage-menu";



$result_arr = array('s');
$result_arr_val = array(md5($_SESSION["admin_id"]));
$result_tkn         =   $db_object->execute_select($fetch_token,$result_arr,$result_arr_val);
while($rows_tkn       =   pg_fetch_array($result_tkn)){
    $admin_tkn   =   $rows_tkn["token"];
}

if(isset($_REQUEST["f9c7a57c74dcc509"]))
{
	$id					=	$db_object->sanatize_value($_REQUEST["f9c7a57c74dcc509"]);
	/*if( ! filter_var($_REQUEST["f9c7a57c74dcc509"], FILTER_VALIDATE_INT) )
	{
  	redirect('manage-menu');
	}*/

    //mysqli_query('SET character_set_results=utf8');

$result_arr = array('s');
$result_arr_val = array($_REQUEST["f9c7a57c74dcc509"]);
	$result_cat				=	$db_object->execute_select($fetch_edit_faq,$result_arr,$result_arr_val);
    if(pg_num_rows($result_cat)=== 0) exit('No rows');
    while($sql       =   pg_fetch_array($result_cat)){
    	$myaction			=	"ecdbiftc";
    	
    	$label				=	"Edit";
    	$parent_id			=	$sql["faq_cat_parent"];
    	$category_id		=	$sql["faq_cat_id"];
    	$category_name		=	$sql["faq_category_name"];
    	$question			=	$sql["faq_question"];
    	$ans				=	$sql["faq_ans"];
    	$flag				=	$sql["sc_flg"];
        $parent_name        =   $sql["parent_name"];
    }
}
else
{
	$cat_name			=	"";
	$myaction			=	"cabdfdc";
	$id					=	"";
	$label				=	"Create";
	$parent_id			=	"";
	$category_id		=	"";
	$question			=	"";
	$ans				=	"";
	$flag				=	"1";	
}

?>
<!DOCTYPE html>
<html lang="en" class="app">
<head>
    <meta charset="utf-8" />
    <title>Create FAQ</title>
    <link rel="icon" href="images/favicon.ico">
    <meta name="description" content="app, web app, responsive, admin dashboard, admin, flat, flat ui, ui kit, off screen nav" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="stylesheet" href="css/app.v2.css" type="text/css" />
    <link rel="stylesheet" href="css/font.css" type="text/css" cache="false" />
    <link rel="stylesheet" href="js/calendar/bootstrap_calendar.css" type="text/css" cache="false" />
    <link rel="stylesheet" href="js/datatables/datatables.css" type="text/css" cache="false" />
    
    <!--[if lt IE 9]> <script src="js/ie/html5shiv.js" cache="false"></script> <script src="js/ie/respond.min.js" cache="false"></script> <script src="js/ie/excanvas.js" cache="false"></script> <![endif]-->
</head>

<body>
    <section class="vbox">
        <?php include("include/latest_js.php") ?>
        <?php include("include/topbar.php") ?>
        <section>
            <section class="hbox stretch">
                <!-- .aside -->
                <?php include("include/sidebar.php") ?>
                <!-- /.aside -->
                <section id="content">
                    <section class="vbox">
                        <section class="scrollable padder">
                            <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                                <li><a href="index.html"><i class="fa fa-home"></i> Home</a></li>
                                <li><a href="manage-menu">Manage FAQ</a></li>
                                <li><a href="manage-menu">Manage FAQ</a></li>
                                <li class="active"><?php echo $label ?> FAQ</li>
                            </ul>
                            <div class="m-b-md">
                                <h3 class="m-b-none"><?php echo $label ?> FAQ</h3>
                                <small>Welcome back, <?php echo $_SESSION["admin_name"] ?></small>
                                <input type="button" class="btn btn-danger pull-right" value="Back" onClick="location.href='manage-faq'">
                            </div>
                            <form name="myform" id="myform" method="post" action="actions/create-faq.php">
                            <input type="hidden" name="myaction" value="<?php echo md5($myaction) ?>">
                            <input type="hidden" name="token" value="<?php echo $admin_tkn; ?>">
                            <input type="hidden" name="f9c7a57c74dcc509" value='<?php echo $_REQUEST["f9c7a57c74dcc509"]?>'>
                                <section class="panel panel-default">
                                   <header class="panel-heading font-bold"><?php echo $label ?> Category</header>
                                   <div class="panel-body">
                                      	<div class="form-group"> <label>FAQ Category</label>
                                            <?php
                                            if($label=="Edit"){    ?>
                                            
                                            <input type="text" name="parent_category_name" id="parent_category_name"  readonly  class="form-control" value="<?php echo $parent_name; ?>">

                                            <?php }
                                            else {
                                                $result_arr = array('i');
                                                $result_arr_val = array(0);
                                                $result_cat     =   $db_object->execute_select($fetch_parent_cat,$result_arr,$result_arr_val);
                                                ?>
                                                <select name="maincategory" id="maincategory" class="form-control category" required>
                                                    <option value="0">Select Category</option>
                                                    <?php
        											while($rows       =   pg_fetch_array($result_cat))
        											{
                                                        //$parent_cat = $rows["faq_category_name"];
                                                        //$parent_cat_id = $rows["faq_cat_id"];?>
                                                    <option value="<?php echo $rows["faq_cat_id"] ?>"><?php echo $rows["faq_category_name"] ?></option>
                                                    <?php
                                                    }
                                                    ?>
                                                </select>
                                            <?php } ?>    
                                        </div>
										<div class="form-group" id="response"> 
											<?php if($label=="Edit"){   
                                                    $result_arr = array('i');
                                                    $result_arr_val = array($parent_id);
                                                    $result_cat_sub     =   $db_object->execute_select($fetch_parent_cat,$result_arr,$result_arr_val);
                                                     ?>
                                                    <select name="category_edit" id="category_edit" class="form-control category" required>
                                                        <option value="0">Select SubCategory</option>
                                                        <?php
                                                        
                                                        while($rows_sub       =   pg_fetch_array($result_cat_sub))
                                                        {
                                                        ?>
                                                        <option value="<?php echo $rows_sub['faq_cat_id'] ?>" <?php echo $db_object->return_compare($category_id,$rows_sub['faq_cat_id'],"selected='selected'","") ?>><?php echo $rows_sub['faq_category_name'] ?></option>
                                                        <?php
                                                        } ?>                                               
                                                    </select>
                                                <?php }
                                                else {    ?>
                                                    <select name="category" id="category" class="form-control category" required>
                                                        <option value="0">Select SubCategory</option>                                                
                                                    </select>
                                                <?php
                                                        }
                                                ?> 										
                                        </div>
                                         <div class="form-group"> <label>Question</label> <textarea class="form-control" placeholder="Enter Question" name="question"  required><?php echo $question ?></textarea> </div>
                                          <div class="form-group"> <label>Answer</label> <textarea class="form-control" placeholder="Enter Answer" name="ans"  required> <?php echo $ans ?></textarea></div>
                                         <div class="form-group"> <label>Flag</label>
                                        <select name="flag" class="form-control" required>
                                            <option value="1" <?php echo $db_object->return_compare($flag,1,"selected='selected'","") ?>>Publish</option>
                                            <option value="0" <?php echo $db_object->return_compare($flag,0,"selected='selected'","") ?>>Unpublish</option>
                                        </select>
                                        </div>
                                        <input type="hidden" name="sidebar" value="0"> 										
                                        <input type="button" class="btn btn-success btn-default" value="Submit" name="action" id="submit_action">
                                   </div>
                                </section>
                            </form>                                                        
                        </section>
                    </section>
                    <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav"></a>
                </section>
                <aside class="bg-light lter b-l aside-md hide" id="notes">
                    <div class="wrapper">Notification</div>
                </aside>
            </section>
        </section>
    </section>
    <script src="js/app.v2.js"></script>
    <!-- Bootstrap -->
    <!-- App -->
    <script src="js/sortable/jquery.sortable.js" cache="false"></script>
    <script src="js/datatables/jquery.dataTables.min.js" cache="false"></script>
    <script>
	$(document).ready(function() {
		$('#mytable').DataTable();

		$("#submit_action").click(function(){               
			$( "#myform" ).submit();        
		});
		 

         //Cat-subcat
        $("#maincategory").change(function(){
            var partid = $(this).val();
            $.ajax({
                url: 'getSubCategory.php',
                type: 'post',
                data: {depart:partid},
                dataType: 'json',
                success:function(response){
                    var len = response.length;
                    for( var i = 0; i<len; i++){
                        var id = response[i]['id'];
                        var name = response[i]['name'];
                        $("#category").append("<option value='"+id+"' >"+name+"</option>");
                    }
                }
            });
        });
    });
    function validateForm(){              
            return true;            
        }
	</script>
</body>

</html>
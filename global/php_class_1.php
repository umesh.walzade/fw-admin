<?php
error_reporting(1);

//**************This class is used to perform various database related functions*****************
class db_connect
{
	var $conn;
	public function __construct($host = 'localhost',$user, $password, $database) {
		$this->user = $user;
		$this->password = $password;
		$this->database = $database;
		$this->host = $host;
	}
	protected function connect() {


		return new mysqli($this->host, $this->user, $this->password, $this->database);
	}


	//This function is used t0 establish a database connection
	function db_connect($dbhost,$dbuser,$dbpass,$dbname)
	{				
		$this->conn = mysql_connect($dbhost, $dbuser, $dbpass) or die ('Error connecting to mysql..........');
		mysql_select_db($dbname);
		//mysql_select_db($dbname);
		/*
		$conn = new mysqli($dbhost, $dbuser, $dbpass, $dbname);
		$conn->set_charset("utf8");
		if (mysqli_connect_errno()) {	
			echo "Failed to connect to MySQL: " . mysqli_connect_error();	
			mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
		   exit();
		}*/

	}
	//Token function added
	function conn_test($query){

		$dbcon = $this->connect();
		//$update_token ="update fw_token_tbl set token='".$iv."' where session_id=".$_SESSION["admin_id"];
		$stmt = $dbcon->prepare($query);
		echo $query."<br>";		
		if($stmt->execute()){
		    print 'Successfully inserted and Last inserted ID is :  <br />'; 
		    exit;
		}else{
		    echo "Prepare failed: -----".mysqli_stmt_error($stmt)."**************" . $dbcon->error;
		    //echo "Operation failed: Please try again";
		    exit();
		}
		$result = $stmt->get_result();
		$stmt->close();

		return $result;

	}
	
	// This function returns a resultset for select query 
	function execute_select($query, $bindParam,$paramValue)
	{
		$dbcon = $this->connect();
		//mysqli_set_charset( $dbcon, 'utf8');
		$dbcon->set_charset('utf8');
		$stmt = $dbcon->prepare($query); 
		if($bindParam != '1'){
			$stmt->bind_param($bindParam, $dbcon->real_escape_string($paramValue));
		}
		//$stmt->execute();
		if($stmt->execute()){
		    //print 'Successfully inserted and Last inserted ID is :  <br />'; 
		}else{
		    //echo "Prepare failed: -----".mysqli_stmt_error($stmt)."**************" . $dbcon->error;
		    echo "Operation failed: Please try again";
		    exit();
		}
		//echo $query, $bindParam,$paramValue; 
		//exit();
		$result = $stmt->get_result();
		$stmt->close();
		return $result;
	}

	function execute_select_multiple_cnt ($query, &$param_array = array(), &$param_arr_val =array()){
		
		$dbcon = $this->connect();
		//mysqli_set_charset( $dbcon, 'utf8');
		$dbcon->set_charset('utf8');
		$arr_cnt = count($param_array);

		//echo $arr_cnt. "inside";

		$arr_type = '';
		$arr_type_val ='';
		
		for($x = 0; $x < $arr_cnt; $x++) {
			$arr_type .= $param_array[$x];		
		}
		$Param_arr_cnt = count($param_arr_val);
		$stmt = $dbcon->prepare($query);
		
		if ($Param_arr_cnt > 0) {
			switch ($Param_arr_cnt) {	
			case 2:
				$var = $dbcon->real_escape_string($param_arr_val[0]);
				$var1 = $dbcon->real_escape_string($param_arr_val[1]);	
				$stmt->bind_param($arr_type,$var,$var1);
				break;
			case 3:
				$var = $dbcon->real_escape_string($param_arr_val[0]);
				$var1 = $dbcon->real_escape_string($param_arr_val[1]);	
				$var2 = $dbcon->real_escape_string($param_arr_val[2]);	
				$stmt->bind_param($arr_type,$var,$var1,$var2);
				break;
			
			default:
				
			}
		}
		
		if($stmt->execute()){
		    //print 'Successfully inserted and Last inserted ID is :  <br />'; 
		}else{
		    echo "Operation failed: Please try again"; //"Prepare failed: -----".mysqli_stmt_error($stmt)."**************" . $dbcon->error;
		    exit();
		}
		$result = $stmt->get_result();
		$row = mysqli_num_rows($result);
		//print_r($result);
		$stmt->close();
		return $row;
	
	}
	// This functionreturn inserted rowid for insert queiry
	function execute_select_multiple ($query, &$param_array = array(), &$param_arr_val =array()){
		
		$dbcon = $this->connect();
		//mysqli_set_charset( $dbcon, 'utf8');
		$dbcon->set_charset('utf8');
		$arr_cnt = count($param_array);

		//echo $arr_cnt. "inside";

		$arr_type = '';
		$arr_type_val ='';
		
		for($x = 0; $x < $arr_cnt; $x++) {
			$arr_type .= $param_array[$x];		
		}
		$Param_arr_cnt = count($param_arr_val);
		$stmt = $dbcon->prepare($query);
		
		if ($Param_arr_cnt > 0) {
			switch ($Param_arr_cnt) {	
			case 2:
				$var = $dbcon->real_escape_string($param_arr_val[0]);
				$var1 = $dbcon->real_escape_string($param_arr_val[1]);	
				$stmt->bind_param($arr_type,$var,$var1);
				break;
			case 3:
				$var = $dbcon->real_escape_string($param_arr_val[0]);
				$var1 = $dbcon->real_escape_string($param_arr_val[1]);	
				$var2 = $dbcon->real_escape_string($param_arr_val[2]);	
				$stmt->bind_param($arr_type,$var,$var1,$var2);
				break;
			case 4:
				$var = $dbcon->real_escape_string($param_arr_val[0]);
				$var1 = $dbcon->real_escape_string($param_arr_val[1]);	
				$var2 = $dbcon->real_escape_string($param_arr_val[2]);	
				$var3 = $dbcon->real_escape_string($param_arr_val[3]);	
				$stmt->bind_param($arr_type,$var,$var1,$var2,$var3);
				break;
			default:
				
			}
		}
		
		if($stmt->execute()){
		    //print 'Successfully inserted and Last inserted ID is :  <br />'; 
		}else{
		    echo "Operation failed: Please try again"; // "Prepare failed: -----".mysqli_stmt_error($stmt)."**************" . $dbcon->error;
		    exit();
		}
		$result = $stmt->get_result();
		//print_r($result);
		$stmt->close();
		return $result;
	
	}
	// This functionreturn inserted rowid for insert queiry
	function execute_insert ($query, &$param_array = array(), &$param_arr_val =array()){
		/*$rtn =0;
		$dbcon = $this->connect();
		$stmt = $dbcon->prepare($query);
		$stmt->bind_param($bindParam,$paramValue);
		//$stmt->execute();			
		//if($stmt->affected_rows === 0) $rtn =1;
		if($stmt->execute()){
		    print 'Successfully inserted and Last inserted ID is : ' .$stmt->insert_id .'<br />'; 
		}else{
		    echo "Prepare failed: -----".mysqli_stmt_error($stmt)."**************" . $dbcon->error;
		    exit();
		}
		$stmt->close();
		return $rtn;*/

		$dbcon = $this->connect();
		//mysqli_set_charset( $dbcon, 'utf8');
		$dbcon->set_charset('utf8');
		$arr_cnt = count($param_array);

		$arr_type = '';
		$arr_type_val ='';
		
		for($x = 0; $x < $arr_cnt; $x++) {
			$arr_type .= $param_array[$x];		
		}
		$Param_arr_cnt = count($param_arr_val);
		$stmt = $dbcon->prepare($query);
		
		if ($Param_arr_cnt > 0) {
			switch ($Param_arr_cnt) {	
			case 2:
				$var = $param_arr_val[0];
				$var1 = $param_arr_val[1];
				$stmt->bind_param($arr_type,$var,$var1);
				break;	
			case 4:
				$var = $param_arr_val[0];
				$var1 = $param_arr_val[1];
				$var2 = $param_arr_val[2];
				$var3 = $param_arr_val[3];
				$stmt->bind_param($arr_type,$var,$var1,$var2,$var3);
				break;
			case 5:
				$var = $param_arr_val[0];
				$var1 = $param_arr_val[1];
				$var2 = $param_arr_val[2];
				$var3 = $param_arr_val[3];
				$var4 = $param_arr_val[4];
				$stmt->bind_param($arr_type,$var,$var1,$var2,$var3,$var4);
				break;
			case 6:
				$var = $param_arr_val[0];
				$var1 = $param_arr_val[1];
				$var2 = $param_arr_val[2];
				$var3 = $param_arr_val[3];
				$var4 = $param_arr_val[4];
				$var5 = $param_arr_val[5];
				$stmt->bind_param($arr_type,$var,$var1,$var2,$var3,$var4,$var5);
				break;
			case 8:
				$var = $param_arr_val[0];
				$var1 = $param_arr_val[1];
				$var2 = $param_arr_val[2];
				$var3 = $param_arr_val[3];
				$var4 = $param_arr_val[4];
				$var5 = $param_arr_val[5];
				$var6 = $param_arr_val[6];
				$var7 = $param_arr_val[7];
				$stmt->bind_param($arr_type,$var,$var1,$var2,$var3,$var4,$var5,$var6,$var7);
				break;
			case 11:
				$var = $param_arr_val[0];
				$var1 = $param_arr_val[1];
				$var2 = $param_arr_val[2];
				$var3 = $param_arr_val[3];
				$var4 = $param_arr_val[4];
				$var5 = $param_arr_val[5];
				$var6 = $param_arr_val[6];
				$var7 = $param_arr_val[7];
				$var8 = $param_arr_val[8];
				$var9 = $param_arr_val[9];
				$var10 = $param_arr_val[10];
				$stmt->bind_param($arr_type,$var,$var1,$var2,$var3,$var4,$var5,$var6,$var7,$var8,$var9,$var10);
				break;	
			case 13:
				$var = $param_arr_val[0];
				$var1 = $param_arr_val[1];
				$var2 = $param_arr_val[2];
				$var3 = $param_arr_val[3];
				$var4 = $param_arr_val[4];
				$var5 = $param_arr_val[5];
				$var6 = $param_arr_val[6];
				$var7 = $param_arr_val[7];
				$var8 = $param_arr_val[8];
				$var9 = $param_arr_val[9];
				$var10 = $param_arr_val[10];
				$var11 = $param_arr_val[11];
				$var12 = $param_arr_val[12];
				$stmt->bind_param($arr_type,$var,$var1,$var2,$var3,$var4,$var5,$var6,$var7,$var8,$var9,$var10,$var11,$var12);
				break;	
			case 14:
				$var = $param_arr_val[0];
				$var1 = $param_arr_val[1];
				$var2 = $param_arr_val[2];
				$var3 = $param_arr_val[3];
				$var4 = $param_arr_val[4];
				$var5 = $param_arr_val[5];
				$var6 = $param_arr_val[6];
				$var7 = $param_arr_val[7];
				$var8 = $param_arr_val[8];
				$var9 = $param_arr_val[9];
				$var10 = $param_arr_val[10];
				$var11 = $param_arr_val[11];
				$var12 = $param_arr_val[12];
				$var13 = $param_arr_val[13];
				$stmt->bind_param($arr_type,$var,$var1,$var2,$var3,$var4,$var5,$var6,$var7,$var8,$var9,$var10,$var11,$var12,$var13);
				break;
			case 15:
				$var = $param_arr_val[0];
				$var1 = $param_arr_val[1];
				$var2 = $param_arr_val[2];
				$var3 = $param_arr_val[3];
				$var4 = $param_arr_val[4];
				$var5 = $param_arr_val[5];
				$var6 = $param_arr_val[6];
				$var7 = $param_arr_val[7];
				$var8 = $param_arr_val[8];
				$var9 = $param_arr_val[9];
				$var10 = $param_arr_val[10];
				$var11 = $param_arr_val[11];
				$var12 = $param_arr_val[12];
				$var13 = $param_arr_val[13];
				$var14 = $param_arr_val[14];
				$stmt->bind_param($arr_type,$var,$var1,$var2,$var3,$var4,$var5,$var6,$var7,$var8,$var9,$var10,$var11,$var12,$var13,$var14);
				break;
			case 16:
				$var = $param_arr_val[0];
				$var1 = $param_arr_val[1];
				$var2 = $param_arr_val[2];
				$var3 = $param_arr_val[3];
				$var4 = $param_arr_val[4];
				$var5 = $param_arr_val[5];
				$var6 = $param_arr_val[6];
				$var7 = $param_arr_val[7];
				$var8 = $param_arr_val[8];
				$var9 = $param_arr_val[9];
				$var10 = $param_arr_val[10];
				$var11 = $param_arr_val[11];
				$var12 = $param_arr_val[12];
				$var13 = $param_arr_val[13];
				$var14 = $param_arr_val[14];
				$var15 = $param_arr_val[15];
				$stmt->bind_param($arr_type,$var,$var1,$var2,$var3,$var4,$var5,$var6,$var7,$var8,$var9,$var10,$var11,$var12,$var13,$var14,$var15);
				break;	
			case 20:
				$var = $param_arr_val[0];
				$var1 = $param_arr_val[1];
				$var2 = $param_arr_val[2];
				$var3 = $param_arr_val[3];
				$var4 = $param_arr_val[4];
				$var5 = $param_arr_val[5];
				$var6 = $param_arr_val[6];
				$var7 = $param_arr_val[7];
				$var8 = $param_arr_val[8];
				$var9 = $param_arr_val[9];
				$var10 = $param_arr_val[10];
				$var11 = $param_arr_val[11];
				$var12 = $param_arr_val[12];
				$var13 = $param_arr_val[13];
				$var14 = $param_arr_val[14];
				$var15 = $param_arr_val[15];
				$var16 = $param_arr_val[16];
				$var17 = $param_arr_val[17];
				$var18 = $param_arr_val[18];
				$var19 = $param_arr_val[19];
				$stmt->bind_param($arr_type,$var,$var1,$var2,$var3,$var4,$var5,$var6,$var7,$var8,$var9,$var10,$var11,$var12,$var13,$var14,$var15,$var16,$var17,$var18,$var19);
				break;
			default:
				
			}
		}
		
		//$stmt->execute();			
		if($stmt->execute()){
		   // print 'Successfully inserted and Last inserted ID is : ' .$stmt->insert_id .'<br />'; 
		}else{
		    echo "Error while inserting data: Please try again"; // .mysqli_stmt_error($stmt)."**************" . $dbcon->error ."*****" .$Param_arr_cnt;
		    exit();
		}
		$rtn_row =  $stmt->affected_rows;
		$stmt->close();
		$this->chk_upd_token();
		return $rtn_row;
	
	}
	// This functionreturn inserted rowid for insert queiry
	function execute_update ($query, &$param_array = array(), &$param_arr_val =array()){
		
		$dbcon = $this->connect();
		mysqli_set_charset( $dbcon, 'utf8');
		//$dbcon->set_charset("utf8");
		$arr_cnt = count($param_array);

		/*print_r($query);
		echo "+";
		print_r($param_arr_val);
exit;*/
		$arr_type = '';
		$arr_type_val ='';
		
		for($x = 0; $x < $arr_cnt; $x++) {
			$arr_type .= $param_array[$x];		
		}
		$Param_arr_cnt = count($param_arr_val);
		$stmt = $dbcon->prepare($query);
		
		if ($Param_arr_cnt > 0) {
			switch ($Param_arr_cnt) {	
			case 2:
				$var = $dbcon->real_escape_string($param_arr_val[0]);
				$var1 = $dbcon->real_escape_string($param_arr_val[1]);
				$stmt->bind_param($arr_type,$var,$var1);
				break;
			case 3:
				$var = $dbcon->real_escape_string($param_arr_val[0]);
				$var1 = $dbcon->real_escape_string($param_arr_val[1]);
				$var2 = $dbcon->real_escape_string($param_arr_val[2]);
				$stmt->bind_param($arr_type,$var,$var1,$var2);
				break;
			case 4:
				$var = $dbcon->real_escape_string($param_arr_val[0]);
				$var1 = $dbcon->real_escape_string($param_arr_val[1]);
				$var2 = $dbcon->real_escape_string($param_arr_val[2]);
				$var3 = $dbcon->real_escape_string($param_arr_val[3]);
				$stmt->bind_param($arr_type,$var,$var1,$var2,$var3);
				break;
			case 5:
				$var = $dbcon->real_escape_string($param_arr_val[0]);
				$var1 = $dbcon->real_escape_string($param_arr_val[1]);
				$var2 = $dbcon->real_escape_string($param_arr_val[2]);
				$var3 = $dbcon->real_escape_string($param_arr_val[3]);
				$var4 = $dbcon->real_escape_string($param_arr_val[4]);
				$stmt->bind_param($arr_type,$var,$var1,$var2,$var3,$var4);
				break;
			case 6:
				$var = $dbcon->real_escape_string($param_arr_val[0]);
				$var1 = $dbcon->real_escape_string($param_arr_val[1]);
				$var2 = $dbcon->real_escape_string($param_arr_val[2]);
				$var3 = $dbcon->real_escape_string($param_arr_val[3]);
				$var4 = $dbcon->real_escape_string($param_arr_val[4]);
				$var5 = $dbcon->real_escape_string($param_arr_val[5]);
				$stmt->bind_param($arr_type,$var,$var1,$var2,$var3,$var4,$var5);
				break;
			
			case 8:
				$var = $param_arr_val[0];
				$var1 = $param_arr_val[1];
				$var2 = $param_arr_val[2];
				$var3 = $param_arr_val[3];
				$var4 = $param_arr_val[4];
				$var5 = $param_arr_val[5];
				$var6 = $param_arr_val[6];
				$var7 = $param_arr_val[7];
				$stmt->bind_param($arr_type,$var,$var1,$var2,$var3,$var4,$var5,$var6,$var7);
				break;
			case 11:
				$var = $param_arr_val[0];
				$var1 = $param_arr_val[1];
				$var2 = $param_arr_val[2];
				$var3 = $param_arr_val[3];
				$var4 = $param_arr_val[4];
				$var5 = $param_arr_val[5];
				$var6 = $param_arr_val[6];
				$var7 = $param_arr_val[7];
				$var8 = $param_arr_val[8];
				$var9 = $param_arr_val[9];
				$var10 = $param_arr_val[10];
				$stmt->bind_param($arr_type,$var,$var1,$var2,$var3,$var4,$var5,$var6,$var7,$var8,$var9,$var10);
				break;	
			case 12:
				$var = $param_arr_val[0];
				$var1 = $param_arr_val[1];
				$var2 = $param_arr_val[2];
				$var3 = $param_arr_val[3];
				$var4 = $param_arr_val[4];
				$var5 = $param_arr_val[5];
				$var6 = $param_arr_val[6];
				$var7 = $param_arr_val[7];
				$var8 = $param_arr_val[8];
				$var9 = $param_arr_val[9];
				$var10 = $param_arr_val[10];
				$var11 = $param_arr_val[11];
				$stmt->bind_param($arr_type,$var,$var1,$var2,$var3,$var4,$var5,$var6,$var7,$var8,$var9,$var10,$var11);
				break;	
			case 13:
				$var = $param_arr_val[0];
				$var1 = $param_arr_val[1];
				$var2 = $param_arr_val[2];
				$var3 = $param_arr_val[3];
				$var4 = $param_arr_val[4];
				$var5 = $param_arr_val[5];
				$var6 = $param_arr_val[6];
				$var7 = $param_arr_val[7];
				$var8 = $param_arr_val[8];
				$var9 = $param_arr_val[9];
				$var10 = $param_arr_val[10];
				$var11 = $param_arr_val[11];
				$var12 = $param_arr_val[12];
				$stmt->bind_param($arr_type,$var,$var1,$var2,$var3,$var4,$var5,$var6,$var7,$var8,$var9,$var10,$var11,$var12);
				break;	
			case 14:
				$var = $param_arr_val[0];
				$var1 = $param_arr_val[1];
				$var2 = $param_arr_val[2];
				$var3 = $param_arr_val[3];
				$var4 = $param_arr_val[4];
				$var5 = $param_arr_val[5];
				$var6 = $param_arr_val[6];
				$var7 = $param_arr_val[7];
				$var8 = $param_arr_val[8];
				$var9 = $param_arr_val[9];
				$var10 = $param_arr_val[10];
				$var11 = $param_arr_val[11];
				$var12 = $param_arr_val[12];
				$var13 = $param_arr_val[13];
				$stmt->bind_param($arr_type,$var,$var1,$var2,$var3,$var4,$var5,$var6,$var7,$var8,$var9,$var10,$var11,$var12,$var13);
				break;	
			case 15:
				$var = $param_arr_val[0];
				$var1 = $param_arr_val[1];
				$var2 = $param_arr_val[2];
				$var3 = $param_arr_val[3];
				$var4 = $param_arr_val[4];
				$var5 = $param_arr_val[5];
				$var6 = $param_arr_val[6];
				$var7 = $param_arr_val[7];
				$var8 = $param_arr_val[8];
				$var9 = $param_arr_val[9];
				$var10 = $param_arr_val[10];
				$var11 = $param_arr_val[11];
				$var12 = $param_arr_val[12];
				$var13 = $param_arr_val[13];
				$var14 = $param_arr_val[14];
				$stmt->bind_param($arr_type,$var,$var1,$var2,$var3,$var4,$var5,$var6,$var7,$var8,$var9,$var10,$var11,$var12,$var13,$var14);
				break;
			case 16:
				$var = $param_arr_val[0];
				$var1 = $param_arr_val[1];
				$var2 = $param_arr_val[2];
				$var3 = $param_arr_val[3];
				$var4 = $param_arr_val[4];
				$var5 = $param_arr_val[5];
				$var6 = $param_arr_val[6];
				$var7 = $param_arr_val[7];
				$var8 = $param_arr_val[8];
				$var9 = $param_arr_val[9];
				$var10 = $param_arr_val[10];
				$var11 = $param_arr_val[11];
				$var12 = $param_arr_val[12];
				$var13 = $param_arr_val[13];
				$var14 = $param_arr_val[14];
				$var15 = $param_arr_val[15];
				$stmt->bind_param($arr_type,$var,$var1,$var2,$var3,$var4,$var5,$var6,$var7,$var8,$var9,$var10,$var11,$var12,$var13,$var14,$var15);
				break;				
			default:
				
			}
		}

		//$stmt->execute();			
		if($stmt->execute()){
		    //print 'Successfully inserted and Last inserted ID is : ' .$stmt->insert_id .'<br />'; 
		    $rtn =1;
		}else{
			$rtn =0;
		    echo "Error while updating data: Please try again"; //.mysqli_stmt_error($stmt)."**************" . $dbcon->error;
		    exit();
		}
		//$rtn_row =  $stmt->affected_rows;
		$stmt->close();
		$this->chk_upd_token();
		
		return $rtn ;
	
	}

	//Token function added
	function chk_upd_token(){

		$dbcon = $this->connect();
		//mysqli_set_charset( $dbcon, 'utf8');
		$dbcon->set_charset('utf8');

		$key = 'mcrypt@encrypt!logout';
		$date = new DateTime("now");
		$dateTime = $date->format("Y-m-d H:m:s");
		$token_key =$_SERVER['REMOTE_ADDR'].$_SESSION["admin_id"].$dateTime.$key;
		$iv = md5(md5($token_key));
		$update_token ="update fw_token_tbl set token='".$iv."' where session_id=".$_SESSION["admin_id"];


		$stmt = $dbcon->prepare($update_token); 
		
		//$stmt->execute();
		if($stmt->execute()){
		    //print 'Successfully inserted and Last inserted ID is :  <br />'; 
		    
		}else{
		    //echo "Prepare failed: -----".mysqli_stmt_error($stmt)."**************" . $dbcon->error;
		    echo "Operation failed: Please try again";
		    exit();
		}
		//echo $query, $bindParam,$paramValue; 
		//exit();
		$result = $stmt->get_result();
		$stmt->close();

		return $result;

	}
	//Token end

	//this function check if record exist or not
	function execute_check($query, $bindParam,$paramValue)
	{
		//$query = "select * from fw_administrator where  ". $where;
		//echo $query, $bindParam,$paramValue;
		
		$rtn =1;
		$dbcon = $this->connect();	
		mysqli_set_charset( $dbcon, 'utf8');	
		$stmt = $dbcon->prepare($query); 
		$stmt->bind_param($bindParam, $dbcon->real_escape_string($paramValue));
		$stmt->execute();
		$result = $stmt->get_result();
		$row = mysqli_num_rows($result);
		if ($row ==0){ $rtn =0;}
				
		$stmt->close();
		return $rtn;
	} 


	// This function returns a resultset
	function execute_query($query)
	{
		//die($query);
		//echo "**********".$query;
		$result = mysql_query($query,$this->conn);
		//print_r($result);
	  	//echo mysql_error();  
	  	return $result;			

	}
	
	
	//This function returns each rows value arraylist from a resultset
	function return_query($query)
	{
		$result = mysql_query($query,$this->conn);
	  	echo mysql_error($this->conn);
	  	$value = mysql_fetch_array($result, MYSQL_ASSOC);
	  	return $value;	
	}
	
	//This method is used to remove slashes added using the addslash function
	function strip_slashes($value)
	{		
		reset($value);
		while (list($key, $val) = each($value))
		{
		  $value[$key] = (stripslashes($val));
		}
		return $value;
	}
	
	//This method is used to snatize a string for safe database entry
	function sanatize_string($value)
	{		
		$value = htmlentities(addslashes(strip_tags(trim($value))));		
		return $value;
	}
	
	//This method checks if a variable has been set in an array if yes the returns value else returns blank
	function check_value($array,$variable,$return)
	{
		if(isset($array[$variable]))
		{
			return($array[$variable]);
		}
		else
		{
			return($return);
		}	
	}
	
	// This method compares a variable1 with variable 2 and if same returns variable 3 else returns variable 1 itself
	function sring_compare($compare,$with,$return)
	{
		if($compare != $with)
		{
			return($compare);
		}
		else
		{
			return($return);
		}	
	}
	
	// This method compares a variable1 with variable 2 and if same returns [return1] else returns [return2]
	function return_compare($compare,$with,$return1,$return2)
	{
		if($compare == $with)
		{
			return($return1);
		}
		else
		{
			return($return2);
		}	
	}
	
	//This method combines 2 arrays to create a new array taking array 1 for keys and array 2 for values
	function combine_array($array_1,$array_2)
	{
		$new_array = array();		
		$i = 0;	
		while($i < count($array_1))
		{	
			$key = $array_1[$i];		
			$new_array = array_merge($new_array,array("'".$key."'"=>$array_2[$i]));
			$i++;			
		}		
		return $new_array;
	}
	
	//This function checks for the specified variables to see if they are numbers only
	function sanatize_query()
	{
		$array = $_GET;
		$count = count($_GET);
		$permitted = " 1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ*#_.";
		$querystring = "?";
		$isOk = true;
		
		foreach($array as $key => $val)
		{				
				if(strlen($val) <= 0)
				{					
					$val = "0";
					$isOk = false;
				}
				
				$fdata = "";
				for($i=0;$i<strlen($val);$i++)
				{
					$char = substr($val,$i,1);
					
					if($char == ' ')
					{
						$fdata .= ' ';
						continue;
					}
										
					if(strpos($permitted,$char))
					{									
						$fdata .= $char; 						
					}
					else
					{
						$isOk = false;
					}
				}
				$querystring .= $key."=".$fdata."&";								
		}		
		$querystring = substr($querystring,0,strlen($querystring)-1);
		if(!$isOk)
		{
		
			header("location:".$_SERVER['PHP_SELF']."$querystring");
			die("Not OK");
		}		
		return true;
	}
	
	function sanatize_value($value)
	{
		$permitted = " 1234567890";				
		
		$fdata = "";				
		for($i=0; $i < strlen($value);$i++)
		{			
			$char = substr($value,$i,1);						
			if(strpos($permitted,$char))
			{
				$fdata .= $char;												
			}						
		}
		
		if(strlen(trim($fdata)) <= 0)
		{					
			$fdata = "0";
		}
				
		return $fdata;
	}
					 
	//This function us used to close a database connection
	function con_close()
	{
		//mysql_close($this->conn);
	}
}

?>

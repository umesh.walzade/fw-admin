<?php
session_start();
session_regenerate_id();

header("X-XSS-Protection: 1");
header("strict-transport-security: max-age=600");
//Header injection trace Start

$allowed_hosts = array('www.saras.gov.in','172.21.26.50','172.21.26.51','127.0.0.1');
if (!isset($_SERVER['HTTP_HOST']) || !in_array($_SERVER['HTTP_HOST'], $allowed_hosts)) {
    //header($_SERVER['SERVER_PROTOCOL'].' 400 Bad Request');
	unset($_SESSION["admin_id"]);
	unset($_SESSION["admin_name"]);
	session_regenerate_id();
    session_destroy();
    header('Location: ../404.html');
    exit();
}



//Header injection trace End

date_default_timezone_set('Asia/Kolkata');
require('php_class.php');
$title 	= "";
/*
$host		= "10.130.18.70";
$database	= "rms_portal";
$username 	= "enterprisedb";
$password 	= "nsdl1234";
$port		= 5444;
*/
// $host           = "10.129.2.59";
// $database       = "new_rms_portal";
// $username       = "enterprisedb";
// $password       = "nsdl1234";

$host           = "localhost";
$database       = "saras";
$username       = "postgres";
$password       = "root";

$port		= 5432;
$master_url	= "https://www.saras.gov.in/main/";
$page_limit	=	20;


$upload_path=	"main/uploads/";
$img_path	="main/uploads/";
//$video_uploads_path	="https://www.saras.gov.in/main/video_uploads/";
$target_dir ="/main/uploads/";
$download_dir ="/main/download_format/";


$db_object 	= new db_connect("$host","$port","$username","$password","$database");
require('fetch_query_list.php');
//session_start();
if(isset($_SESSION["admin_id"]))
{
	$parent_id	=	$_SESSION["admin_id"];
}

//Session destroy
function fun_session_destroy(){
	session_start();
	$_SESSION = array();
	session_destroy();
}


//------- session end
// auth_key generate after successful login
function getBrowser() { 
  $u_agent = $_SERVER['HTTP_USER_AGENT'];
  $bname = 'Unknown';
  $platform = 'Unknown';
  $version= "";

  //First get the platform?
  if (preg_match('/linux/i', $u_agent)) {
	$platform = 'linux';
  }elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
	$platform = 'mac';
  }elseif (preg_match('/windows|win32/i', $u_agent)) {
	$platform = 'windows';
  }

  // Next get the name of the useragent yes seperately and for good reason
  if(preg_match('/MSIE/i',$u_agent) && !preg_match('/Opera/i',$u_agent)){
	$bname = 'Internet Explorer';
	$ub = "MSIE";
  }elseif(preg_match('/Firefox/i',$u_agent)){
	$bname = 'Mozilla Firefox';
	$ub = "Firefox";
  }elseif(preg_match('/OPR/i',$u_agent)){
	$bname = 'Opera';
	$ub = "Opera";
  }elseif(preg_match('/Chrome/i',$u_agent) && !preg_match('/Edge/i',$u_agent)){
	$bname = 'Google Chrome';
	$ub = "Chrome";
  }elseif(preg_match('/Safari/i',$u_agent) && !preg_match('/Edge/i',$u_agent)){
	$bname = 'Apple Safari';
	$ub = "Safari";
  }elseif(preg_match('/Netscape/i',$u_agent)){
	$bname = 'Netscape';
	$ub = "Netscape";
  }elseif(preg_match('/Edge/i',$u_agent)){
	$bname = 'Edge';
	$ub = "Edge";
  }elseif(preg_match('/Trident/i',$u_agent)){
	$bname = 'Internet Explorer';
	$ub = "MSIE";
  }

  // finally get the correct version number
  $known = array('Version', $ub, 'other');
  $pattern = '#(?<browser>' . join('|', $known) .
')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
  if (!preg_match_all($pattern, $u_agent, $matches)) {
	// we have no matching number just continue
  }
  // see how many we have
  $i = count($matches['browser']);
  if ($i != 1) {
	//we will have two since we are not using 'other' argument yet
	//see if version is before or after the name
	if (strripos($u_agent,"Version") < strripos($u_agent,$ub)){
		$version= $matches['version'][0];
	}else {
		$version= $matches['version'][1];
	}
  }else {
	$version= $matches['version'][0];
  }

  // check if we have a number
  if ($version==null || $version=="") {$version="?";}

  return array(
	'userAgent' => $u_agent,
	'name'      => $bname,
	'version'   => $version,
	'platform'  => $platform,
	'pattern'    => $pattern
  );
} 


function mcrypt_encrypt_data($sion_id,$dt) 
{
	$ua=getBrowser(); 
	if($sion_id ==0){
		$key = 'default@encrypt!sushrut';
	}
	else{
		$key = 'mcrypt@encrypt!sushrut';
	}
	
	$token_key = $dt.$ua['name'].$ua['version'].$_SERVER['REMOTE_ADDR'].$sion_id.$key;
	$iv = md5(md5($token_key));
	return $iv; 
}


function check_login()
{	
	global $db_object;
	$home_url	= "https://www.uat.saras.gov.in/FW-Admin/index";
	//$home_url	= "http://localhost:90/RMS_portal/FW-Admin/";
	$cnt_val =1;
	sessionTimeOut();		
	if($cnt_val == 1){
		if($_SESSION["admin_id"] <> 1)
		{
			fun_session_destroy();
			header("HTTP/1.1 404 Not Found");
			header("Location: ".$home_url."/404.html");
			exit;

		}
		else{
			$qry ="SELECT * from fw_administrator where admin_id = $1";
			$result_arr = array('i');
			$result_arr_val = array($_SESSION["admin_id"]);
			$result_cat		=	$db_object->execute_select($qry,$result_arr,$result_arr_val);
	        if(pg_num_rows($result_cat) < 1){ 
	        	fun_session_destroy();
		    	header("Location: ".$home_url."/404.html");
				exit;
	        }
	    
	        while($rows       =   pg_fetch_array($result_cat))
			{
				$dt = date("Y-m-d H:m:s", strtotime($rows["token_dt"])); 
				$token = mcrypt_encrypt_data($_SESSION["admin_id"],$dt);
				$sql= "SELECT * from fw_administrator where admin_id = $1 and um_token= $2";
				$result_arr = array('i','s');
				$result_arr_val = array($_SESSION["admin_id"],$token);
				$result1 = $db_object->execute_select($sql,$result_arr,$result_arr_val);

				if(pg_num_rows($result1) === 0) {
					fun_session_destroy();
					$_SESSION["error_id"]  = "Something wrong, please try again";
					header("Location: ".$home_url."FW-Admin/index.php");
					exit;
				}
			}
		}

	}
	if(!isset($_SESSION["admin_id"]))
	{
		fun_session_destroy();
		header("Location: ".$home_url."/404.html");
		exit;
		
	}
	if(!$userid = $db_object->check_value($_SESSION,"admin_id", false))
	{
		fun_session_destroy();
		header("Location: ".$home_url."/404.html");
		exit;
	}
}

//Session tie out check start 
function sessionTimeOut(){
	$time = $_SERVER['REQUEST_TIME'];
	/**
	* for a 5 minute timeout, specified in seconds
	*/
	$timeout_duration = 300;

	if (isset($_SESSION['LAST_ACTIVITY']) && 
	   ($time - $_SESSION['LAST_ACTIVITY']) > $timeout_duration) {
	    redirect("actions/logout.php");
	}
	$_SESSION['LAST_ACTIVITY'] = $time;
}
//Session tie out check End 

function display_msg($id)
{
	global $db_object;
	$sql = $db_object->return_query("SELECT * from fw_error_master where error_id=$id");
	$m			=	$sql["error_msg"];
	$msg		=	"<div style='clear:both'></div><div class='alert alert-danger alert-dismissible' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button><strong>$m</strong></div>";
	return $msg;
}

function redirect($url)
{
	global $db_object;
	$db_object->con_close();
	header("Location: $url");
}

function upload_photo($filefield)
{
	global $photos;
	$photoname = date("dmYHis").rand(0,50);		

	if($_FILES[$filefield]['name'] == "")
	{
		return "blank";
	}
	
	$file_obj = new file_manup($photos);

	$photoname = $file_obj->upload_file($_FILES[$filefield]['name'],$_FILES[$filefield]['tmp_name'],$photoname,".jpg,.jpeg,.gif,.png");		
	if($photoname != "")
	{
		$photo_obj = new img_manup($photos);
		$photo_obj->createThumbs($photoname,500,"");		
		return $photoname;
	}
	else
	{
		return false;
	}	
}


function initialize_paging($query)
{
	global $host, $username , $password, $database;
	$objpaging = new pagination();
	$objpaging->db_connect($host,$username,$password,$database);			
	$objpaging->limit = 10;
	if(isset($_REQUEST["start"]))
	{		  
		$objpaging->start = $_REQUEST["start"];				
	}					  
	$objpaging->query = $query;
	
	return $objpaging;
}


function resize_photo($filename)
{
	if($filename != "blank")
	{
		global $photos;
		$photo_obj = new img_manup($photos);	
		return $photo_obj->imageResize($filename, 100);
	}
}


function delete_image($filename)
{
	global $photos;
	$file_obj = new file_manup($photos);
	return $file_obj->delete_file($filename);
}

function check_error($message = "")
{
	if(isset($_REQUEST["error"]))
	{
		echo ($_REQUEST["error"]);
	}
	else
	{
		echo($message);
	}
}

function check_access($admin_type)
{
	switch($admin_type)
	{
		case 1:
			if(!($_SESSION["admin_role"] == 1))
			{
				redirect("index.php?error=You are not authorized to access this page.");
			}			
		break;
		case 2:
			if(!($_SESSION["admin_role"] == 1 || $_SESSION["admin_type"] == 2))
			{
				redirect("index.php?error=You are not authorized to access this page.");
			}			
		break;
		
		case 3:
			if(!($_SESSION["admin_role"] == 1 || $_SESSION["admin_type"] == 3))
			{
				redirect("index.php?error=You are not authorized to access this page.");
			}					
		break;
		
		case 4:
			if(!($_SESSION["admin_role"] == 1 || $_SESSION["admin_type"] == 4))
			{
				redirect("index.php?error=You are not authorized to access this page.");
			}			
		break;
		
		case 5:
			if(!($_SESSION["admin_role"] == 1 || $_SESSION["admin_type"] == 5))
			{
				redirect("index.php?error=You are not authorized to access this page.");
			}			
		break;
		
		case 6:
			if(!($_SESSION["admin_role"] == 1 || $_SESSION["admin_type"] == 6))
			{
				redirect("index.php?error=You are not authorized to access this page.");
			}			
		break;
	}
}

function getIp() {
    $ip = $_SERVER['REMOTE_ADDR'];
 
    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    }
 
    return $ip;
}

?>

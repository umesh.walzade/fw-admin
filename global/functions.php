<?php
	function get_product_name($pid){
		$result=mysql_query("select product_name from product_master where product_id=$pid");
		$row=mysql_fetch_array($result);
		return $row['product_name'];
	}
	
	function get_my_email($id)
	{
		$result=mysql_query("select email_id from register_master where reg_id=$pid");
		$row=mysql_fetch_array($result);
		return $row['product_name'];
	}
	
	function get_year($pid,$sid,$yid)
	{
		$result=mysql_query("select year_name from year_master where year_id = (SELECT year_id from pro_price_master where product_id=$pid and year_id=$yid and sub_id=$sid)");
		$row=mysql_fetch_array($result);
		return $row['year_name'];
	}
	
	function get_sub($pid,$sid,$yid)
	{
		$result=mysql_query("select sub_name from sub_master where sub_id=(SELECT sub_id from pro_price_master where product_id=$pid and year_id=$yid and sub_id=$sid)");
		$row=mysql_fetch_array($result);
		return $row['sub_name'];
	}
	
	function get_price($pid,$yid,$sid){
		$result=mysql_query("select price from pro_price_master where product_id=$pid and year_id=$yid and sub_id=$sid");
		$row=mysql_fetch_array($result);
		return $row['price'];
	}
	
	function get_price_id($pid,$yid,$sid){
		$result=mysql_query("select price_id from pro_price_master where product_id=$pid and year_id=$yid and sub_id=$sid");
		$row=mysql_fetch_array($result);
		return $row['price_id'];
	}
	
	function get_image($pid){
	$result = mysql_query("SELECT * from product_master where product_id=$pid");
	$row=mysql_fetch_array($result);
		return $row['product_photo'];
	}
	
	function get_cat($pid){
		$result=mysql_query("select cat_name from cat_master where cat_id=(SELECT cat_id from product_master where product_id=$pid)");
		$row=mysql_fetch_array($result);
		return $row['cat_name'];
	}
	
	function remove_product($pid,$q,$sid,$yid){
		$pid=intval($pid);
		$q = intval($q);
		$sid=intval($sid);
		$yid=intval($yid);
		$max=count($_SESSION['cart']);
		for($i=0;$i<$max;$i++){
			if($pid==$_SESSION['cart'][$i]['productid'] && $q==$_SESSION['cart'][$i]['qty'] && $sid==$_SESSION['cart'][$i]['sub_id'] && $yid==$_SESSION['cart'][$i]['yid']){
				unset($_SESSION['cart'][$i]);
				break;
			}
		}
		$_SESSION['cart']=array_values($_SESSION['cart']);
	}
	
	function get_order_total(){
		$max=count($_SESSION['cart']);
		$sum=0;
		for($i=0;$i<$max;$i++){
			$pid=$_SESSION['cart'][$i]['productid'];
			$q=$_SESSION['cart'][$i]['qty'];
			$sid = $_SESSION['cart'][$i]['sub_id'];
			$yid = $_SESSION['cart'][$i]['yid'];
			$price=get_price($pid,$yid,$sid);
			$sum+=$price*$q;
		}
		return $sum;
	}
	
	function addtocart($pid,$q,$sid,$yid){
		if($pid<1 or $q<1 or $sid<1 or $yid<1) return;
		
		if(is_array($_SESSION['cart'])){
			if(product_exists($pid,$sid,$yid)) return;
			$max=count($_SESSION['cart']);
			$_SESSION['cart'][$max]['productid']=$pid;
			$_SESSION['cart'][$max]['qty']=$q;
			$_SESSION['cart'][$max]['sub_id']=$sid;
			$_SESSION['cart'][$max]['yid']=$yid;
		}
		else{
			$_SESSION['cart']=array();
			$_SESSION['cart'][0]['productid']=$pid;
			$_SESSION['cart'][0]['qty']=$q;
			$_SESSION['cart'][0]['sub_id']=$sid;
			$_SESSION['cart'][0]['yid']=$yid;
		}
	}
	
	function product_exists($pid,$sid,$yid){
		$pid=intval($pid);
		$sid=intval($sid);
		$yid=intval($yid);
		$max=count($_SESSION['cart']);
		$flag=0;
		for($i=0;$i<$max;$i++){
			if($pid==$_SESSION['cart'][$i]['productid'] && $sid==$_SESSION['cart'][$i]['sub_id'] && $yid==$_SESSION['cart'][$i]['yid']){
				$flag=1;
				break;
			}
		}
		return $flag;
	}
?>
<?php
error_reporting(1);

//**************This class is used to perform various database related functions*****************
class db_connect
{
	var $conn;
	public function __construct($host,$port,$user, $password, $database) {
		$this->user = $user;
		$this->password = $password;
		$this->database = $database;
		$this->host = $host;
		$this->port = $port;
	}
	/*protected function connect() {
		//return new pg_connect("host=$host dbname=$database user=$username password=$password")  or die ("Could not connect to server\n"); 
		return new mysqli($this->host, $this->user, $this->password, $this->database);
	}*/
	
	//This function is used t0 establish a database connection
	function connect()
	{	
		return pg_connect("host=$this->host port=$this->port dbname=$this->database user=$this->user password=$this->password options='--client_encoding=UTF8'");
	}
	
	// This function returns a resultset for select query 
	function execute_select($query, $bindParam,$paramValue)
	{
		$dbcon = $this->connect();		
		$result = pg_prepare($dbcon, "prepare1", $query);
		//echo pg_last_error($dbconn);
		$result = pg_execute($dbcon, "prepare1", $paramValue);
		pg_close($dbcon); 		
		return $result;
	}
	
	function execute_update ($query, $param_array = array(), $param_arr_val){
		$dbcon = $this->connect();		
		$result = pg_prepare($dbcon, "prepare1", $query);
		echo pg_last_error($dbconn);
		$result = pg_execute($dbcon, "prepare1", $paramValue);
		$affected_rows = pg_affected_rows ($result);
		pg_close($dbcon); 		
		return $affected_rows;
	}

	function execute_select_multiple_cnt ($query, &$param_array = array(), &$param_arr_val =array()){
		
		$dbcon = $this->connect();
		//mysqli_set_charset( $dbcon, 'utf8');
		$dbcon->set_charset('utf8');
		$arr_cnt = count($param_array);

		//echo $arr_cnt. "inside";

		$arr_type = '';
		$arr_type_val ='';
		
		for($x = 0; $x < $arr_cnt; $x++) {
			$arr_type .= $param_array[$x];		
		}
		$Param_arr_cnt = count($param_arr_val);
		$stmt = $dbcon->prepare($query);
		
		if ($Param_arr_cnt > 0) {
			switch ($Param_arr_cnt) {	
			case 2:
				$var = $dbcon->real_escape_string($param_arr_val[0]);
				$var1 = $dbcon->real_escape_string($param_arr_val[1]);	
				$stmt->bind_param($arr_type,$var,$var1);
				break;
			case 3:
				$var = $dbcon->real_escape_string($param_arr_val[0]);
				$var1 = $dbcon->real_escape_string($param_arr_val[1]);	
				$var2 = $dbcon->real_escape_string($param_arr_val[2]);	
				$stmt->bind_param($arr_type,$var,$var1,$var2);
				break;
			
			default:
				
			}
		}
		
		if($stmt->execute()){
		    //print 'Successfully inserted and Last inserted ID is :  <br />'; 
		}else{
		    echo "Operation failed: Please try again"; //"Prepare failed: -----".mysqli_stmt_error($stmt)."**************" . $dbcon->error;
		    exit();
		}
		$result = $stmt->get_result();
		$row = mysqli_num_rows($result);
		//print_r($result);
		$stmt->close();
		return $row;
	
	}
	// This functionreturn inserted rowid for insert queiry
	function execute_select_multiple ($query, &$param_array = array(), &$param_arr_val =array()){
		
		$dbcon = $this->connect();
		//mysqli_set_charset( $dbcon, 'utf8');
		$dbcon->set_charset('utf8');
		$arr_cnt = count($param_array);

		//echo $arr_cnt. "inside";

		$arr_type = '';
		$arr_type_val ='';
		
		for($x = 0; $x < $arr_cnt; $x++) {
			$arr_type .= $param_array[$x];		
		}
		$Param_arr_cnt = count($param_arr_val);
		$stmt = $dbcon->prepare($query);
		
		if ($Param_arr_cnt > 0) {
			switch ($Param_arr_cnt) {	
			case 2:
				$var = $dbcon->real_escape_string($param_arr_val[0]);
				$var1 = $dbcon->real_escape_string($param_arr_val[1]);	
				$stmt->bind_param($arr_type,$var,$var1);
				break;
			case 3:
				$var = $dbcon->real_escape_string($param_arr_val[0]);
				$var1 = $dbcon->real_escape_string($param_arr_val[1]);	
				$var2 = $dbcon->real_escape_string($param_arr_val[2]);	
				$stmt->bind_param($arr_type,$var,$var1,$var2);
				break;
			case 4:
				$var = $dbcon->real_escape_string($param_arr_val[0]);
				$var1 = $dbcon->real_escape_string($param_arr_val[1]);	
				$var2 = $dbcon->real_escape_string($param_arr_val[2]);	
				$var3 = $dbcon->real_escape_string($param_arr_val[3]);	
				$stmt->bind_param($arr_type,$var,$var1,$var2,$var3);
				break;
			default:
				
			}
		}
		
		if($stmt->execute()){
		    //print 'Successfully inserted and Last inserted ID is :  <br />'; 
		}else{
		    echo "Operation failed: Please try again"; // "Prepare failed: -----".mysqli_stmt_error($stmt)."**************" . $dbcon->error;
		    exit();
		}
		$result = $stmt->get_result();
		//print_r($result);
		$stmt->close();
		return $result;
	
	}
	// This functionreturn inserted rowid for insert queiry
	function execute_insert ($query, &$param_array = array(), &$param_arr_val =array()){
		/*$rtn =0;
		$dbcon = $this->connect();
		$stmt = $dbcon->prepare($query);
		$stmt->bind_param($bindParam,$paramValue);
		//$stmt->execute();			
		//if($stmt->affected_rows === 0) $rtn =1;
		if($stmt->execute()){
		    print 'Successfully inserted and Last inserted ID is : ' .$stmt->insert_id .'<br />'; 
		}else{
		    echo "Prepare failed: -----".mysqli_stmt_error($stmt)."**************" . $dbcon->error;
		    exit();
		}
		$stmt->close();
		return $rtn;*/

		$dbcon = $this->connect();
		//mysqli_set_charset( $dbcon, 'utf8');
		$dbcon->set_charset('utf8');
		$arr_cnt = count($param_array);

		$arr_type = '';
		$arr_type_val ='';
		
		for($x = 0; $x < $arr_cnt; $x++) {
			$arr_type .= $param_array[$x];		
		}
		$Param_arr_cnt = count($param_arr_val);
		$stmt = $dbcon->prepare($query);
		
		if ($Param_arr_cnt > 0) {
			switch ($Param_arr_cnt) {	
			case 2:
				$var = $param_arr_val[0];
				$var1 = $param_arr_val[1];
				$stmt->bind_param($arr_type,$var,$var1);
				break;	
			case 4:
				$var = $param_arr_val[0];
				$var1 = $param_arr_val[1];
				$var2 = $param_arr_val[2];
				$var3 = $param_arr_val[3];
				$stmt->bind_param($arr_type,$var,$var1,$var2,$var3);
				break;
			case 5:
				$var = $param_arr_val[0];
				$var1 = $param_arr_val[1];
				$var2 = $param_arr_val[2];
				$var3 = $param_arr_val[3];
				$var4 = $param_arr_val[4];
				$stmt->bind_param($arr_type,$var,$var1,$var2,$var3,$var4);
				break;
			case 6:
				$var = $param_arr_val[0];
				$var1 = $param_arr_val[1];
				$var2 = $param_arr_val[2];
				$var3 = $param_arr_val[3];
				$var4 = $param_arr_val[4];
				$var5 = $param_arr_val[5];
				$stmt->bind_param($arr_type,$var,$var1,$var2,$var3,$var4,$var5);
				break;
			case 7:
				$var = $param_arr_val[0];
				$var1 = $param_arr_val[1];
				$var2 = $param_arr_val[2];
				$var3 = $param_arr_val[3];
				$var4 = $param_arr_val[4];
				$var5 = $param_arr_val[5];
				$var6 = $param_arr_val[6];
				$stmt->bind_param($arr_type,$var,$var1,$var2,$var3,$var4,$var5,$var6);
				break;	
			case 8:
				$var = $param_arr_val[0];
				$var1 = $param_arr_val[1];
				$var2 = $param_arr_val[2];
				$var3 = $param_arr_val[3];
				$var4 = $param_arr_val[4];
				$var5 = $param_arr_val[5];
				$var6 = $param_arr_val[6];
				$var7 = $param_arr_val[7];
				$stmt->bind_param($arr_type,$var,$var1,$var2,$var3,$var4,$var5,$var6,$var7);
				break;
			case 11:
				$var = $param_arr_val[0];
				$var1 = $param_arr_val[1];
				$var2 = $param_arr_val[2];
				$var3 = $param_arr_val[3];
				$var4 = $param_arr_val[4];
				$var5 = $param_arr_val[5];
				$var6 = $param_arr_val[6];
				$var7 = $param_arr_val[7];
				$var8 = $param_arr_val[8];
				$var9 = $param_arr_val[9];
				$var10 = $param_arr_val[10];
				$stmt->bind_param($arr_type,$var,$var1,$var2,$var3,$var4,$var5,$var6,$var7,$var8,$var9,$var10);
				break;	
			case 13:
				$var = $param_arr_val[0];
				$var1 = $param_arr_val[1];
				$var2 = $param_arr_val[2];
				$var3 = $param_arr_val[3];
				$var4 = $param_arr_val[4];
				$var5 = $param_arr_val[5];
				$var6 = $param_arr_val[6];
				$var7 = $param_arr_val[7];
				$var8 = $param_arr_val[8];
				$var9 = $param_arr_val[9];
				$var10 = $param_arr_val[10];
				$var11 = $param_arr_val[11];
				$var12 = $param_arr_val[12];
				$stmt->bind_param($arr_type,$var,$var1,$var2,$var3,$var4,$var5,$var6,$var7,$var8,$var9,$var10,$var11,$var12);
				break;	
			case 14:
				$var = $param_arr_val[0];
				$var1 = $param_arr_val[1];
				$var2 = $param_arr_val[2];
				$var3 = $param_arr_val[3];
				$var4 = $param_arr_val[4];
				$var5 = $param_arr_val[5];
				$var6 = $param_arr_val[6];
				$var7 = $param_arr_val[7];
				$var8 = $param_arr_val[8];
				$var9 = $param_arr_val[9];
				$var10 = $param_arr_val[10];
				$var11 = $param_arr_val[11];
				$var12 = $param_arr_val[12];
				$var13 = $param_arr_val[13];
				$stmt->bind_param($arr_type,$var,$var1,$var2,$var3,$var4,$var5,$var6,$var7,$var8,$var9,$var10,$var11,$var12,$var13);
				break;
			case 15:
				$var = $param_arr_val[0];
				$var1 = $param_arr_val[1];
				$var2 = $param_arr_val[2];
				$var3 = $param_arr_val[3];
				$var4 = $param_arr_val[4];
				$var5 = $param_arr_val[5];
				$var6 = $param_arr_val[6];
				$var7 = $param_arr_val[7];
				$var8 = $param_arr_val[8];
				$var9 = $param_arr_val[9];
				$var10 = $param_arr_val[10];
				$var11 = $param_arr_val[11];
				$var12 = $param_arr_val[12];
				$var13 = $param_arr_val[13];
				$var14 = $param_arr_val[14];
				$stmt->bind_param($arr_type,$var,$var1,$var2,$var3,$var4,$var5,$var6,$var7,$var8,$var9,$var10,$var11,$var12,$var13,$var14);
				break;
			case 16:
				$var = $param_arr_val[0];
				$var1 = $param_arr_val[1];
				$var2 = $param_arr_val[2];
				$var3 = $param_arr_val[3];
				$var4 = $param_arr_val[4];
				$var5 = $param_arr_val[5];
				$var6 = $param_arr_val[6];
				$var7 = $param_arr_val[7];
				$var8 = $param_arr_val[8];
				$var9 = $param_arr_val[9];
				$var10 = $param_arr_val[10];
				$var11 = $param_arr_val[11];
				$var12 = $param_arr_val[12];
				$var13 = $param_arr_val[13];
				$var14 = $param_arr_val[14];
				$var15 = $param_arr_val[15];
				$stmt->bind_param($arr_type,$var,$var1,$var2,$var3,$var4,$var5,$var6,$var7,$var8,$var9,$var10,$var11,$var12,$var13,$var14,$var15);
				break;	
			case 20:
				$var = $param_arr_val[0];
				$var1 = $param_arr_val[1];
				$var2 = $param_arr_val[2];
				$var3 = $param_arr_val[3];
				$var4 = $param_arr_val[4];
				$var5 = $param_arr_val[5];
				$var6 = $param_arr_val[6];
				$var7 = $param_arr_val[7];
				$var8 = $param_arr_val[8];
				$var9 = $param_arr_val[9];
				$var10 = $param_arr_val[10];
				$var11 = $param_arr_val[11];
				$var12 = $param_arr_val[12];
				$var13 = $param_arr_val[13];
				$var14 = $param_arr_val[14];
				$var15 = $param_arr_val[15];
				$var16 = $param_arr_val[16];
				$var17 = $param_arr_val[17];
				$var18 = $param_arr_val[18];
				$var19 = $param_arr_val[19];
				$stmt->bind_param($arr_type,$var,$var1,$var2,$var3,$var4,$var5,$var6,$var7,$var8,$var9,$var10,$var11,$var12,$var13,$var14,$var15,$var16,$var17,$var18,$var19);
				break;
			default:
				
			}
		}
		
		//$stmt->execute();			
		if($stmt->execute()){
		   // print 'Successfully inserted and Last inserted ID is : ' .$stmt->insert_id .'<br />'; 
		}else{
		    echo "Error while inserting data: Please try again"; // .mysqli_stmt_error($stmt)."**************" . $dbcon->error ."*****" .$Param_arr_cnt;
		    exit();
		}
		$rtn_row =  $stmt->affected_rows;
		$stmt->close();
		$this->chk_upd_token();
		return $rtn_row;
	
	}
	// This functionreturn inserted rowid for insert queiry
	function m_execute_update ($query, &$param_array = array(), &$param_arr_val =array()){
		
		$dbcon = $this->connect();
		mysqli_set_charset( $dbcon, 'utf8');
		//$dbcon->set_charset("utf8");
		$arr_cnt = count($param_array);

		/*print_r($query);
		echo "+";
		print_r($param_arr_val);
exit;*/
		$arr_type = '';
		$arr_type_val ='';
		
		for($x = 0; $x < $arr_cnt; $x++) {
			$arr_type .= $param_array[$x];		
		}
		$Param_arr_cnt = count($param_arr_val);
		$stmt = $dbcon->prepare($query);
		
		if ($Param_arr_cnt > 0) {
			switch ($Param_arr_cnt) {	
			case 2:
				$var = $dbcon->real_escape_string($param_arr_val[0]);
				$var1 = $dbcon->real_escape_string($param_arr_val[1]);
				$stmt->bind_param($arr_type,$var,$var1);
				break;
			case 3:
				$var = $dbcon->real_escape_string($param_arr_val[0]);
				$var1 = $dbcon->real_escape_string($param_arr_val[1]);
				$var2 = $dbcon->real_escape_string($param_arr_val[2]);
				$stmt->bind_param($arr_type,$var,$var1,$var2);
				break;
			case 4:
				$var = $dbcon->real_escape_string($param_arr_val[0]);
				$var1 = $dbcon->real_escape_string($param_arr_val[1]);
				$var2 = $dbcon->real_escape_string($param_arr_val[2]);
				$var3 = $dbcon->real_escape_string($param_arr_val[3]);
				$stmt->bind_param($arr_type,$var,$var1,$var2,$var3);
				break;
			case 5:
				$var = $dbcon->real_escape_string($param_arr_val[0]);
				$var1 = $dbcon->real_escape_string($param_arr_val[1]);
				$var2 = $dbcon->real_escape_string($param_arr_val[2]);
				$var3 = $dbcon->real_escape_string($param_arr_val[3]);
				$var4 = $dbcon->real_escape_string($param_arr_val[4]);
				$stmt->bind_param($arr_type,$var,$var1,$var2,$var3,$var4);
				break;
			case 6:
				$var = $dbcon->real_escape_string($param_arr_val[0]);
				$var1 = $dbcon->real_escape_string($param_arr_val[1]);
				$var2 = $dbcon->real_escape_string($param_arr_val[2]);
				$var3 = $dbcon->real_escape_string($param_arr_val[3]);
				$var4 = $dbcon->real_escape_string($param_arr_val[4]);
				$var5 = $dbcon->real_escape_string($param_arr_val[5]);
				$stmt->bind_param($arr_type,$var,$var1,$var2,$var3,$var4,$var5);
				break;
			case 7:
				$var = $param_arr_val[0];
				$var1 = $param_arr_val[1];
				$var2 = $param_arr_val[2];
				$var3 = $param_arr_val[3];
				$var4 = $param_arr_val[4];
				$var5 = $param_arr_val[5];
				$var6 = $param_arr_val[6];
				$stmt->bind_param($arr_type,$var,$var1,$var2,$var3,$var4,$var5,$var6);
				break;
			case 8:
				$var = $param_arr_val[0];
				$var1 = $param_arr_val[1];
				$var2 = $param_arr_val[2];
				$var3 = $param_arr_val[3];
				$var4 = $param_arr_val[4];
				$var5 = $param_arr_val[5];
				$var6 = $param_arr_val[6];
				$var7 = $param_arr_val[7];
				$stmt->bind_param($arr_type,$var,$var1,$var2,$var3,$var4,$var5,$var6,$var7);
				break;
			case 11:
				$var = $param_arr_val[0];
				$var1 = $param_arr_val[1];
				$var2 = $param_arr_val[2];
				$var3 = $param_arr_val[3];
				$var4 = $param_arr_val[4];
				$var5 = $param_arr_val[5];
				$var6 = $param_arr_val[6];
				$var7 = $param_arr_val[7];
				$var8 = $param_arr_val[8];
				$var9 = $param_arr_val[9];
				$var10 = $param_arr_val[10];
				$stmt->bind_param($arr_type,$var,$var1,$var2,$var3,$var4,$var5,$var6,$var7,$var8,$var9,$var10);
				break;	
			case 12:
				$var = $param_arr_val[0];
				$var1 = $param_arr_val[1];
				$var2 = $param_arr_val[2];
				$var3 = $param_arr_val[3];
				$var4 = $param_arr_val[4];
				$var5 = $param_arr_val[5];
				$var6 = $param_arr_val[6];
				$var7 = $param_arr_val[7];
				$var8 = $param_arr_val[8];
				$var9 = $param_arr_val[9];
				$var10 = $param_arr_val[10];
				$var11 = $param_arr_val[11];
				$stmt->bind_param($arr_type,$var,$var1,$var2,$var3,$var4,$var5,$var6,$var7,$var8,$var9,$var10,$var11);
				break;	
			case 13:
				$var = $param_arr_val[0];
				$var1 = $param_arr_val[1];
				$var2 = $param_arr_val[2];
				$var3 = $param_arr_val[3];
				$var4 = $param_arr_val[4];
				$var5 = $param_arr_val[5];
				$var6 = $param_arr_val[6];
				$var7 = $param_arr_val[7];
				$var8 = $param_arr_val[8];
				$var9 = $param_arr_val[9];
				$var10 = $param_arr_val[10];
				$var11 = $param_arr_val[11];
				$var12 = $param_arr_val[12];
				$stmt->bind_param($arr_type,$var,$var1,$var2,$var3,$var4,$var5,$var6,$var7,$var8,$var9,$var10,$var11,$var12);
				break;	
			case 14:
				$var = $param_arr_val[0];
				$var1 = $param_arr_val[1];
				$var2 = $param_arr_val[2];
				$var3 = $param_arr_val[3];
				$var4 = $param_arr_val[4];
				$var5 = $param_arr_val[5];
				$var6 = $param_arr_val[6];
				$var7 = $param_arr_val[7];
				$var8 = $param_arr_val[8];
				$var9 = $param_arr_val[9];
				$var10 = $param_arr_val[10];
				$var11 = $param_arr_val[11];
				$var12 = $param_arr_val[12];
				$var13 = $param_arr_val[13];
				$stmt->bind_param($arr_type,$var,$var1,$var2,$var3,$var4,$var5,$var6,$var7,$var8,$var9,$var10,$var11,$var12,$var13);
				break;	
			case 15:
				$var = $param_arr_val[0];
				$var1 = $param_arr_val[1];
				$var2 = $param_arr_val[2];
				$var3 = $param_arr_val[3];
				$var4 = $param_arr_val[4];
				$var5 = $param_arr_val[5];
				$var6 = $param_arr_val[6];
				$var7 = $param_arr_val[7];
				$var8 = $param_arr_val[8];
				$var9 = $param_arr_val[9];
				$var10 = $param_arr_val[10];
				$var11 = $param_arr_val[11];
				$var12 = $param_arr_val[12];
				$var13 = $param_arr_val[13];
				$var14 = $param_arr_val[14];
				$stmt->bind_param($arr_type,$var,$var1,$var2,$var3,$var4,$var5,$var6,$var7,$var8,$var9,$var10,$var11,$var12,$var13,$var14);
				break;
			case 16:
				$var = $param_arr_val[0];
				$var1 = $param_arr_val[1];
				$var2 = $param_arr_val[2];
				$var3 = $param_arr_val[3];
				$var4 = $param_arr_val[4];
				$var5 = $param_arr_val[5];
				$var6 = $param_arr_val[6];
				$var7 = $param_arr_val[7];
				$var8 = $param_arr_val[8];
				$var9 = $param_arr_val[9];
				$var10 = $param_arr_val[10];
				$var11 = $param_arr_val[11];
				$var12 = $param_arr_val[12];
				$var13 = $param_arr_val[13];
				$var14 = $param_arr_val[14];
				$var15 = $param_arr_val[15];
				$stmt->bind_param($arr_type,$var,$var1,$var2,$var3,$var4,$var5,$var6,$var7,$var8,$var9,$var10,$var11,$var12,$var13,$var14,$var15);
				break;				
			default:
				
			}
		}

		//$stmt->execute();			
		if($stmt->execute()){
		    //print 'Successfully inserted and Last inserted ID is : ' .$stmt->insert_id .'<br />'; 
		    $rtn =1;
		}else{
			$rtn =0;
		    echo "Error while updating data: Please try again"; //.mysqli_stmt_error($stmt)."**************" . $dbcon->error;
		    exit();
		}
		//$rtn_row =  $stmt->affected_rows;
		$stmt->close();
		$this->chk_upd_token();
		
		return $rtn ;
	
	}

	//Token function added
	function chk_upd_token(){

		$dbcon = $this->connect();
		//mysqli_set_charset( $dbcon, 'utf8');
		$dbcon->set_charset('utf8');

		$key = 'mcrypt@encrypt!logout';
		$date = new DateTime("now");
		$dateTime = $date->format("Y-m-d H:m:s");
		$token_key =$_SERVER['REMOTE_ADDR'].$_SESSION["admin_id"].$dateTime.$key;
		$iv = md5(md5($token_key));
		$update_token ="update fw_token_tbl set token='".$iv."' where session_id=".$_SESSION["admin_id"];


		$stmt = $dbcon->prepare($update_token); 
		
		//$stmt->execute();
		if($stmt->execute()){
		    //print 'Successfully inserted and Last inserted ID is :  <br />'; 
		    
		}else{
		    //echo "Prepare failed: -----".mysqli_stmt_error($stmt)."**************" . $dbcon->error;
		    echo "Operation failed: Please try again";
		    exit();
		}
		//echo $query, $bindParam,$paramValue; 
		//exit();
		$result = $stmt->get_result();
		$stmt->close();

		return $result;

	}
	//Token end


	//this function check if record exist or not
	function execute_check($query, $bindParam,$paramValue)
	{
		$rtn =1;
		$dbcon = $this->connect();		
		$result = pg_prepare($dbcon, "prepare1", $query);
		$result = pg_execute($dbcon, "prepare1", $paramValue);
		$row = pg_num_rows($result);
		if ($row ==0){ $rtn =0;}		
		pg_close($dbcon); 		
		return $rtn;		
	} 


	// This function returns a resultset
	function execute_query($query)
	{
		//die($query);
		//echo "**********".$query;
		$result = mysql_query($query,$this->conn);
		//print_r($result);
	  	//echo mysql_error();  
	  	return $result;			

	}
	
	
	//This function returns each rows value arraylist from a resultset
	function return_query($query)
	{
		$result = mysql_query($query,$this->conn);
	  	echo mysql_error($this->conn);
	  	$value = mysql_fetch_array($result, MYSQL_ASSOC);
	  	return $value;	
	}
	
	//This method is used to remove slashes added using the addslash function
	function strip_slashes($value)
	{		
		reset($value);
		while (list($key, $val) = each($value))
		{
		  $value[$key] = (stripslashes($val));
		}
		return $value;
	}
	
	//This method is used to snatize a string for safe database entry
	function sanatize_string($value)
	{		
		$value = htmlentities(addslashes(strip_tags(trim($value))));		
		return $value;
	}
	
	//This method checks if a variable has been set in an array if yes the returns value else returns blank
	function check_value($array,$variable,$return)
	{
		if(isset($array[$variable]))
		{
			return($array[$variable]);
		}
		else
		{
			return($return);
		}	
	}
	
	// This method compares a variable1 with variable 2 and if same returns variable 3 else returns variable 1 itself
	function sring_compare($compare,$with,$return)
	{
		if($compare != $with)
		{
			return($compare);
		}
		else
		{
			return($return);
		}	
	}
	
	// This method compares a variable1 with variable 2 and if same returns [return1] else returns [return2]
	function return_compare($compare,$with,$return1,$return2)
	{
		if($compare == $with)
		{
			return($return1);
		}
		else
		{
			return($return2);
		}	
	}
	
	//This method combines 2 arrays to create a new array taking array 1 for keys and array 2 for values
	function combine_array($array_1,$array_2)
	{
		$new_array = array();		
		$i = 0;	
		while($i < count($array_1))
		{	
			$key = $array_1[$i];		
			$new_array = array_merge($new_array,array("'".$key."'"=>$array_2[$i]));
			$i++;			
		}		
		return $new_array;
	}
	
	//This function checks for the specified variables to see if they are numbers only
	function sanatize_query()
	{
		$array = $_GET;
		$count = count($_GET);
		$permitted = " 1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ*#_.";
		$querystring = "?";
		$isOk = true;
		
		foreach($array as $key => $val)
		{				
				if(strlen($val) <= 0)
				{					
					$val = "0";
					$isOk = false;
				}
				
				$fdata = "";
				for($i=0;$i<strlen($val);$i++)
				{
					$char = substr($val,$i,1);
					
					if($char == ' ')
					{
						$fdata .= ' ';
						continue;
					}
										
					if(strpos($permitted,$char))
					{									
						$fdata .= $char; 						
					}
					else
					{
						$isOk = false;
					}
				}
				$querystring .= $key."=".$fdata."&";								
		}		
		$querystring = substr($querystring,0,strlen($querystring)-1);
		if(!$isOk)
		{
		
			header("location:".$_SERVER['PHP_SELF']."$querystring");
			die("Not OK");
		}		
		return true;
	}
	
	function sanatize_value($value)
	{
		$permitted = " 1234567890";				
		
		$fdata = "";				
		for($i=0; $i < strlen($value);$i++)
		{			
			$char = substr($value,$i,1);						
			if(strpos($permitted,$char))
			{
				$fdata .= $char;												
			}						
		}
		
		if(strlen(trim($fdata)) <= 0)
		{					
			$fdata = "0";
		}
				
		return $fdata;
	}
					 
	//This function us used to close a database connection
	function con_close()
	{
		//mysql_close($this->conn);
	}
}

//**********This is the sql helper class for adding updating and deleting records**********

class sqlhelper extends db_connect
{
	var $table;
	
	function __construct($table)
	{
		$this->table = $table;
	}

	
	function checkStatus($where)
	{
		/*$query = "select * from ".$this->table." where $where";	
		$dbcon = $this->connect();
		$stmt = $dbcon->prepare($query); 		
		$stmt->execute();
		$arr = $stmt->get_result()->fetch_all(MYSQLI_NUM);
		if(!$arr)
        {
        	return 0;
        }
    	else 
    	{
    		return 1;
    	}*/
    	$query = "select * from ".$this->table." where $where";	

		if(mysql_fetch_array($this->execute_query($query)) !== false)
        {
        	return 1;
        }
    	else 
    	{
    		return 0;
    	}

	}

	function adddata($columns, $values)
	{
		$query = "INSERT INTO ".$this->table." ($columns) VALUES ($values)";	
		$this->execute_query($query);
	}
	

	function updatedata($columns, $values, $where, $callback="")
	{
		$query 		= "UPDATE ".$this->table." SET ";
		
		$columns 	= explode(",",$columns);
		$values		= explode("|",$values);				

		reset($columns);
		reset($values);

		foreach($columns as $col)
		{
			$query = $query.$col."=". current($values).", ";			
			next($values);
		}
		
		if($callback != "")
		{
			$valid = explode("=",$where);
			$callback($valid[1]);
		}
		
		$query = rtrim($query,", ");
		$query = $query." WHERE  $where";
		$this->execute_query($query);				

	}
	
	function deletedata($where,$lessvar=1,$callback="")
	{
		$cnt = 1;		
		$a = array_flip($_POST);
		$total_cnt = count($a) - $lessvar;		
		if ($total_cnt > 0)
		{
			while ($cnt <=  $total_cnt) 
			{
				$val = key($a);	
				
				if($callback != "")
				{
					$callback($val);
				}
				
				$query = "DELETE FROM ".$this->table." WHERE $where=$val";
				$this->execute_query($query);				
				next($a);
				$cnt++;
			}
		}
	}

	function cleanText($string) {
	   //$string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
	   $string = preg_replace('/[^A-Za-z0-9\-]/', ' ', $string); // Removes special chars.
       return strip_tags(addslashes($string));
	}
	function cleanURL($string) {
	   $character = array("<", ">", "javascript", "script","alert");
	   $string = str_replace($character,".",$string);
       return $string;
	}
}

//*********This class is used to provide pagination property to a recordset****************
class pagination extends db_connect
{
	var $query, $limit, $params, $page, $start;	
	var $pgback, $pgnext, $reccount;
	
	function __construct() // Pagination constructor
	{
		$this->start 	= 1;
		$this->limit 	= "10";
		
		
		if(strlen($_SERVER['QUERY_STRING']) > 0)
		{
			if(!isset($_REQUEST["start"]))
			{
				$this->params = $_SERVER['QUERY_STRING'];	
			}
			else
			{
				$inipos = strpos($_SERVER['QUERY_STRING'],"&")+1;
				$this->params	= substr($_SERVER['QUERY_STRING'],$inipos);
			}
		}
		
		/*******************************************************************						
		if(strpos($_SERVER['QUERY_STRING'],"&") > 0)
		{
			$inipos = strpos($_SERVER['QUERY_STRING'],"&")+1;			
		}
		else
		{
			$inipos = strpos($_SERVER['QUERY_STRING'],"&");
		}		
		$this->params	= substr($_SERVER['QUERY_STRING'],$inipos);	
		********************************************************************/
			
		$this->page 	= substr($_SERVER['SCRIPT_FILENAME'],strrpos($_SERVER['SCRIPT_FILENAME'],"/")+1);				
	}
	
	//This function initializes the pagination sequence and returns the executed query
	function ini_paging()
	{
	 	$this->reccount = mysql_num_rows($this->execute_query($this->query));
		$this->pgback 	= $this->start - 1;
		$this->pgnext	= $this->start + 1;
		$from = (($this->start - 1) * $this->limit);
		$to = $this->limit;
		return($this->execute_query($this->query." limit ".$from.",".$to));
	}
	
	//This function displays the bottom pagination
	function display_paging()
	{
		$prev 	= "";
		$paging = "";
		$next	= "";
		
		if($this->pgback > 0)
		{				
				$prev = "<a href=".$this->page."?start=".$this->pgback."&".$this->params.">PREV</a>";				
		}						 		 
								 
		 for($i=($this->start - 5); $i < ($this->start + 5);$i++)
		 {
				if($i <> $this->start)
				{
					if($i > 0 && $i <= ceil($this->reccount/$this->limit))
					{					
						$paging = $paging."<a href=".$this->page."?start=".$i."&".$this->params.">".$i."</a>";
					}
				}	
				else 
				{ 
					$paging = $paging. "<font color=red>".$this->start."</font>";
				} 
		 }
		 		 
		 if($this->pgnext <=  ceil($this->reccount/$this->limit))
		 {		 		
				$next = "<a href=".$this->page."?start=".$this->pgnext."&".$this->params.">NEXT</a>";				
		 }
		 
		 return($prev."&nbsp;&nbsp;".$paging."&nbsp;&nbsp;".$next);
	}
}

//************This class is used to perform image manupulation functions**************
class img_manup
{
	
	var $folderpath;
	
	//This function initializes the path to the folder conatining the image or where the image needs to be uploaded
	function __construct($folderpath)
	{
		$this->folderpath = $folderpath;
	}
	
	//This function checks if an image exists or else returns default image
	function get_image($file_name) 
	{
		if(!empty($file_name))
		{
			return $file_name; 	
		}
		else
		{
			return $this->folderpath."/no_image.jpg";
		}
	}
	
	// This function returns the height and width of the image as an array
	function get_size($filename)
	{
		$imagepath = $this->folderpath ."/".$filename;
		$imagesize = getimagesize($imagepath);
		
		return $imagesize;
	}	
		
	// This function returns the img tag with proportionate height and width of an image as per specified in the target variable
	function imageResize($filename, $area, $titletext="", $atltext="", $classtext="") 
	{
		$imagepath = $this->folderpath ."/".$filename;
		$imagesize = getimagesize($imagepath); 
		
		$width 	= $imagesize[0];
		$height = $imagesize[1];
		
		if ($width > $height) 
		{ 
			$percentage = ($area / $width); 
		} 
		else 
		{ 
			$percentage = ($area / $height); 
		} 
			
		$width = round($width * $percentage); 
		$height = round($height * $percentage); 
	
		return "<img src='$imagepath' width=\"".$width."\" height=\"".$height."\" border=\"0\" title=\"".$titletext."\" alt=\"".$atltext."\" class=\"".$classtext."\"/>"; 
	} 
	
	// This function validates if a file is a gif or jpg file
	function validate_image($filename)
	{
		$image_ext = substr($filename,strlen($filename) - 4,strlen($filename));			
		
		if($image_ext != ".gif" && $image_ext != ".jpg" && $image_ext != ".png")
		{
			$image_result[0] = "False";
			$image_result[1] = "";
		}
		else
		{
			$image_result[0] = "True";		
			$image_result[1] = $image_ext;
		}			
		return $image_result;
	}
	
	// This function is used to resize an uploaded image
	function createThumbs($filename, $imgratio, $text="") 
	{
		$image_result = $this->validate_image($filename);
		
		if($image_result[0] == "True")
		{
			$pathToImages = $this->folderpath ."/".$filename;;
			$pathToThumbs = $this->folderpath ."/".$filename;;
			
			// load image and get image size
			switch($image_result[1])
			{
				case ".jpg":
					$img = imagecreatefromjpeg("$pathToImages");
				break;
				
				case ".gif":
					$img = imagecreatefromgif("$pathToImages");
				break;
				
				case ".png":
					$img = imagecreatefrompng("$pathToImages");
				break;			
			}
						
			$width = imagesx( $img );
			$height = imagesy( $img );
				
			// calculate thumbnail size
			if($width > $height)
			{
				$new_width = $imgratio;
				$new_height = floor( $height * ( $imgratio/ $width ) );
				}
			else
			{
				$new_height = $imgratio;
				$new_width = floor( $width * ( $imgratio/ $height ) );
			}
			
			// create a new temporary image and maintain its transperancy
			$tmp_img = imagecreatetruecolor( $new_width, $new_height );
			imagealphablending($tmp_img, false);
			imagesavealpha($tmp_img,true);
			$transparent = imagecolorallocatealpha($tmp_img, 255, 255, 255,127);
			imagefilledrectangle($tmp_img, 0, 0, $new_width, $new_height, $transparent);
			
			// copy and resize old image into new image
			imagecopyresized($tmp_img, $img, 0, 0, 0, 0, $new_width, $new_height, $width, $height );
			

			//font color 
			$white = imagecolorallocate($tmp_img, 0xFF, 0xFF, 0xFF);
			// Path to our ttf font file
			$font_file = $this->folderpath.'verdana.ttf';
			 
			//Write text on image
			imagefttext($tmp_img, 10, 0, 12, $new_height - 8, $white, $font_file, $text);			
			 
			// save thumbnail into a file
			switch($image_result[1])
			{
				case ".jpg":
					imagejpeg($tmp_img, "$pathToThumbs");
				break;
				
				case ".gif":
					imagegif($tmp_img, "$pathToThumbs");
				break;
				
				case ".png":
					imagepng($tmp_img, "$pathToThumbs");
				break;			
			}
			imagedestroy($img);			
		}
		else
		{
			exit();
		}											  	  
	}

}

//**************This class is used for performing file manupulation******************
class file_manup
{
	var $folderpath;
	var $content;
	var $mime_type = array('pdf' => 'application/pdf', 'doc' => 'application/msword', 'png' => 'image/png','jpg' => 'image/jpeg', 
					       'jpeg' => 'image/jpeg','gif' => 'image/gif', 'mp4' => 'video/mp4', 'mpg' => 'video/mpeg', 'mpeg' => 'video/mpeg', 'mov' => 'video/quicktime', 'avi' => 'video/x-msvideo','swf' => 'application/x-shockwave-flash', 'flv' => 'video/x-flv');
	
	//This function initializes the path to the folder conatining the image or where the image needs to be uploaded
	function __construct($folderpath)
	{
		$this->folderpath = $folderpath;
	}
	
	//This function validates a file against the array of extensions and if the file being uploaded is ok then uploads it
	function upload_file($sourcename, $tmpname, $dstnname, $ext_array, $mime="")
	{
		$file_ext  = substr($sourcename,strlen($sourcename) - 4,strlen($sourcename));		
		
		$ext_array = explode(",",$ext_array);
		$file_ok   = false;
		
		if($mime != "")
		{				
			foreach($ext_array as $key => $val)
			{				
				for($i=0; $i<count($this->mime_type); $i++)
				{					
					if($mime == $this->mime_type[$val])					{
						$file_ok = true;
						break;
					}
				}
			}
		}
		else
		{
			if($sourcename!="")
			{
				$file_ok	= true;
			}
			else
			{
				$file_ok	= false;
			}
			
		}
		
		if($file_ok == true)
		{
			$uploaddir = $this->folderpath;
			$uploadfile = $uploaddir.$dstnname.$file_ext;
			move_uploaded_file($tmpname, $uploadfile);
			return($dstnname.$file_ext);
		}
		else
		{
			return false;
		}					
	}
	
	//This function will be used to delete files passed as an array from a folder, returns count of total files and deleted files  
	function delete_file($file_list)
	{
		$file_list 	= split(",",$file_list);
		reset($file_list);
		$int[0] = count($file_list);
		$int[1] = 0;
		while (list($key, $val) = each($file_list))
		{
			$file = $this->folderpath.$val;
			if(file_exists($file))
			{
				if(unlink($file))
				{
					$int[1]++;
				}
			}		  
		}
		return $int;
	}

	//This function is used to extract html from a different URL and update the $content property of the class with 
	//the extracted content
	function extract_html($filename)
	{
		$filename = $this->folderpath.$filename;
		if($file = fopen($filename,"r"))
		{
			$content = "";
		   	while(!feof($file))
		  	{
		  		$this->content = $this->content.fgets($file);
		 	}
			fclose($file);
			return($this->content);			
		}
		else
		{
			echo("File is not readable!");
			die();
		} 
	}
	
	//This function is used to replace specified tags within a {} with a list of words specified in an array
	function replace_tags($tags,$values) 
	{
		$tags 	= explode(",", $tags);
		$values = explode(",",$values);
		reset($tags);
		reset($values);
		for($i=0;$i<count($tags);$i++)
		{			
			$this->content = str_replace($tags[$i],$values[$i],$this->content);
		}
		return($this->content);
	}	 
}

//*************This class is used to perform various email related functions********************************
class smtp_mailer
{			
	var $smtphost, $smtpuser, $smtppass;
	
	function __construct($smtphost="", $smtpuser="", $smtppass="")
	{
		$this->smtphost = $smtphost;							// specify main and backup server [mail.kalptech.com]
		$this->smtpuser = $smtpuser;							// SMTP username [support@kalptech.com]
		$this->smtppass = $smtppass;							// SMTP password [ks0679*]
	}
	// This function is used for sending emails with attachments, it utilizes the PHP mailer base class functionality	
	function send_mail($from,$to,$attachments,$subject,$body)
	{
		require("phpmailer/class.phpmailer.php");
		
		$smtphost = $this->smtphost;
		$smtpuser = $this->smtpuser;
		$smtppass = $this->smtppass;
				
		$mail = new PHPMailer();	
		if(!empty($smtphost))
		{
			$mail->IsSMTP();                                   	// set mailer to use SMTP
			$mail->Host 	= $smtphost;  					
			$mail->SMTPAuth = true;     						// turn on SMTP authentication
			$mail->Username = $smtpuser;  				
			$mail->Password = $smtppass; 					
		}
		else
		{
			$mail->IsMail();
		}
		
		$from = explode(",",$from);								//Name,Email
		$mail->From = $from[0];
		if(isset($from[1]))
		{
			$mail->FromName = $from[1];
		}

		$to	= explode(",",$to);									//Name,Email
		if(isset($to[1]))
		{
			$mail->AddAddress($to[0], $to[1]);					// name is optional					
		}
		else
		{
			$mail->AddAddress($to[0]);							// name is optional										
		}
		
		$mail->AddReplyTo($from[0]);		
		$mail->WordWrap = 125;									// set word wrap to 50 characters
		
		if(!empty($attachments))
		{
			$attachments	= explode(",",$attachments);
			reset($attachments);
			for($i=0;$i<count($attachments);$i++)
			{		                                 					   	
				$mail->AddAttachment($attachments[$i]);         // add attachments
			}
		}
				
		$mail->IsHTML(true);                                  	// set email format to HTML
		
		$mail->Subject = strip_tags($subject);
		$mail->Body    = $body;
		$mail->AltBody = strip_tags($body);
		
		if(!$mail->Send())
		{
		   //echo "Message could not be sent. <br/>";
		   //echo "Mailer Error: " . $mail->ErrorInfo;
		   //exit;
		   return false;
		}
		else
		{
			//echo"Message has been sent successfully";
			return true;
		}		
	}
}

//This Class will be utilized to access a mailbox using PHP IMAP class library	
class imap_access
{	
	var $imapserver, $imapuser, $imappass, $imapconn;
	var $folders, $emailcount, $headers;
	
	//This is the constructor of the class
	function __construct($imapserver, $imapuser, $imappass)
	{
		$this->imapserver 	= $imapserver; 	//{mail.kalptech.com:110/pop3}INBOX
		$this->imapuser 	= $imapuser;	//kalpesh@kalptech.com
		$this->imappass 	= $imappass;	//kalpum
	}
	
	function access_mail()
	{
		$this->imapconn		= @imap_open($this->imapserver, $this->imapuser, $this->imappass) or die(imap_last_error());		
		$this->folders 		= imap_listmailbox($this->imapconn, $this->imapserver, "*"); // This property returns the list of mail folders
		
		if ($this->folders == false) 
		{
			echo "Call to the folders failed!";
		} 
		
		$this->headers = imap_headers($this->imapconn);	//This property returns total messages in the form of an array	
		$this->emailcount = sizeof($this->headers); // This property returns the count of total messages				
	}
	
	//This function will close the connection to the opened imap mail box
	function close_conn()
	{
		imap_close($this->imapconn);	
	}
	
	//This function returns the headers of an email
	function msg_headers($msg_no)
	{
		$mailHeader = imap_headerinfo($this->imapconn, $msg_no);
		
		$msg_headers["recent"] 	= $mailHeader->Recent;
		$msg_headers["from"] 	= $mailHeader->fromaddress;
		$msg_headers["replyto"] = $mailHeader->reply_toaddress;
		$msg_headers["subject"] = $mailHeader->subject;
		$msg_headers["date"] 	= $mailHeader->date;
		$msg_headers["Size"] 	= $mailHeader->Size;
		$msg_headers["From"]	= $mailHeader->from;									
		
		return($msg_headers);
	}
	
	//This function returns the body of an email ***** Still there is some work to be done on this *****
	function msg_body($msg_no)
	{
		return imap_fetchbody($this->imapconn,$msg_no,1);
	}


}

?>

<?php

//Generic query
//$fetch_rows="select * from ? where ?";	
//Session Logout
$update_login="update fw_administrator set is_login =? where email=?";
$update_check_login="select * from fw_administrator where email=?";

//Manage Admin
$fetch_manage_admin="SELECT * from fw_administrator order by admin_id DESC";

$fetch_edit_admin="SELECT * from fw_administrator where md5(admin_id::text)=$1";

$fetch_insert_admin="INSERT into fw_administrator(name,email,pass,admin_role,created_on) values($1,$2,$3,$4,$5)";

$check_exist_admin="SELECT * from  fw_administrator where name =$1 and email =$2 and admin_role =$3 ";

$delete_by_id_admin="DELETE from  fw_administrator where md5(admin_id::text) =$1 ";

$update_admin="UPDATE fw_administrator set name =$1 ,email = $2 ,pass =$3 ,admin_role= $4 ,created_on =$5 where md5(admin_id::text)=?";

//Manage Menu
$fetch_manage_menu="SELECT * from fw_menu_master where 1=$1 order by menu_id DESC";
$fetch_manage_menu_type="SELECT * FROM fw_menu_type_master where is_active =$1";
$fetch_menu_check="SELECT * from fw_menu_master where url=$1 and menu_name = $2 and menu_name_hindi =$3 and url_hindi =$4";

$fetch_edit_menu="SELECT * from fw_menu_master where md5(menu_id::text)=$1";

$fetch_insert_menu = "INSERT into fw_menu_master(menu_type, url, menu_name,menu_name_hindi, parent_menu,flag, sidebar, url_hindi, target,target_hindi,ordering,circular,created_by, created_on) values($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14)";

$fetch_update_menu = "UPDATE fw_menu_master set menu_type=$1, url=$2, menu_name=$3,menu_name_hindi=$4, parent_menu=$5,flag=$6, sidebar=$7, url_hindi=$8, target=$9, target_hindi=$10,ordering=$11,circular=$12, created_by=$13, updated_on =$14 where md5(menu_id::text)=$15";


$delete_menu= "UPDATE fw_menu_master set flag=0 where md5(menu_id::text)=$1";

//Manage content
$fetch_manage_content="SELECT *,c.created_on as cret_date from fw_content_master c INNER JOIN fw_menu_master m on m.menu_id = c.menu_id where 1=$1 order by c.cid DESC";
$fetch_edit_content="SELECT * from fw_content_master c  INNER JOIN fw_menu_master m on m.menu_id = c.menu_id where md5(c.cid::text)=$1";
$fetch_content_cnt = "SELECT count(*) as con_cnt from fw_content_master c  where c.is_active =1 and c.menu_id =$1";
$fetch_edit_menu_mstr="SELECT * from fw_menu_master where flag=$1 order by menu_name ASC";
$fetch_insert_content = "INSERT into fw_content_master(menu_id,parent_menu_id,banner, page_name,page_title, content_title_eng, content_title_hindi,main_content_eng, main_content_hindi, meta_keywords, meta_desc, is_active,start_date,end_date,created_on) values($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15)";

$fetch_insert_content_13= "INSERT into fw_content_master(menu_id,parent_menu_id,banner,page_name,page_title,content_title_eng,content_title_hindi,main_content_eng,main_content_hindi,meta_keywords,meta_desc,is_active,created_on)values($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13)";

$update_content="UPDATE fw_content_master set menu_id=$1,parent_menu_id=$2,banner=$3, page_name=$4,page_title=$5, content_title_eng=$6, content_title_hindi=$7,main_content_eng=$8, main_content_hindi=$9, meta_keywords=$10, meta_desc=$11, is_active=$12,start_date=$13,end_date=$14,updated_on=$15  where md5(cid::text)=$16";

$update_content_14="UPDATE fw_content_master set menu_id=$1,parent_menu_id=$2,banner=$3, page_name=$4,page_title=$5, content_title_eng=$6, content_title_hindi=$7,main_content_eng=$8, main_content_hindi=$9, meta_keywords=$10, meta_desc=$11, is_active=$12,start_date=$13,end_date=$14,updated_on=$15  where md5(cid::text)=$16";

$content_archive ="INSERT INTO fw_content_history (menu_id, url, parent_menu_id, banner, page_name, page_title, content_title_eng, content_title_hindi, main_content_eng, main_content_hindi, meta_keywords, meta_desc, start_date, end_date, created_on) SELECT menu_id, url, parent_menu_id, banner, page_name, page_title, 
            content_title_eng, content_title_hindi, main_content_eng, main_content_hindi, meta_keywords, meta_desc, start_date, end_date, now() FROM fw_content_master
    WHERE md5(cid::text)=$1";

$delete_content = "UPDATE fw_content_master set is_active =0 where md5(cid::text) = $1";


//Manage Feedback
$fetch_manage_feedback = "SELECT * from fw_feedback_master order by id DESC";
$fetch_manage_feedback_dtls = "SELECT * from fw_feedback_details where 1=$1 order by fd_feedback_id DESC limit 100";


//Portal last Update date
$fetch_update_date ="SELECT * from fw_portal_update_date where 1=$1 order by 1 desc" ;
$insert_update_date ="INSERT into fw_portal_update_date(lst_update_tmstmp,modify_dtls,created_by,created_on) values($1, $2,$3,$4)";


//Add photo
$fetch_edit_photo="SELECT * from  fw_gallery_data_master where md5(data_id::text) =?";
$insert_photo = "INSERT into fw_gallery_data_master(gallery_id,type, title_eng, title_hindi, photo, created_on) values(?,?,?,?,?,?)";

$delete_photo = "DELETE from fw_gallery_data_master where md5(data_id::text) =?";


//Manage Archieve
$fetch_manage_archieve="SELECT * from  fw_archieve_master order by aid DESC";
$fetch_edit_archieve="SELECT * from fw_content_master where md5(content_id::text)=?";
$delete_archieve = "DELETE from fw_content_master where md5(content_id::text)=?";
$fetch_create_archieve ="SELECT * from fw_content_master where md5(cid::text)=?";

$insert_archieve = "INSERT into fw_archieve_master (menu_id,content_id,url,parent_menu_id,banner, page_name,page_title, content_title_eng, content_title_hindi,main_content_eng, main_content_hindi, meta_keywords, meta_desc, options,created_by,created_on) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

$update_archieve = "UPDATE fw_archieve_master set menu_id=?, parent_menu_id=?,banner=?, page_name=?,page_title=?, content_title_eng=?, content_title_hindi=?,main_content_eng=?, main_content_hindi=?, meta_keywords=?, meta_desc=?, options=?,created_on =? where md5(cid)=?";

$update_archieve_13 = "update fw_archieve_master set menu_id=?, parent_menu_id=?, page_name=?,page_title=?, content_title_eng=?, content_title_hindi=?,main_content_eng=?, main_content_hindi=?, meta_keywords=?, meta_desc=?, options=?,created_on =? where md5(cid)=?";

$update_archieve_restore = "update fw_archieve_master set restored_by=?,restored_on=? where md5(aid)=?";

$update_archieve_content = "update fw_content_master set menu_id=?, parent_menu_id=?,banner=?, page_name=?,page_title=?, content_title_eng=?, content_title_hindi=?,main_content_eng=?, main_content_hindi=?, meta_keywords=?, meta_desc=?, options=?,created_on =? where md5(cid)=?";

//Managa Gallery
$fetch_manage_gallery = "SELECT * from fw_gallery_master order by gallery_id DESC";
$fetch_edit_gallery="SELECT * from fw_gallery_master where md5(gallery_id)=?";

$fetch_insert_gallery ="insert into fw_gallery_master(type, title_eng, title_hindi, photo, created_on) values(?,?,?,?,?)";

$update_gallery="update fw_gallery_master set type=?, title_eng=?, title_hindi=?, photo=?, created_on=? where md5(gallery_id) =?";
$update_gallery_4="update fw_gallery_master set type=?, title_eng=?, title_hindi=?, created_on=? where md5(gallery_id) =?";

$delete_gallery = "DELETE from fw_gallery_master where md5(gallery_id) = ?";

//Manage Slider
$fetch_manage_slider ="SELECT * from fw_slider_master where 1=$1 order by slider_id DESC";
$fetch_edit_slider ="SELECT * from fw_slider_master where md5(slider_id::text)=$1";

$fetch_insert_slider ="INSERT into fw_slider_master(slider_name, slider_type, slider_url, slider_data, img, flag,created_by, created_on) values($1,$2,$3,$4,$5,$6,$7,$8)";

$update_slider="UPDATE fw_slider_master set slider_name=$1, slider_type =$2, slider_url=$3, slider_data=$4, img=$5, flag=$6,created_by=$7, updated_on=$8 where md5(slider_id::text) =$9";

$update_slider_1="UPDATE fw_slider_master set slider_name=$1, slider_type=$2, slider_url=$3, slider_data=$4, flag=$5, created_by=$6, updated_on=$7 where md5(slider_id::text) =$8";

$delete_slider = "UPDATE fw_slider_master set flag=0 where md5(slider_id::text) = $1";

//Manage Download
$fetch_manage_download ="SELECT * from fw_download_master where flag=$1 order by dwnld_id DESC";

$fetch_edit_download ="SELECT * from fw_download_master where md5(dwnld_id::text)=$1";

$fetch_insert_download ="INSERT into fw_download_master(dwnld_file_nm, dwnld_title_eng, dwnld_title_hindi, dwnld_url, dwnld_text_eng, dwnld_text_hindi, dwnld_size, dwnld_type, flag, created_by, created_on) values($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11)";

$update_download_file="UPDATE fw_download_master set dwnld_file_nm=$1, dwnld_title_eng =$2, dwnld_title_hindi=$3, dwnld_url=$4, dwnld_text_eng=$5, dwnld_text_hindi=$6,dwnld_size=$7, dwnld_type=$8, flag=$9, updated_on=$10 where md5(dwnld_id::text) =$11";

$update_download="UPDATE fw_download_master set dwnld_title_eng =$1, dwnld_title_hindi=$2, dwnld_text_eng=$3, dwnld_text_hindi=$4, flag=$5, updated_on=$6 where md5(dwnld_id::text) =$7";

$delete_download = "UPDATE fw_download_master set flag=0 where md5(dwnld_id::text) = $1";

//Manage Online
$fetch_manage_online = "SELECT * from fw_online_master order by id DESC limit 1000";


//Dashboard

$fetch_online_cnt = "SELECT count(1) as cnt from fw_online_master where 1=$1";

$fetch_online_dt ="SELECT * from fw_online_master where date >=$1 and date <= $2";
$fetch_menu_cnt = "SELECT * from fw_menu_master where 1=$1";

//Token

$fetch_token ="SELECT * from fw_token_tbl where md5(session_id::text) =$1 ";
$verify_token ="SELECT * from fw_token_tbl where session_id =$1 and token =$2";

$update_token ="UPDATE fw_token_tbl set token=$1 where session_id=$2";

//Manage FAQ Category Master

$fetch_faq_sub_cat = "SELECT * from fw_faq_cat_master where faq_cat_parent =0 and flag =$1 order by faq_cat_id DESC";

$fetch_faq_mstr_map = "SELECT faq_cat_id as cat_id, faq_category_name as category, (SELECT a.faq_category_name FROM fw_faq_cat_master a where a.faq_cat_id = b.faq_cat_parent) as parent_cat, flag as flg, created_on as created FROM fw_faq_cat_master b  where 1 =$1 ";

$edit_faq_mstr_map = "SELECT faq_cat_id as cat_id, faq_category_name as category, (SELECT a.faq_category_name FROM fw_faq_cat_master a where a.faq_cat_id = b.faq_cat_parent) as parent_cat, flag as flg, created_on as created FROM fw_faq_cat_master b where md5(b.faq_cat_id::text)=$1";

$fetch_edit_faq_mstr ="SELECT * from fw_faq_cat_master where md5(faq_cat_id::text)=$1";
$fetch_parent_cat ="select * from fw_faq_cat_master where faq_cat_parent = $1";
$fetch_parent_faq_mstr ="select * from fw_faq_cat_master where faq_cat_id =(select faq_cat_parent from fw_faq_cat_master where faq_cat_id = $1)";
$fetch_child_faq_mstr ="SELECT faq_cat_id, faq_category_name from fw_faq_cat_master where flag =1 and faq_cat_parent=$1";

$fetch_insert_faq_mstr ="INSERT into fw_faq_cat_master(faq_category_name,faq_cat_parent,faq_category_desc,flag,created_by, created_on) values($1,$2,$3,$4,$5,$6)";

$update_faq_mstr="UPDATE fw_faq_cat_master set faq_category_name=$1,faq_cat_parent=$2,faq_category_desc=$3, flag=$4, created_by=$5, updated_on =$6 where md5(faq_cat_id::text)=$7";

$delete_faq_mstr = "UPDATE fw_faq_cat_master set flag =0 where md5(faq_cat_id::text)=$1";

//Manage FAQ Ques-Ans

$fetch_faq = "SELECT faq_id,(SELECT faq_category_name from fw_faq_cat_master where faq_cat_id =faq_category_id) as category_name,faq_question,faq_ans,flag,created_on as cret_date from fw_faq_dtls where  1 =$1 order by faq_id DESC";

$fetch_check_faq = "SELECT * from fw_faq_dtls where faq_category_id =$1 and faq_question=$2 and faq_ans=$3";

$fetch_edit_faq ="SELECT faq_cat_parent,(select faq_category_name from fw_faq_cat_master c where c.faq_cat_id = a.faq_cat_parent) as parent_name,faq_cat_id, faq_category_name, faq_question,faq_ans,b.flag as sc_flg from fw_faq_cat_master a right join fw_faq_dtls b on faq_cat_id =faq_category_id where md5(faq_id::text)=$1";

$fetch_insert_faq ="INSERT into fw_faq_dtls(faq_category_id,faq_question,faq_ans,flag,created_by, created_on) values($1,$2,$3,$4,$5,$6)";

$update_faq="UPDATE fw_faq_dtls set faq_category_id=$1,faq_question=$2,faq_ans=$3, flag=$4, updated_on =$5 where md5(faq_id::text)=$6";

$delete_faq = "UPDATE fw_faq_dtls set flag=0 where md5(faq_id::text)=$1";



//Manage gallery
$fetch_photo_gallery_count ="SELECT * from fw_photo_gallery_master where 1=$1 order by gallery_id DESC limit 1";
$fetch_manage_photo_gallery ="SELECT * from fw_photo_gallery_master where 1=$1 order by gallery_id DESC";
$fetch_edit_photo_gallery ="SELECT * from fw_photo_gallery_master where md5(gallery_id::text)=$1";

$fetch_insert_photo_gallery ="INSERT into fw_photo_gallery_master(gallery_id,gallery_name, gallery_data, img, flag,created_by, created_on) values($1,$2,$3,$4,$5,$6,$7)";

$update_photo_gallery="UPDATE fw_photo_gallery_master set gallery_name=$1, gallery_data=$2, img=$3, flag=$4,created_by=$5, updated_on=$6 where md5(gallery_id::text) =$7";

$update_gaphoto_llery_1="UPDATE fw_photo_gallery_master set gallery_name=$1, gallery_type=$2, gallery_url=$3, gallery_data=$4, flag=$5, created_by=$6, updated_on=$7 where md5(gallery_id::text) =$8";

$delete_photo_gallery = "UPDATE fw_photo_gallery_master set flag=0 where md5(gallery_id::text) = $1";

//Manage Tab
$fetch_tab_count ="SELECT * from fw_tab_master where 1=$1 order by tab_id DESC limit 1";
$fetch_manage_tab="SELECT * from fw_tab_master where 1=$1 order by tab_id DESC";
$fetch_tab_check="SELECT * from fw_tab_master where tab_name = $1";

$fetch_insert_tab = "INSERT into fw_tab_master(tab_id,tab_name,tab_name_hindi,main_content_eng,main_content_hindi,flag,created_by, created_on) values ($1,$2,$3,$4,$5,$6,$7,$8)";

$fetch_edit_tab="SELECT * from fw_tab_master where md5(tab_id::text)=$1";

$fetch_update_tab = "UPDATE fw_tab_master set tab_name=$1,tab_name_hindi=$2, main_content_eng=$3, main_content_hindi=$4, flag=$5,created_by=$6, updated_on =$7 where md5(tab_id::text)=$8";


//Manage Updated Date
$fetch_date_count ="SELECT * from fw_date_master where 1=$1 order by date_id DESC limit 1";
$fetch_manage_date="SELECT * from fw_date_master where 1=$1 order by date_id DESC";
$fetch_date_check="SELECT * from fw_date_master where title = $1";

$fetch_insert_date = "INSERT into fw_date_master(date_id,lst_dt,title,created_on) values ($1,$2,$3,$4)";

$fetch_edit_date="SELECT * from fw_date_master where md5(date_id::text)=$1";

$fetch_updated_date = "UPDATE fw_date_master set lst_dt=$1,title=$2,updated_on=$3 where md5(date_id::text)=$4";



?>

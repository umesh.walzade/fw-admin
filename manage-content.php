<?php
include("global/user_global.php");

check_login();
$page		=	"content";
$sub_page	=	"manage-content";
if(isset($_SESSION["id"]))
{
	unset($_SESSION["id"]);
}
$array_menu		=	array("0"=>"No Menu / Other Menu","1"=>"Main Menu","2"=>"Middle Menu","3"=>"Footer Menu","4"=>"No Menu / Other Menu");


?>
<!DOCTYPE html>
<html lang="en" class="app">
<head>
    <meta charset="utf-8" />
    <title>Manage Content</title>
    <link rel="icon" href="images/favicon.ico">
    <meta name="description" content="app, web app, responsive, admin dashboard, admin, flat, flat ui, ui kit, off screen nav" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="stylesheet" href="css/app.v2.css" type="text/css" />
    <link rel="stylesheet" href="css/font.css" type="text/css" cache="false" />
    <link rel="stylesheet" href="js/calendar/bootstrap_calendar.css" type="text/css" cache="false" />
    <link rel="stylesheet" href="js/datatables/datatables.css" type="text/css" cache="false" />
    
    <!--[if lt IE 9]> <script src="js/ie/html5shiv.js" cache="false"></script> <script src="js/ie/respond.min.js" cache="false"></script> <script src="js/ie/excanvas.js" cache="false"></script> <![endif]-->
</head>

<body>
    <section class="vbox">
        <?php include("include/latest_js.php") ?>
        <?php include("include/topbar.php") ?>
        <section>
            <section class="hbox stretch">
                <!-- .aside -->
                <?php include("include/sidebar.php") ?>
                <!-- /.aside -->
                <section id="content">
                    <section class="vbox">
                        <section class="scrollable padder">
                            <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                                <li><a href="dashboard"><i class="fa fa-home"></i> Home</a></li>
                                <li><a href="">Content</a></li>
                                <li class="active">Manage Content</li>
                            </ul>
                            <div class="m-b-md">
                                <h3 class="m-b-none">Manage Content</h3>
                                <small>Welcome back, <?php echo $_SESSION["admin_name"] ?></small>
                                <input type="button" class="btn btn-danger pull-right" value="CREATE" onClick="location.href='create-content'">
                            </div>
                            <section class="panel panel-default">
                                <div class="table-responsive">
                                    <table class="table table-striped m-b-none" id="mytable">
                                        <thead>
                                            <tr>
												<th width="10%">Sr.</th>
                                                <th>Menu Name</th>
                                                <th>Menu Type</th>
                                                <th>Page Name</th>
                                                <th>Content  Head</th>
                                                <th>Is Active</th>
                                                <th>Created On</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php
										$i				=	1;
										$result_arr = array('i');
                                        $result_arr_val = array(1);
                                        $result_cat     =   $db_object->execute_select($fetch_manage_content,$result_arr,$result_arr_val);
                                        while($rows       =   pg_fetch_array($result_cat))
										{
											$type_array	=	array("0","Sub Menu","Main Menu","Middle Menu","Footer Menu","Other Menu","header-Footer Menu");
											$flag 		=	array(0=>"Unpublish",1=>"Publish");
										?>
                                        	<tr>
												<td width="10%"><?php echo $i ?></td>
                                                <td><?php echo $rows["menu_name"] ?></td>
                                                <td><?php echo $type_array[$rows["menu_type"]] ?></td>
                                                <td><?php echo $rows["page_name"] ?></td>
                                                <td><?php echo $rows["content_title_eng"] ?></td>
                                                <td><?php echo $flag[$rows["is_active"]] ?></td>
                                                <td><?php echo date("d-m-Y",strtotime($rows["cret_date"])) ?></td>
                                                <td><a href="create-content?f9c7a57c74dcc509=<?php echo md5($rows["cid"]) ?>"><i class="fa fa-pencil-square-o"></i></a>  <?php if($rows["is_active"] ==1){ ?> | <a title="Delete" href="actions/create-content?action&myaction=<?php echo md5("Dceblfecte");?>&f9c7a57c74dcc509=<?php echo md5($rows["cid"]) ?>" onclick="return ConfirmDelete();"> <i class="fa fa-trash-o"></i></a><?php }?> <?php //| <a title="Copy to Archive" href="actions/create-archive?action&myaction=< ?php echo md5("cabdfdc")? >&f9c7a57c74dcc509=<?php echo md5($rows["cid"]) ? >"><i class="fa fa-archive"></i></a> ?></td>
                                            </tr>
                                         <?php
										 $i++;
										}
										?>
                                        </tbody>
                                    </table>
                                </div>
                            </section>
                            
                        </section>
                    </section>
                    <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav"></a>
                </section>
                <aside class="bg-light lter b-l aside-md hide" id="notes">
                    <div class="wrapper">Notification</div>
                </aside>
            </section>
        </section>
    </section>
    <script src="js/app.v2.js"></script>
    <!-- Bootstrap -->
    <!-- App -->
    <script src="js/sortable/jquery.sortable.js" cache="false"></script>
    <script src="js/datatables/jquery.dataTables.min.js" cache="false"></script>
    <script>
	$(document).ready(function() {
    $('#mytable').DataTable();
} );
	</script>
     <script>
	function ConfirmDelete()
    {
      var x = confirm("Are you sure you want to delete?");
      if (x)
          return true;
      else
        return false;
    }
	$(document).ready(function() {
    $('#mytable').DataTable();
} );
    
	</script>

</body>

</html>
/**
 * @license Copyright (c) 2003-2016, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
	config.allowedContent = true;
	config.protectedSource.push( /<i class[\s\S]*?\>/g ); //allows beginning <i> tag
	config.protectedSource.push( /<\/i>/g ); //allows ending </i> tag
	config.extraAllowedContent = 'dl dt dd i';
	config.protectedSource.push( /<em class[\s\S]*?\>/g ); //allows beginning <i> tag
	config.protectedSource.push( /<\/em>/g ); //allows ending </i>
	config.extraAllowedContent= 'data-toggle[*]{*}';
	

};

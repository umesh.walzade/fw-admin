<?php
include("global/user_global.php");
check_login();
$page		=	"Gallery";
$sub_page	=	"manage-slider";
?>
<!DOCTYPE html>
<html lang="en" class="app">
<head>
    <meta charset="utf-8" />
    <title>Manage Slider</title>
    <link rel="icon" href="images/favicon.ico">
    <meta name="description" content="app, web app, responsive, admin dashboard, admin, flat, flat ui, ui kit, off screen nav" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="stylesheet" href="css/app.v2.css" type="text/css" />
    <link rel="stylesheet" href="css/font.css" type="text/css" cache="false" />
    <link rel="stylesheet" href="js/calendar/bootstrap_calendar.css" type="text/css" cache="false" />
    <link rel="stylesheet" href="js/datatables/datatables.css" type="text/css" cache="false" />
    
    <!--[if lt IE 9]> <script src="js/ie/html5shiv.js" cache="false"></script> <script src="js/ie/respond.min.js" cache="false"></script> <script src="js/ie/excanvas.js" cache="false"></script> <![endif]-->
    <style>
	.fa{
		cursor:pointer !important;
	}
	</style>
</head>

<body>
    <section class="vbox">
        <?php include("include/latest_js.php") ?>
        <?php include("include/topbar.php") ?>
        <section>
            <section class="hbox stretch">
                <!-- .aside -->
                <?php include("include/sidebar.php") ?>
                <!-- /.aside -->
                <section id="content">
                    <section class="vbox">
                        <section class="scrollable padder">
                            <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                                <li><a href="index.html"><i class="fa fa-home"></i> Home</a></li>
                                <li><a href="manage-gallery">Gallery</a></li>
                                <li class="active">Manage Gallery</li>
                            </ul>
                            <div class="m-b-md">
                                <h3 class="m-b-none">Manage Gallery</h3>
                                <small>Welcome back, <?php echo $_SESSION["admin_name"] ?></small>
                                <input type="button" class="btn btn-danger pull-right" value="CREATE" onClick="location.href='add-slider'">
                            </div>
                            <section class="panel panel-default">
                                <div class="table-responsive">
                                    <table class="table table-striped m-b-none" id="mytable">
                                        <thead>
                                            <tr>
												<th>Sr.</th>
                                                <th>Type</th>
                                                <th>Image</th>
                                                <th>Name</th>
                                                <th>Flag</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php
										$i				=	1;
                                        $result_arr = array('i');
                                        $result_arr_val = array(1);
										$result_cat     =   $db_object->execute_select($fetch_manage_slider,$result_arr,$result_arr_val);
                                        while($rows       =   pg_fetch_array($result_cat))
										{
											if($rows["flag"]==1)
											{
												$type	=	"Published";
											}
											else
											{
												$type	=	"Un-Published";
											}
										?>
                                        	<tr>
												<td><?php echo $i ?></td>
                                                <td><?php echo $rows["slider_type"] ?></td>
                                                <td>
                                                <?php
												    $base_path="../".$img_path;                                                    
													echo "<img src=\"$base_path".$rows["img"]."\" width=\"100\" style=\"margin-left:-30px;\" alt=\"".$rows["img"]."\" >";
												?>
                                                </td>
                                                <td><?php echo $rows["slider_name"] ?></td>
                                                <td><?php echo $type ?></td>
                                                <td><a title="Edit" href="add-slider?action&myaction=<?php echo md5("ecdbiftc");?>&f9c7a57c74dcc509=<?php echo md5($rows["slider_id"]) ?>"><i class="fa fa-pencil-square-o"></i></a>  <?php if($type=="Published") {?> | <a title="Delete" href="actions/add-slider?action&myaction=<?php echo md5("Dceblfecte");?>&f9c7a57c74dcc509=<?php echo md5($rows["slider_id"]) ?>" Onclick="return ConfirmDelete();"><i class="fa fa-trash-o"></i></a><?php } ;?></td>
                                            </tr>
                                         <?php
										 $i++;
										}
										?>
                                        </tbody>
                                    </table>
                                </div>
                            </section>
                            
                        </section>
                    </section>
                    <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav"></a>
                </section>
                <aside class="bg-light lter b-l aside-md hide" id="notes">
                    <div class="wrapper">Notification</div>
                </aside>
            </section>
        </section>
    </section>
    <script src="js/app.v2.js"></script>
    <!-- Bootstrap -->
    <!-- App -->
    <script src="js/sortable/jquery.sortable.js" cache="false"></script>
    <script src="js/datatables/jquery.dataTables.min.js" cache="false"></script>
    <script>
	function ConfirmDelete()
    {
      var x = confirm("Are you sure you want to delete?");
      if (x)
          return true;
      else
        return false;
    }
	$(document).ready(function() {
    $('#mytable').DataTable();
} );
    
	</script>
</body>

</html>